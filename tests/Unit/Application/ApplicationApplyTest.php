<?php

namespace Tests\Unit\Application;

use App\Application;
use App\Organizer;
use App\Services\ApplicationService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Throwable;

class ApplicationApplyTest extends ApplicationTest
{

    /** @test */
    public function apply_creates_event_application_if_request_valid()
    {
        $this->mock_validation_true('validateApplication');
        $request = new Request($this->application->toArray());
        $response = $this->controller->apply($request, $this->event);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertDatabaseHas('applications', $this->application->toArray());
    }

    /** @test */
    public function apply_returns_error_message_if_request_invalid()
    {
        $this->mock_validation_false('validateApplication');
        $request = new Request($this->application->toArray());
        try {
            $response = $this->controller->apply($request, $this->event);
        } catch (Throwable $e) {
            $this->assertEquals(400, $e->getStatusCode());
        }
        $this->assertDatabaseMissing('applications', $this->application->toArray());
    }

    /** @test */
    public function validate_application_returns_true_if_application_valid()
    {
        $request = new Request($this->application->toArray());
        $response = $this->service->validateApplication($request, $this->event);

        $this->assertTrue($response->isValid);
    }

    /** @test */
    public function validate_application_returns_false_if_no_letter()
    {
        $request = new Request(Arr::except($this->application->toArray(), ['letter']));
        $response = $this->service->validateApplication($request, $this->event);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_application_returns_false_if_letter_empty()
    {
        $request = new Request(factory(Application::class)->make(['letter' => ''])->toArray());
        $service = new ApplicationService();
        $response = $service->validateApplication($request, $this->event);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_application_returns_false_if_past_deadline()
    {
        $this->event->deadline = Carbon::yesterday();
        $request = new Request($this->application->toArray());
        $response = $this->service->validateApplication($request, $this->event);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_application_returns_false_if_user_already_applied()
    {
        factory(Application::class)->create(['user_id' => $this->user->id, 'event_id' => $this->event->id]);
        $request = new Request($this->application->toArray());
        $response = $this->service->validateApplication($request, $this->event);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_application_returns_false_if_organizer_applies_to_own_event()
    {
        $this->organizer = factory(Organizer::class)->create([
            'organizer_id' => $this->user->id,
            'organizer_type' => 'user',
            'event_id' => $this->event->id
        ]);
        $request = new Request($this->application->toArray());
        $response = $this->service->validateApplication($request, $this->event);
        $this->assertFalse($response->isValid);
    }
}
