<?php

namespace Tests\Unit\Application;

use App\Application;
use App\Event;
use App\Helpers\ValidationResponse;
use App\Http\Controllers\ApplicationController;
use App\Services\ApplicationService;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use Tests\TestCase;

abstract class ApplicationTest extends TestCase
{
    // use RefreshDatabase;
    use DatabaseTransactions;

    // Does not truncate actual database

    protected $controller;
    /* @var User */
    protected $user;
    /* @var Event */
    protected $event;
    /* @var Application */
    protected $application;
    protected $service;

    public function setUp(): void
    {
        parent::setUp();

        $applicationServiceMock = Mockery::mock(ApplicationService::class);
        $this->controller = new ApplicationController($applicationServiceMock);
        $this->service = new ApplicationService();

        // Create a fake user, event and application; Log in as fake user.
        $this->user = factory(User::class)->create();
        $this->event = factory(Event::class)->create(['deadline' => Carbon::tomorrow()]);
        $this->application = factory(Application::class)->make([
            'user_id' => $this->user->id,
            'event_id' => $this->event->id,
        ]);
        $this->be($this->user);
    }


    //==================================================================================================================
    // Helpers
    //==================================================================================================================

    protected function mock_validation_true($validationMethod)
    {
        $applicationServiceMock = Mockery::mock(ApplicationService::class);
        $applicationServiceMock->shouldReceive($validationMethod)
            ->once()
            ->andReturn(new ValidationResponse(true));
        $this->controller = new ApplicationController($applicationServiceMock);
    }

    protected function mock_validation_false($validationMethod)
    {
        $applicationServiceMock = Mockery::mock(ApplicationService::class);
        $applicationServiceMock->shouldReceive($validationMethod)
            ->once()
            ->andReturn(new ValidationResponse(false, 'Error.'));
        $this->controller = new ApplicationController($applicationServiceMock);
    }
}
