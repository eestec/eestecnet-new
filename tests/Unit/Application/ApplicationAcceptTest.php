<?php

namespace Tests\Unit\Application;

use App\Application;
use Illuminate\Http\Request;

class ApplicationAcceptTest extends ApplicationTest
{
    private $applications;

    public function setUp(): void
    {
        parent::setUp();
        $this->applications = factory(Application::class, 5)->create([
            'user_id' => $this->user->id,
            'event_id' => $this->event->id,
        ])->pluck('id')->toArray();
    }

    /** @test */
    public function accept_list_sets_all_provided_applications_as_accepted()
    {
        $accept = array_slice($this->applications, 0, 3);
        $response = $this->controller->acceptList(new Request(['accept_ids' => $accept]), $this->event);

        $this->assertEquals(200, $response->getStatusCode());
        $acceptApps = Application::whereId($accept);
        foreach ($acceptApps as $app) {
            $this->assertTrue($app->is_accepted());
        }
    }

    /** @test */
    public function accept_list_sets_all_provided_applications_as_declined()
    {
        $decline = array_slice($this->applications, 0, 3);
        $response = $this->controller->acceptList(new Request(['decline_ids' => $decline]), $this->event);

        $this->assertEquals(200, $response->getStatusCode());
        $declineApps = Application::whereId($decline);
        foreach ($declineApps as $app) {
            $this->assertFalse($app->is_accepted());
        }
    }

    /** @test */
    public function accept_list_sets_all_provided_applications_as_accepted_and_declined()
    {
        $accept = array_slice($this->applications, 0, 3);
        $decline = array_slice($this->applications, 3, 2);
        $requestData = [
            'accept_ids' => $accept,
            'decline_ids' => $decline
        ];
        $response = $this->controller->acceptList(new Request($requestData), $this->event);

        $this->assertEquals(200, $response->getStatusCode());
        $acceptApps = Application::whereId($accept);
        $declineApps = Application::whereId($decline);
        foreach ($acceptApps as $app) {
            $this->assertTrue($app->is_accepted());
        }
        foreach ($declineApps as $app) {
            $this->assertFalse($app->is_accepted());
        }
    }

    /** @test */
    public function accept_sets_application_as_accepted()
    {
        $response = $this->controller->review($this->event, $this->application, true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($this->application->is_accepted());
    }

    /** @test */
    public function decline_sets_application_as_declined()
    {
        $response = $this->controller->review($this->event, $this->application, false);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertFalse($this->application->is_accepted());
    }
}
