<?php

namespace Tests\Unit\Application;

use App\Application;
use App\User;
use Carbon\Carbon;

class ApplicationDeleteTest extends ApplicationTest
{
    public function setUp(): void
    {
        parent::setUp();
        $this->application->save();
    }

    /** @test */
    public function delete_deletes_application_if_valid()
    {
        $this->mock_validation_true('validateDelete');
        $response = $this->controller->destroy($this->event, $this->application);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseMissing('applications', $this->application->toArray());
    }

    /** @test */
    public function delete_fails_if_invalid()
    {
        $this->mock_validation_false('validateDelete');
        $response = $this->controller->destroy($this->event, $this->application);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertDatabaseHas('applications', $this->application->toArray());
    }

    /** @test */
    public function validate_delete_returns_valid_if_request_valid()
    {
        $response = $this->service->validateDelete($this->event, $this->application);
        $this->assertTrue($response->isValid);
    }

    /** @test */
    public function validate_update_returns_invalid_if_past_deadline()
    {
        $this->event->deadline = Carbon::yesterday();
        $this->event->save();
        $response = $this->service->validateDelete($this->event, $this->application);
        $this->assertFalse($response->isValid);

    }

    /** @test */
    public function validate_update_returns_invalid_if_updating_another_users_application()
    {
        $anotherUser = factory(User::class)->create();
        $application = factory(Application::class)->create([
            'user_id' => $anotherUser->id,
            'event_id' => $this->event->id,
        ]);
        $response = $this->service->validateDelete($this->event, $application);
        $this->assertFalse($response->isValid);
    }
}
