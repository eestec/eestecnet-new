<?php

namespace Tests\Unit\Application;

use App\Application;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ApplicationUpdateTest extends ApplicationTest
{
    public function setUp(): void
    {
        parent::setUp();
        $this->application->save();
    }

    /** @test */
    public function update_updates_application_if_valid()
    {
        $this->mock_validation_true('validateUpdate');
        $this->application->letter = 'Modified letter';
        $request = new Request($this->application->toArray());
        $response = $this->controller->update($request, $this->event, $this->application);
        $application = Application::whereId($this->application->id)->first();

        $this->assertEquals($this->application->letter, $application->letter);
        $this->assertEquals(200, $response->getStatusCode());
    }

    /** @test */
    public function update_fails_if_invalid()
    {
        $this->mock_validation_false('validateUpdate');
        $oldLetter = $this->application->letter;
        $this->application->letter = 'Modified letter';
        $request = new Request($this->application->toArray());
        $response = $this->controller->update($request, $this->event, $this->application);
        $application = Application::whereId($this->application->id)->first();

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals($oldLetter, $application->letter);
    }

    /** @test */
    public function validate_update_returns_valid_if_request_valid()
    {
        $this->application->letter = 'New motivational letter';
        $request = new Request($this->application->toArray());
        $response = $this->service->validateUpdate($this->event, $this->application, $request);
        $this->assertTrue($response->isValid);
    }

    /** @test */
    public function validate_update_returns_invalid_if_past_deadline()
    {
        $this->event->deadline = Carbon::yesterday();
        $this->event->save();
        $request = new Request($this->application->toArray());
        $response = $this->service->validateUpdate($this->event, $this->application, $request);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_update_returns_invalid_if_updating_another_users_application()
    {
        $anotherUser = factory(User::class)->create();
        $application = factory(Application::class)->create([
            'user_id' => $anotherUser->id,
            'event_id' => $this->event->id,
        ]);

        $request = new Request($application->toArray());
        $response = $this->service->validateUpdate($this->event, $application, $request);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_update_returns_invalid_if_no_motivational_letter()
    {
        $request = new Request(Arr::except($this->application->toArray(), ['letter']));
        $response = $this->service->validateUpdate($this->event, $this->application, $request);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function show_letter_returns_motivational_letter_if_request_valid()
    {
        $this->mock_validation_true('validateShowLetter');
        $response = $this->controller->showLetter($this->event, $this->application);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('letter', $response->getOriginalContent());
    }

    /** @test */
    public function show_letter_returns_error_if_request_invalid()
    {
        $this->mock_validation_false('validateShowLetter');
        $response = $this->controller->showLetter($this->event, $this->application);
        $this->assertEquals(422, $response->getStatusCode());
    }

    /** @test */
    public function validate_show_letter_returns_invalid_if_past_deadline()
    {
        $this->event->deadline = Carbon::yesterday();
        $this->event->save();
        $response = $this->service->validateShowLetter($this->event, $this->application);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_show_letter_returns_invalid_if_asking_for_another_users_application()
    {
        $anotherUser = factory(User::class)->create();
        $application = factory(Application::class)->create([
            'user_id' => $anotherUser->id,
            'event_id' => $this->event->id,
        ]);
        $response = $this->service->validateShowLetter($this->event, $application);
        $this->assertFalse($response->isValid);
    }
}
