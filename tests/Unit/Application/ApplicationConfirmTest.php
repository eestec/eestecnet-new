<?php

namespace Tests\Unit\Application;


use App\Application;
use App\User;
use Carbon\Carbon;

class ApplicationConfirmTest extends ApplicationTest
{

    public function setUp(): void
    {
        parent::setUp();

        $this->application->accepted = true;
        $this->application->reviewed_at = Carbon::now();
        $this->application->reviewed_by = $this->user->id;
        $this->application->save();

        $this->event->deadline = Carbon::yesterday();
        $this->event->save();
    }

    /** @test */
    public function confirm_sets_application_confirmed_if_valid()
    {
        $this->mock_validation_true('validateConfirm');
        $response = $this->controller->confirm($this->event, $this->application);
        $updatedApplication = $this->application->fresh();
        $this->assertTrue($updatedApplication->is_confirmed());
        $this->assertEquals(200, $response->getStatusCode());
    }

    /** @test */
    public function confirm_keeps_application_confirmation_unchanged_if_invalid()
    {
        $this->mock_validation_false('validateConfirm');
        $response = $this->controller->confirm($this->event, $this->application);
        $updatedApplication = $this->application->fresh();

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertFalse($updatedApplication->is_confirmed());
    }

    /** @test */
    public function validate_confirm_returns_true_if_valid()
    {
        $response = $this->service->validateConfirm($this->event, $this->application);
        $this->assertTrue($response->isValid);
    }

    /** @test */
    public function validate_confirm_returns_false_if_before_deadline()
    {
        $this->event->deadline = Carbon::tomorrow();
        $response = $this->service->validateConfirm($this->event, $this->application);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_confirm_returns_false_if_another_users_application()
    {
        $anotherUser = factory(User::class)->create();
        $application = factory(Application::class)->create([
            'user_id' => $anotherUser->id,
            'event_id' => $this->event->id,
        ]);
        $response = $this->service->validateConfirm($this->event, $application);
        $this->assertFalse($response->isValid);
    }

    /** @test */
    public function validate_confirm_returns_false_if_not_accepted()
    {
        $this->application->accepted = false;
        $response = $this->service->validateConfirm($this->event, $this->application);
        $this->assertFalse($response->isValid);
    }
}
