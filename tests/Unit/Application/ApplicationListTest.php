<?php

namespace Tests\Unit\Application;

use App\Application;
use App\Event;

class ApplicationListTest extends ApplicationTest
{
    public function setUp(): void
    {
        parent::setUp();
        $this->application->save();
    }

    /** @test */
    public function index_lists_all_applications_for_event()
    {
        $randomEvent = factory(Event::class)->create();
        $this->application = factory(Application::class)->create([
            'user_id' => $this->user->id,
            'event_id' => $randomEvent->id,
        ]);
        $response = $this->controller->index($this->event);
        $this->assertCount(1, $response);
    }

    /** @test */
    public function show_returns_a_specified_application()
    {
        $response = $this->controller->show($this->event, $this->application);
        $this->assertEquals($this->application->id, $response->toArray()['id']);
    }
}
