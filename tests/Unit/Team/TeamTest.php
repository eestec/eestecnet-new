<?php

namespace Tests\Unit\Team;

use App\Http\Controllers\TeamController;
use App\Services\TeamService;
use App\Team;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Mockery;
use Tests\TestCase;

abstract class TeamTest extends TestCase
{
    use DatabaseTransactions;

    // Does not truncate actual database

    protected $controller;
    /* @var User */
    protected $user;
    /* @var Team */
    protected $team;
    protected $service;

    public function setUp(): void
    {
        parent::setUp();

        $teamServiceMock = Mockery::mock(TeamService::class);
        $this->controller = new TeamController($teamServiceMock);
        $this->service = new TeamService();

        // Create a fake user; Log in as fake user.
        $this->user = factory(User::class)->create();
        $this->be($this->user);
    }

    //==================================================================================================================
    // Helpers
    //==================================================================================================================

    protected function mock_validation_true()
    {
        $teamServiceMock = Mockery::mock(TeamService::class);
        $team = $this->team->toArray();
        $teamServiceMock->shouldReceive('validateTeam')
            ->once()
            ->andReturn($team);
        $this->controller = new TeamController($teamServiceMock);
    }

    protected function mock_validation_false()
    {
        $teamServiceMock = Mockery::mock(TeamService::class);
        $this->expectException(ValidationException::class);
        $teamServiceMock->shouldReceive('validateTeam')
            ->once()
            ->andThrow(new ValidationException(Validator::make([], [])));
        $this->controller = new TeamController($teamServiceMock);
    }
}
