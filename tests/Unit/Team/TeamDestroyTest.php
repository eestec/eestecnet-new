<?php

namespace Tests\Unit\Team;

use App\Team;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TeamDestroyTest extends TeamTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    protected $team;

    public function setUp(): void
    {
        parent::setUp();
        $this->team = factory(Team::class)->create();
        $this->teamArray = $team = $this->team->toArray();
    }

    /** @test */
    public function destroy_team()
    {
        $response = $this->controller->destroy($this->team->slug);
        $this->assertEquals(202, $response->getStatusCode());
    }
}
