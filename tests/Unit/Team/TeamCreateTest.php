<?php

namespace Tests\Unit\Team;

use App\Team;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;

class TeamCreateTest extends TeamTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->team = factory(Team::class)->create();
        $this->teamArray = $team = $this->team->toArray();
    }

    /** @test */
    public function store_creates_team_if_valid()
    {
        $this->mock_validation_true();
        $response = $this->controller->store(new Request($this->teamArray));
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertDatabaseHas('teams', $this->team->toArray());
    }

    /** @test */
    public function store_fails_if_invalid()
    {
        $this->mock_validation_false();
        $response = $this->controller->store(new Request($this->teamArray));
        $this->assertDatabaseMissing('teams', $this->team->toArray());
        $this->assertEquals(500, $response->getStatusCode());
    }

    /** @test */
    public function validate_team_fails_if_required_field_not_provided()
    {
        $requiredFields = [
            'name',
            'category',
            'thumbnail',
            'description',
            'founded',
        ];
        foreach ($requiredFields as $field) {
            $team = Arr::except($this->teamArray, [$field]);
            $this->expectException(ValidationException::class);
            $response = $this->service->validateTeam(new Request($team));
            $this->assertNotEmpty($response->getContent());
        }
    }

    /** @test */
    public function validate_team_fails_if_type_invalid()
    {
        $team = $this->teamArray;
        $team['category'] = 'ab';
        $this->expectException(ValidationException::class);
        $response = $this->service->validateTeam(new Request($team));
        $this->assertNotEmpty($response->getContent());
    }

    /** @test */
    public function validate_team_succeeds_if_type_valid()
    {
        $team = $this->teamArray;
        foreach (['team', 'project', 'ba'] as $category) {
            $team['category'] = $category;
            $response = $this->service->validateTeam(new Request($team));
            $this->assertNotEmpty($response);
        }
    }
}
