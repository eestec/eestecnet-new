<?php

namespace Tests\Unit\Team;

use App\Membership;
use App\Team;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TeamMembersTest extends TeamTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->team = factory(Team::class)->create();
        $this->teamArray = $team = $this->team->toArray();
        $this->another_user = factory(User::class)->create();
        $this->membership = factory(Membership::class)->create([
            'user_id' => $this->another_user->id,
            'membership_id' => $this->team->id,
            'membership_type' => 'team'
        ]);
    }

    /** @test */
    public function list_pending_members_requests()
    {
        $response = $this->controller->pendingMembers($this->team->slug);
        $this->assertNotEmpty($response->toArray());
    }

    /** @test */
    public function accept_member_to_team()
    {
        $membership = $this->membership->toArray();
        $response = $this->controller->accept($membership['id']);
        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals($this->user->id, Membership::whereId($this->membership['id'])->first()->accepted_by);
    }

    /** @test */
    public function apply_to_join_team()
    {
        $response = $this->controller->apply($this->team->slug);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue(Membership::whereUserId($this->user->id)->exists());
    }
}
