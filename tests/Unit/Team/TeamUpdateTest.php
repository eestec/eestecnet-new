<?php

namespace Tests\Unit\Team;

use App\Team;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

class TeamUpdateTest extends TeamTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->team = factory(Team::class)->create();
        $this->teamArray = $team = $this->team->toArray();
    }

    /** @test */
    public function update_modifies_team_if_valid()
    {
        $team = $this->teamArray;
        $team['name'] = 'new name';
        $this->mock_validation_true($team);
        $response = $this->controller->update(new Request($team), $this->team->slug);

        $this->assertEquals(202, $response->getStatusCode());
        // $this->assertEquals('new name', Team::find($this->team->id)->name);
    }

    /** @test */
    public function update_fails_if_invalid()
    {
        $team = $this->teamArray;
        $team['name'] = 'new name';
        $this->mock_validation_false();
        $response = $this->controller->update(new Request($team), $this->team->slug);

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals($this->team->name, Team::find($this->team->id)->name);
    }
}
