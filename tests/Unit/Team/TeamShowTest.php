<?php

namespace Tests\Unit\Team;

use App\Branch;
use App\Event;
use App\Organizer;
use App\Team;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TeamShowTest extends TeamTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->team = factory(Team::class)->create();
        $this->teamArray = $team = $this->team->toArray();
    }

    /** @test */
    public function show_team_if_slug_is_valid()
    {
        $response = $this->controller->show($this->team->slug);
        $this->assertNotEmpty($response->toArray());
    }

    /** @test */
    public function get_404_if_slug_is_invalid()
    {
        $response = $this->controller->show('invalid-slug');
        $this->assertEquals(404, $response->getStatusCode());
    }

    /** @test */
    public function list_events_of_team()
    {
        $this->branch = factory(Branch::class)->create();
        $this->event = factory(Event::class)->create();
        $this->organizer = factory(Organizer::class)->create([
            'organizer_id' => $this->team->id,
            'organizer_type' => 'team',
            'event_id' => $this->event->id
        ]);
        $response = $this->controller->listEvents($this->team->slug);
        $this->assertNotEmpty($response->toArray());
    }
}
