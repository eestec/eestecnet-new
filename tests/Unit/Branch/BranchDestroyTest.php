<?php

namespace Tests\Unit\Branch;

use App\Branch;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BranchDestroyTest extends BranchTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->branch = factory(Branch::class)->create();
        $this->branchArray = $branch = $this->branch->toArray();
    }

    /** @test */
    public function destroy_branch()
    {
        $response = $this->controller->destroy($this->branch->slug);
        $this->assertEquals(202, $response->getStatusCode());
    }
}
