<?php

namespace Tests\Unit\Branch;

use App\Branch;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;

class BranchCreateTest extends BranchTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->branch = factory(Branch::class)->make();
        $this->branchArray = $this->branch->toArray();
    }

    /** @test */
    public function store_creates_branch_if_valid()
    {
        $this->mock_validation_true();
        $response = $this->controller->store(new Request($this->branchArray));
        $this->assertDatabaseHas('branches', $this->branch->toArray());
        $this->assertEquals(201, $response->getStatusCode());
    }

    /** @test */
    public function store_fails_if_invalid()
    {
        $this->mock_validation_false();
        $response = $this->controller->store(new Request($this->branchArray));
        $this->assertDatabaseMissing('branches', $this->branch->toArray());
        $this->assertEquals(500, $response->getStatusCode());
    }

    /** @test */
    public function validate_branch_fails_if_required_field_not_provided()
    {
        $requiredFields = [
            'name',
            'country',
            'type',
            'thumbnail',
            'description',
            'region',
            'founded',
        ];
        foreach ($requiredFields as $field) {
            $branch = Arr::except($this->branchArray, [$field]);
            $this->expectException(ValidationException::class);
            $response = $this->service->validateBranch(new Request($branch));
            $this->assertNotEmpty($response->getContent());
        }
    }

    /** @test */
    public function validate_branch_fails_if_type_invalid()
    {
        $branch = $this->branchArray;
        $branch['type'] = 'ab';
        $this->expectException(ValidationException::class);
        $response = $this->service->validateBranch(new Request($branch));
        $this->assertNotEmpty($response->getContent());
    }

    /** @test */
    public function validate_branch_succeeds_if_type_valid()
    {
        $branch = $this->branchArray;
        foreach (['observer', 'jlc', 'lc'] as $type) {
            $branch['type'] = $type;
            $response = $this->service->validateBranch(new Request($branch));
            $this->assertNotEmpty($response);
        }
    }

    /** @test */
    public function validate_branch_fails_if_region_invalid()
    {
        $branch = $this->branchArray;
        $branch['region'] = 15;
        $this->expectException(ValidationException::class);
        $response = $this->service->validateBranch(new Request($branch));
        $this->assertNotEmpty($response->getContent());
    }

    /** @test */
    public function validate_branch_fails_if_region_valid()
    {
        $branch = $this->branchArray;
        foreach ([1, 2, 3, 4, 5, 6] as $region) {
            $branch['region'] = $region;
            $response = $this->service->validateBranch(new Request($branch));
            $this->assertNotEmpty($response);
        }
    }
}
