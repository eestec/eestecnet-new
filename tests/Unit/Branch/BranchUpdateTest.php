<?php

namespace Tests\Unit\Branch;

use App\Branch;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

class BranchUpdateTest extends BranchTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->branch = factory(Branch::class)->create();
        $this->branchArray = $branch = $this->branch->toArray();
    }

    /** @test */
    public function update_modifies_branch_if_valid()
    {
        $branch = $this->branchArray;
        $branch['name'] = 'new name';
        $this->mock_validation_true($branch);
        $response = $this->controller->update(new Request($branch), $this->branch->slug);

        $this->assertEquals(202, $response->getStatusCode());
        // $this->assertEquals('new name', Branch::find($this->branch->id)->name);
    }

    /** @test */
    public function update_fails_if_invalid()
    {
        $branch = $this->branchArray;
        $branch['name'] = 'new name';
        $this->mock_validation_false();
        $response = $this->controller->update(new Request($branch), $this->branch->slug);

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals($this->branch->name, Branch::find($this->branch->id)->name);
    }
}
