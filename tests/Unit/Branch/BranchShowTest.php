<?php

namespace Tests\Unit\Branch;

use App\Branch;
use App\Event;
use App\Organizer;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BranchShowTest extends BranchTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->branch = factory(Branch::class)->create();
        $this->branchArray = $branch = $this->branch->toArray();
    }

    /** @test */
    public function show_branch_if_slug_is_valid()
    {
        $response = $this->controller->show($this->branch->slug);
        $this->assertNotEmpty($response->toArray());
    }

    /** @test */
    public function get_404_if_slug_is_invalid()
    {
        $response = $this->controller->show('invalid-slug');
        $this->assertEquals(404, $response->getStatusCode());
    }

    /** @test */
    public function list_events_of_branch()
    {
        $this->event = factory(Event::class)->create();
        $this->organizer = factory(Organizer::class)->create([
            'organizer_type' => 'branch',
            'organizer_id' => $this->branch->id,
            'event_id' => $this->event->id,
        ]);
        $response = $this->controller->listEvents($this->branch->slug);
        $this->assertNotEmpty($response->toArray());
    }
}
