<?php

namespace Tests\Unit\Branch;

use App\Application;
use App\Branch;
use App\Event;
use App\Membership;
use App\Organizer;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BranchMembersTest extends BranchTest
{
    use DatabaseTransactions;

    // Does not truncate actual database

    public function setUp(): void
    {
        parent::setUp();
        $this->branch = factory(Branch::class)->create();
        $this->another_user = factory(User::class)->create();
        $this->membership = factory(Membership::class)->create([
            'user_id' => $this->another_user->id,
            'membership_id' => $this->branch->id,
            'membership_type' => 'branch'
        ])->toArray();
        $this->branchArray = $branch = $this->branch->toArray();
    }

    /** @test */
    public function list_pending_members_requests()
    {
        $response = $this->controller->pendingMembers($this->branch->slug);
        $this->assertNotEmpty($response->toArray());
    }

    /** @test */
    public function accept_member_to_branch()
    {
        $membership = $this->membership;
        $response = $this->controller->accept($membership['id']);
        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals($this->user->id, Membership::whereId($this->membership['id'])->first()->accepted_by);
    }

    /** @test */
    public function apply_to_join_branch()
    {
        $response = $this->controller->apply($this->branch->slug);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue(Membership::whereUserId($this->user->id)->exists());
    }

    /** @test */
    public function list_applications_for_events()
    {
        $this->another_user = factory(User::class)->create();
        $this->event = factory(Event::class)->create();
        $this->membership = factory(Membership::class)->create([
            'user_id' => $this->another_user->id,
            'membership_id' => $this->branch->id,
            'membership_type' => 'branch'
        ]);
        $this->another_membership = factory(Membership::class)->create([
            'user_id' => $this->another_user->id,
            'membership_id' => $this->branch->id,
            'membership_type' => 'team'
        ]);
        $this->organizer = factory(Organizer::class)->create([
            'organizer_type' => 'branch',
            'organizer_id' => $this->branch->id,
            'event_id' => $this->event->id,
        ]);
        $this->application = factory(Application::class)->create([
            'event_id' => $this->event->id,
            'user_id' => $this->another_user->id
        ]);
        $response = $this->controller->listApplications($this->branch->slug);
        $this->assertNotEmpty($response->toArray());
    }
}
