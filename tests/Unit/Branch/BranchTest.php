<?php

namespace Tests\Unit\Branch;

use App\Branch;
use App\Http\Controllers\BranchController;
use App\Services\BranchService;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Mockery;
use Tests\TestCase;

abstract class BranchTest extends TestCase
{
    // use RefreshDatabase;
    use DatabaseTransactions;

    // Does not truncate actual database

    protected $controller;
    /* @var User */
    protected $user;
    /* @var Branch */
    protected $branch;
    protected $service;

    public function setUp(): void
    {
        parent::setUp();

        $branchServiceMock = Mockery::mock(BranchService::class);
        $this->controller = new BranchController($branchServiceMock);
        $this->service = new BranchService();

        // Create a fake user; Log in as fake user.
        $this->user = factory(User::class)->create();
        $this->be($this->user);
    }

    //==================================================================================================================
    // Helpers
    //==================================================================================================================

    protected function mock_validation_true()
    {
        $branchServiceMock = Mockery::mock(BranchService::class);
        $branch = $this->branch->toArray();
        $branchServiceMock->shouldReceive('validateBranch')
            ->once()
            ->andReturn($branch);
        $this->controller = new BranchController($branchServiceMock);
    }

    protected function mock_validation_false()
    {
        $branchServiceMock = Mockery::mock(BranchService::class);
        $this->expectException(ValidationException::class);
        $branchServiceMock->shouldReceive('validateBranch')
            ->once()
            ->andThrow(new ValidationException(Validator::make([], [])));
        $this->controller = new BranchController($branchServiceMock);
    }
}
