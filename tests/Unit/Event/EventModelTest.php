<?php

namespace Tests\Unit\Event;

use App\Application;
use App\Event;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventModelTest extends TestCase
{

    use RefreshDatabase;

    private $user;
    private $event;
    private $application;

    public function setUp(): void
    {
        parent::setUp();

        $this->event = factory(Event::class)->create();

        $this->user = factory(User::class)->create();
        $this->application = factory(Application::class)->make([
            'user_id' => $this->user->id,
            'event_id' => $this->event->id,
            'priority' => 1,
        ]);
        $this->be($this->user);
    }

    /** @test */
    public function get_application_status_not_applied()
    {
        $this->assertEquals("NOT_APPLIED", $this->event->getApplicationStatus($this->user->id));
    }

    /** @test */
    public function get_application_status_applied()
    {
        $this->application->save();
        $this->assertEquals("APPLIED", $this->event->getApplicationStatus($this->user->id));
    }

    /** @test */
    public function get_application_status_accepted()
    {
        $this->application->accepted = true;
        $this->application->save();
        $this->assertEquals("ACCEPTED", $this->event->getApplicationStatus($this->user->id));
    }

    /** @test */
    public function get_application_status_confirmed()
    {
        $this->application->accepted = true;
        $this->application->confirmed_at = Carbon::now();
        $this->application->save();
        $this->assertEquals("CONFIRMED", $this->event->getApplicationStatus($this->user->id));
    }
}
