<?php

namespace Tests\Unit\Event;

use App\Event;
use App\Http\Controllers\EventController;
use App\Services\EventService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Mockery;
use Tests\TestCase;

class EventListTest extends TestCase
{
    use RefreshDatabase;

    private $controller;

    public function setUp(): void
    {
        parent::setUp();

        $eventServiceMock = Mockery::mock(EventService::class);
        $this->controller = new EventController($eventServiceMock);
    }

    /** @test */
    public function index_returns_all_events()
    {
        $response = $this->controller->index(new Request());
        $this->assertCount(0, $response->toArray()['data']);
        factory(Event::class, 3)->create();
        $response = $this->controller->index(new Request());
        $this->assertCount(3, $response->toArray()['data']);
    }

    /** @test */
    public function index_open_returns_all_events_within_deadline()
    {
        $e1 = factory(Event::class)->create(['deadline' => Carbon::tomorrow()]);
        $e2 = factory(Event::class)->create(['deadline' => Carbon::tomorrow()]);
        $e3 = factory(Event::class)->create(['deadline' => Carbon::yesterday()]);
        $response = $this->controller->index(new Request(['open' => true]));
        $this->assertCount(2, $response->toArray()['data']);

        $ids = [];
        foreach ($response->toArray()['data'] as $event) {
            array_push($ids, $event['id']);
        }
        $this->assertContains($e1->id, $ids);
        $this->assertContains($e2->id, $ids);
        $this->assertNotContains($e3->id, $ids);
    }

    /** @test */
    public function index_open_returns_all_events_within_deadline_boundary_case()
    {
        $e1 = factory(Event::class)->create([
            'deadline' => Carbon::create(2020, 01, 01, 23, 59, 59)
        ]);
        $e2 = factory(Event::class)->create([
            'deadline' => Carbon::create(2020, 01, 02, 00, 00, 01)
        ]);

        $fakeNow = Carbon::create(2020, 01, 02, 00, 00, 00);
        Carbon::setTestNow($fakeNow);
        $response = $this->controller->index((new Request)->merge(['open' => true]));
        Carbon::setTestNow();

        $this->assertCount(1, $response->toArray()['data']);
        $this->assertEquals($e2->id, $response->toArray()['data'][0]['id']);
    }

    /** @test */
    public function index_current_returns_all_events_in_progress()
    {
        $e1 = factory(Event::class)->create([ //NO
            'deadline' => Carbon::now()->subDays(20),
            'starts' => Carbon::now()->subDays(10),
            'ends' => Carbon::now()->subDays(1),
        ]);
        $e2 = factory(Event::class)->create([ //YES
            'deadline' => Carbon::now()->subDays(10),
            'starts' => Carbon::now()->addDays(1),
            'ends' => Carbon::now()->addDays(5),
        ]);
        $e3 = factory(Event::class)->create([ //YES
            'deadline' => Carbon::now()->subDays(5),
            'starts' => Carbon::now()->addDays(5),
            'ends' => Carbon::now()->addDays(10),
        ]);
        $e4 = factory(Event::class)->create([ //NO
            'deadline' => Carbon::now()->addDays(1),
            'starts' => Carbon::now()->addDays(10),
            'ends' => Carbon::now()->addDays(20),
        ]);

        $response = $this->controller->index((new Request)->merge(['pending' => true, 'ongoing' => true]));
        $this->assertCount(2, $response->toArray()['data']);

        $ids = [];
        foreach ($response->toArray()['data'] as $event) {
            array_push($ids, $event['id']);
        }
        $this->assertNotContains($e1->id, $ids);
        $this->assertContains($e2->id, $ids);
        $this->assertContains($e3->id, $ids);
        $this->assertNotContains($e4->id, $ids);
    }

    /** @test */
    public function index_current_returns_all_events_in_progress_boundary_case()
    {
        $fakeNow = Carbon::create(2020, 01, 02, 00, 00, 00);
        Carbon::setTestNow($fakeNow);

        $e1 = factory(Event::class)->create([ //NO (past)
            'starts' => Carbon::now()->subDays(10),
            'ends' => Carbon::now()->subSecond(),
            'deadline' => Carbon::now()->subDays(30),
        ]);
        $e2 = factory(Event::class)->create([ //YES (ongoing)
            'starts' => Carbon::now()->subDays(10),
            'ends' => Carbon::now()->addSecond(),
            'deadline' => Carbon::now()->subDays(20),
        ]);
        $e3 = factory(Event::class)->create([ //YES (pending)
            'starts' => Carbon::now()->addSecond(),
            'deadline' => Carbon::now()->subDays(10),
            'ends' => Carbon::now()->addDays(10),
        ]);
        $e4 = factory(Event::class)->create([ //NO (open)
            'starts' => Carbon::now()->addDays(10),
            'deadline' => Carbon::now()->addSecond(),
            'ends' => Carbon::now()->addDays(20),
        ]);

        $response = $this->controller->index((new Request)->merge(['pending' => true, 'ongoing' => true]));
        $this->assertCount(2, $response->toArray()['data']);
        Carbon::setTestNow();

        $ids = [];
        foreach ($response->toArray()['data'] as $event) {
            array_push($ids, $event['id']);
        }
        $this->assertNotContains($e1->id, $ids);
        $this->assertContains($e2->id, $ids);
        $this->assertContains($e3->id, $ids);
        $this->assertNotContains($e4->id, $ids);
    }

    /** @test */
    public function index_past_returns_all_past_events()
    {
        $fakeNow = Carbon::create(2020, 01, 02, 00, 00, 00);
        Carbon::setTestNow($fakeNow);

        $e1 = factory(Event::class)->create([ //NO
            'ends' => Carbon::now()->addSecond(),
        ]);
        $e2 = factory(Event::class)->create([ //YES
            'ends' => Carbon::now()->subSecond(),
        ]);

        $response = $this->controller->index((new Request)->merge(['past' => true]));
        $this->assertCount(1, $response->toArray()['data']);
        Carbon::setTestNow();

        $ids = [];
        foreach ($response->toArray()['data'] as $event) {
            array_push($ids, $event['id']);
        }
        $this->assertNotContains($e1->id, $ids);
        $this->assertContains($e2->id, $ids);
    }
}
