<?php

namespace Tests\Unit\Event;

use App\Branch;
use App\Event;
use App\Http\Controllers\EventController;
use App\Services\EventService;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Mockery;
use Tests\TestCase;
use Throwable;

class EventUpdateTest extends TestCase
{
    use RefreshDatabase;

    private $controller;
    /* @var User */
    private $user;
    /* @var Event */
    private $event;
    /* @var Branch */
    private $branch;
    private $service;
    private $eventArray;

    public function setUp(): void
    {
        parent::setUp();

        //$eventServiceMock = Mockery::mock(EventService::class);
        //$this->controller = new EventController($eventServiceMock);
        $this->service = new EventService();
        $this->controller = new EventController($this->service);

        $this->branch = factory(Branch::class)->create();
        $this->event = factory(Event::class)->create();
        $this->eventArray = $this->event->toArray();
        //$this->eventArray['branch_id'] = $this->branch->id;

        $this->user = factory(User::class)->create();
        $this->be($this->user);
    }

    /** @test */
    public function update_modifies_event_if_valid()
    {
        $event = $this->eventArray;
        $event['name'] = 'new name';
        $this->mock_validation_true($event);
        $response = $this->controller->update(new Request($event), $this->event->slug);

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals('new name', Event::find($this->event->id)->name);
    }

    protected function mock_validation_true($returnData = null)
    {
        $eventServiceMock = Mockery::mock(EventService::class);
        if ($returnData == null) {
            $returnData = $this->event->toArray();
        }
        $eventServiceMock->shouldReceive('validateEvent')
            ->once()
            ->andReturn($returnData);
        $this->controller = new EventController($eventServiceMock);
    }

    //==================================================================================================================
    // Helpers
    //==================================================================================================================

    /** @test */
    public function update_fails_if_invalid()
    {
        $this->mock_validation_false();
        $event = $this->eventArray;
        $event['name'] = 'new name';
        try {
            $response = $this->controller->update(new Request($event), $this->event->slug);
        } catch (Throwable $e) {
            $this->assertEquals(422, $e->status);
        }

        $this->assertEquals($this->event->name, Event::find($this->event->id)->name);
    }

    protected function mock_validation_false()
    {
        $eventServiceMock = Mockery::mock(EventService::class);
        // $this->expectException(ValidationException::class);
        $eventServiceMock->shouldReceive('validateEvent')
            ->once()
            ->andThrow(new ValidationException(Validator::make([], [])));
        $this->controller = new EventController($eventServiceMock);
    }
}
