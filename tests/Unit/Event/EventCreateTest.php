<?php

namespace Tests\Unit\Event;

use App\Branch;
use App\Event;
use App\Http\Controllers\EventController;
use App\Services\EventService;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Mockery;
use Tests\TestCase;

class EventCreateTest extends TestCase
{
    use RefreshDatabase;

    private $controller;
    /* @var User */
    private $user;
    /* @var Event */
    private $event;
    /* @var Branch */
    private $branch;
    private $service;
    private $eventArray;

    public function setUp(): void
    {
        parent::setUp();

        //$eventServiceMock = Mockery::mock(EventService::class);
        //$this->controller = new EventController($eventServiceMock);
        $this->service = new EventService();
        $this->controller = new EventController($this->service);

        $this->branch = factory(Branch::class)->create();
        $this->event = factory(Event::class)->create();
        $this->eventArray = $this->event->toArray();
        // TODO: fix this test
        //$this->eventArray['branch_ids'] = [$this->branch->id];

        $this->user = factory(User::class)->create();
        $this->be($this->user);
    }

    /** @test */
    public function store_creates_event_if_valid()
    {
        $this->mock_validation_true();
        $response = $this->controller->store(new Request($this->eventArray));

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertDatabaseHas('events', $this->event->toArray());
    }

    protected function mock_validation_true()
    {
        $eventServiceMock = Mockery::mock(EventService::class);
        $event = $this->event->toArray();
        // $event['branch_ids'] = [$this->branch->id];
        $eventServiceMock->shouldReceive('validateEvent')
            ->once()
            ->andReturn($event);
        $this->controller = new EventController($eventServiceMock);
    }

    /** @test */
    public function store_fails_if_invalid()
    {
        $this->mock_validation_false();
        $response = $this->controller->store(new Request($this->eventArray));

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertDatabaseMissing('events', $this->event->toArray());
    }

    protected function mock_validation_false()
    {
        $eventServiceMock = Mockery::mock(EventService::class);
        $this->expectException(ValidationException::class);
        $eventServiceMock->shouldReceive('validateEvent')
            ->once()
            ->andThrow(new ValidationException(Validator::make([], [])));
        $this->controller = new EventController($eventServiceMock);
    }

    /** @test */
    public function validate_event_fails_if_required_field_not_provided()
    {
        $requiredFields = [
            'name',
            'type',
            'thumbnail',
            'location',
            'description',
            'max_pax',
            'fee',
            'starts',
            'ends',
            'deadline'
        ];
        foreach ($requiredFields as $field) {
            $event = Arr::except($this->eventArray, [$field]);
            $this->expectException(ValidationException::class);
            $this->service->validateEvent(new Request($event));
        }

    }

    //TODO: Check why this test fails; validation rule is 'min:0'

    /** @test */
    public function validate_event_fails_if_type_invalid()
    {
        $event = $this->eventArray;
        $event['type'] = 'ab';
        $this->expectException(ValidationException::class);
        $response = $this->service->validateEvent(new Request($event));
    }

    /** @test */
    public function validate_event_succeeds_if_type_valid()
    {
        $event = $this->eventArray;
        foreach (['ws', 'op', 'ex', 'cg', 'imw'] as $type) {
            $event['type'] = $type;
            $response = $this->service->validateEvent(new Request($event));
            $this->assertNotEmpty($response);
        }
    }

    // This test does not work all the time
    // TODO fix test

    public function validate_event_fails_if_fee_is_negative()
    {
        $event = $this->eventArray;
        $this->expectException(ValidationException::class);
        $this->service->validateEvent(new Request($event));
    }

    /** @test */
    public function validate_event_fails_if_date_not_valid()
    {
        $testDates = [
            '2019-17-17 23:59:59',
            '2019-12-17 24:59:59',
        ];
        $event = $this->eventArray;
        foreach ($testDates as $date) {
            $event['deadline'] = $date;
            $this->expectException(ValidationException::class);
            $response = $this->service->validateEvent(new Request($event));
        }
    }

    /**
     * @test
     * @throws Exception
     */
    public function validate_event_succeeds_if_date_valid()
    {
        $fakeNow = Carbon::create(2020, 01, 02, 00, 00, 00);
        Carbon::setTestNow($fakeNow);
        $event = $this->event;
        $carbon = Carbon::now();
        $event->deadline = $carbon;
        $event->starts = $carbon->addDays(20)->toDateString();
        $event->ends = $carbon->addDays(30)->toDateString();
        $response = $this->service->validateEvent(new Request($event->toArray()));
        $this->assertNotEmpty($response);
    }

    ///** @test */
    // public function validate_event_fails_if_branch_not_in_db()
    // {
    //     $event = $this->eventArray;
    //     // $event['branch_ids'] = [$this->branch->id + 1];
    //     $this->expectException(ValidationException::class);
    //     $this->service->validateEvent(new Request($event));
    // }

    //==================================================================================================================
    // Helpers
    //==================================================================================================================

    /** @test */
    public function validate_event_fails_if_starts_after_ends()
    {
        $event = $this->eventArray;
        $event['starts'] = '2019-12-17';
        $event['ends'] = '2019-12-16';
        $this->expectException(ValidationException::class);
        $this->service->validateEvent(new Request($event));
    }

    /** @test */
    public function validate_event_fails_if_deadline_after_starts()
    {
        $event = $this->eventArray;
        $event['starts'] = '2019-12-17';
        $event['deadline'] = '2019-12-18';
        $this->expectException(ValidationException::class);
        $this->service->validateEvent(new Request($event));
    }
}
