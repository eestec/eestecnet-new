<?php

namespace Tests\Unit\Event;

use App\Application;
use App\Event;
use App\Http\Controllers\EventController;
use App\Organizer;
use App\Services\EventService;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

class EventDetailsTest extends TestCase
{
    use RefreshDatabase;

    private $controller;

    public function setUp(): void
    {
        parent::setUp();

        $eventServiceMock = Mockery::mock(EventService::class);
        $this->controller = new EventController($eventServiceMock);
    }

    /** @test */
    public function show_returns_event_details()
    {
        $this->event = factory(Event::class)->create();

        //TODO:
        // Issue in User model - near "~": syntax error (SQL: select count(*) as
        // aggregate from "users" where slug ~ '^(-[0-9]+)?$')
        //$participant = User::create();
        //$organizer = User::create();

        $this->user = factory(User::class)->create();
        $this->another_user = factory(User::class)->create();

        $this->organizer = factory(Organizer::class)->create([
            'organizer_type' => 'user',
            'organizer_id' => $this->user->id,
            'event_id' => $this->event->id,
        ]);
        $this->application = factory(Application::class)->create([
            'event_id' => $this->event->id,
            'user_id' => $this->another_user->id,
            'accepted' => true
        ]);

        $response = $this->controller->show($this->event->slug);
        $this->assertEquals($this->event->id, $response->toArray()['id']);
        $this->assertEquals($this->another_user->id, $this->event->participants()->first()->id);
        $this->assertEquals($this->user->id, $this->event->organizers()->first()->id);
    }

    //==================================================================================================================
    // Helpers
    //==================================================================================================================

}
