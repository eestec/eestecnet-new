<?php

namespace Tests\Integ\Auth;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthApiTest extends TestCase
{

    use DatabaseTransactions; // Does not truncate actual database

    /**
     * @var Collection|Model|mixed
     */
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        // Create a fake user
        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function testLoginSuccess()
    {
        $credentials = ['email' => $this->user->email,
                        'password' => 'password'];
        $response = $this->post('/api/auth/login', $credentials);
        // dd($credentials);
        $response->assertStatus(200)
            ->assertJson([
                    'message' => 'Authorized.'
            ]);
    }

    /** @test */
    public function testLoginFail()
    {
        $credentials = ['email' => $this->user->email,
                        'password' => 'wrong_password'];
        $response = $this->post('/api/auth/login', $credentials);
        $response->assertStatus(401)
            ->assertJson([
                    'errors' => ['Credentials were incorrect.']
            ]);
    }

    /** @test */
    // TODO: Find out why the test is not working, probably an JWT issue
    // public function testLogout()
    // {
    //     $this->be($this->user);
    //     $response = $this->post('/api/auth/logout');
    //     dd($response);
    //     $response->assertStatus(200)
    //         ->assertJson([
    //                 'message' => 'Successfully logged out.'
    //         ]);
    // }

    /** @test */
    public function testRegisterSuccess()
    {
        $credentials = [
            'name'                  => 'John',
            'full_name'             => 'John Doe',
            'date_of_birth'         => '1997-10-15',
            'email'                 => 'johndoe@example.com',
            'password'              => 'password',
            'password_confirmation' => 'password',
            'terms'                 => 'terms'
        ];
        $response = $this->post('/api/auth/register', $credentials);
        $response->assertStatus(200)
            ->assertJson([
                    'message' => 'Successful registration.'
            ]);
    }

    /** @test */
    public function testRegisterFail()
    {
        $credentials = [
            'name'                  => 'John',
            'full_name'             => 'John Doe',
            'date_of_birth'         => '1997-10-15',
            'email'                 => 'johndoe@example.com',
            'password'              => 'password',
            'password_confirmation' => 'wrong_password',
            'terms'                 => 'terms'
        ];
        $response = $this->post('/api/auth/register', $credentials);
        $response->assertStatus(422);
    }

    /** @test */
    public function testForgotPasswordSuccess()
    {
        $credentials = [
            'email' => $this->user->email,
        ];
        $response = $this->post('/api/auth/forgot-password', $credentials);
        $response->assertStatus(200)
            ->assertJson([
                    'message' => 'Email reset link sent.'
            ]);
    }

    /** @test */
    public function testForgotPasswordFail()
    {
        $credentials = [
            'email' => 'wrong@email.com',
        ];
        $response = $this->post('/api/auth/forgot-password', $credentials);
        $response->assertStatus(422)
            ->assertJson([
                    'errors' => ['email' => ['We couldn\'t find an account with that email.']]
            ]);
    }

    /** @test */
    // TODO: Fix, returns server error (500)
    // public function testResetPasswordSuccess()
    // {
    //     $credentials = [
    //         'email' => $this->user->email,
    //     ];
    //     $response = $this->post('/api/auth/forgot-password', $credentials);
    //     $token = DB::table('password_resets')->where('email', $this->user->email)->first('token');
    //     $credentials = [
    //         'id'                    => $this->user->id,
    //         'token'                 => $token->token,
    //         'password'              => 'new_password',
    //         'password_confirmation' => 'new_password',
    //     ];
    //     $response = $this->post('/api/auth/reset-password', $credentials);
    //     $response->assertStatus(200)
    //         ->assertJson([
    //                 'message' => 'Password reset successful.'
    //         ]);
    // }

}
