# Linux Installation Instructions

## 0. Install basic tools

### 1. Install [Git](https://git-scm.com)

### 2. Install Docker

1. Select your server (i.e. Debian, Fedora, Ubuntu) from [here](https://docs.docker.com/engine/install/#server).
2. Follow the instructions on sections `SET UP THE REPOSITORY` and `INSTALL DOCKER ENGINE`.

## 1. Clone this repository

```bash
git clone https://gitlab.com/eestec/eestecnet-new.git

cd eestecnet-new
```

## 2. Build docker container for local deployment

The local deployment container consists of the following services (see [docker-compose.yml](docker-compose.yml)):

| Container name | Image                 | Purpose, description |
|:---------------|:----------------------|:---------------------|
| app            | php:7-fpm             | This service is the base having everything in this directory mounted to its `\var\www` directory, serving the Laravel application with FastCGI Process Manager (FPM) to Nginx. |
| composer       | composer:latest       | This is needed for the installation of backend dependencies. |
| yarn           | node:14-buster-slim   | We use Yarn for frontend dependency management. |
| artisan        |                       | Alias to `app`, this way it is easier to run [artisan](https://laravel.com/docs/7.x/artisan) commands with `docker-compose`. |
| nginx          | nginx:stable-alpine   | On the production server we have Nginx already, so it is better to use that in local deployment as well. |
| db             | postgres:alpine       | Same as `nginx`. |
| pgadmin        | dpage/pgadmin4:latest | Optional service for database management. |

Before starting the containers, you need to have the .env file set up for the database to pull the variables

```bash
cp .env.example .env
```

Build them (first time it runs for long)

```bash
docker-compose build --build-arg USER_ID=$(id -u ${USER}) --build-arg GROUP_ID=$(id -g ${USER})
```

Start them (first time it runs for long as well)

```bash
docker-compose up -d app db nginx
```

> In case you've omitted the `-d` option don't close this console instance just if you wanted to stop them (with one <kbd>Ctrl</kbd> + <kbd>C</kbd> all the running containers shut down).

## 3. Install a database manager

You can start a [pgAdmin](https://www.pgadmin.org) service in docker, or use any other preferred tool instead (
eg. [DBeaver](https://dbeaver.io)).

### Setting up pgAdmin (optional)

1. Start the pgadmin service (first time it runs for long)

   ```bash
   docker-compose up -d pgadmin
   ```

2. Go to go to [localhost:5050](localhost:5050) in a browser

3. Log in using email `admin@admin.com` and password `secret`

4. Add a new server with the following settings

On the General tab

	| Name     | Value                                             |
	|:---------|:--------------------------------------------------|
	| Name     | LOCAL eestecnet (or anything else you would like) |

On the Connection tab:

	| Name     | Value                                             |
	|:---------|:--------------------------------------------------|
	| Host     | value of DB_HOST from your .env file              |
	| Port     | value of DB_PORT from your .env file              |
    | Database | value of DB_DATABASE from your .env file          |
    | Username | value of DB_USERNAME from your .env file          |
	| Password | value of DB_PASSWORD from your .env file          |
    
    Checking "Save password" can be useful before clicking on <kbd>Save</kbd>

5. Create a Database named `eestecnet` in that server (if not exists already):

    - right click Databases -> Create -> Database...
    - name it `eestecnet`

   > The tables after migration are available here: Databases -> LOCAL eestecnet (name of your server set previously) -> Databases -> eestecnet -> Schemas -> public -> Tables

## 4. Configure project

Install backend dependencies with composer (long running task when the vendor directory does not exist)

```bash
docker-compose run --rm composer install
```

Generate encryption key for Laravel with artisan

```bash
docker-compose run --rm artisan key:generate
```

Generate JWT secret key

```bash
docker-compose run --rm artisan jwt:secret
```

Run database migrations

```bash
docker-compose run --rm artisan migrate
```

If you need dummy data found [here](https://gitlab.com/eestec/eestecnet-new/-/tree/develop/database/seeds) to test your
database, import database seed

```bash
docker-compose run --rm artisan db:seed
```

Run tests

```bash
docker-compose run --rm artisan test
```

## 5. Download NodeJS dependencies and build frontend app

Download and install frontend dependencies with Yarn (long running task when the node_modules directory does not exist)

```bash
docker-compose run --rm yarn install
```

Build frontend for development

```bash
docker-compose run --rm yarn run dev
```

Build frontend with live reload (keep this instance of console open until you want this running)

```bash
docker-compose run --rm yarn run watch
```

OR

```bash
docker-compose run --rm yarn run watch-poll
```
