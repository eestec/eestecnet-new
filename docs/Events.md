# Events API Overview

## Query events

These endpoints enable users to view the events that are stored in the database. Anyone can access these endpoints.

### Get all events

`GET api/events`

Returns a list of all the events that are stored in the database.

### Get open events

`GET api/events/open`

Returns a list of all the events that are currently open for application.

### Get events that are in progress

`GET api/events/current`

Returns a list of all the events that are in progress. An event is considered as "in progress" if the deadline for
application is passed but the event is not yet finished.

### Get event details

`GET api/events/{event}`

Returns details about the specified event.

## Event participation

These endpoints allow registered users to apply to events, update their application, track their application status,
confirm their participation once they are accepted and fill in their transportation details and food preferences.

### Apply to event

`POST api/events/{event}/apply`

Creates a new event application with the motivational letter provided in the request body ('letter').

### Update application

`PATCH(PUT) api/events/{event}/applications/{application}`

Only the motivational letter can be modified. Users can only modify their own applications.

### Get motivational letter

`GET api/events/{event}/applications/{application}/letter`

Returns a user's motivational letter. Used when a user wants to modify their event application.

### Cancel application

`DELETE api/events/{event}/applications/{application}`

An application cannot be cancelled after the deadline. Users can only cancel their own applications.

### Confirm participation

`POST api/events/{event}/applications/{application}/confirm`

Confirms an user's participation in an event. Users can only confirm their own applications. Users can only confirm
their participation after the deadline if they are accepted.

### Submit participant&#039;s details

`POST api/events/{event}/applications/{application}/details`

Stores a participant's details in the database. Request
body: `[application_id, arrival, departure, arrival_location, means, allergies, food_prefs, other]`

## Event organizer actions

These endpoints enable event organizers to manage their events.

### Get all event applications

`GET api/events/{event}/applications`

Returns a list of all applications for an event. Only event organizers can access.

### Get applicaiton

`GET api/events/{event}/applications/{application}`

Returns the specified application. Only event organizers can access.

### Accept or decline a list of applications

`POST api/events/{event}/applications/accept`

The request body can contain two lists: accept_ids and decline_ids. All applications from accept_ids are accepted and
all applications from decline_ids are declined.

### Accept application

`POST api/events/{event}/applications/{application}/accept`

Sets an application as accepted.

### Decline application

`POST api/events/{event}/applications/{application}/decline`

Sets an application as not accepted.

### Get participant details

`GET api/events/{event}/applications/{application}/details`

Returns a participant's transportation details and food preferences.

## Event administration

These endpoints enable committee administrators to register new events, as well as to update and delete them

### Create event

`POST api/events/`

Creates a new event and stores it in the database.

**Request body:**

Field           |Type
----------------|----------
name            |`string`
type            |`[ws, op, ex, cg]`
thumbnail       |`string`
location        |`string`
description     |`string`
max_pax         |`int > 0`
fee             |`int > 0`
starts          |`timestamp`
ends            |`timestamp`
deadline        |`timestamp`
committee_ids   |`array`

### Delete event

`DELETE api/events/{event}`

Deletes an event from the database.

### Update event

`PATCH (PUT) api/events/{event}`

Updates an event.
