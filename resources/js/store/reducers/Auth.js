import * as ActionTypes from '../action-types';
import Http from '../../Http';

const defaultUser = {
    name: null,
    full_name: null,
    slug: null,
    thumbnail: null,
};

const initialState = {
    isAuthenticated: false,
    user: defaultUser,
};

const authLogin = (state, payload) => {
    const { access_token: token, user } = payload;
    if (token && user) {
        localStorage.setItem('access_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        Http.defaults.headers.common.Authorization = `Bearer ${token}`;
        return Object.assign({}, state, {
            isAuthenticated: true,
            user,
        });
    } else {
        Http.defaults.headers.common.Authorization = null;
        localStorage.removeItem('access_token');
        localStorage.removeItem('user');
        return Object.assign({}, state, {
            isAuthenticated: false,
            user: defaultUser,
        });
    }
};

const checkAuth = (state) => {
    const stateObj = Object.assign({}, state, {
        isAuthenticated: !!localStorage.getItem('access_token'),
        user: JSON.parse(localStorage.getItem('user')),
    });
    if (state.isAuthenticated) {
        Http.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('access_token')}`;
    } else {
        Http.defaults.headers.common.Authorization = null;
    }
    return stateObj;
};

const logout = (state) => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('user');
    Http.defaults.headers.common.Authorization = null;
    return Object.assign({}, state, {
        isAuthenticated: false,
        user: defaultUser,
    });
};

const Auth = (state = initialState, { type, payload = null }) => {
    switch (type) {
        case ActionTypes.AUTH_LOGIN:
            return authLogin(state, payload);
        case ActionTypes.AUTH_CHECK:
            return checkAuth(state);
        case ActionTypes.AUTH_LOGOUT:
            return logout(state);
        default:
            return state;
    }
};

export default Auth;
