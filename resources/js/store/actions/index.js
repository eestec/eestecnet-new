import * as ActionTypes from '../action-types';

export function authLogin(payload) {
    return {
        type: ActionTypes.AUTH_LOGIN,
        payload,
    };
}

export function authLogout() {
    return {
        type: ActionTypes.AUTH_LOGOUT,
    };
}

export function authCheck() {
    return {
        type: ActionTypes.AUTH_CHECK,
    };
}

export function userFetch(payload) {
    return {
        type: ActionTypes.USER_FETCH,
        payload,
    };
}

export function userUpdate(payload) {
    return {
        type: ActionTypes.USER_UPDATE,
        payload,
    };
}

export function userRemove(payload) {
    return {
        type: ActionTypes.USER_REMOVE,
        payload,
    };
}
