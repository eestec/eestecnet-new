import Home from '../pages/home-page/Home';
import Login from '../pages/login/Login';
import Register from '../pages/register/Register';
import ForgotPassword from '../pages/forgot-password/ForgotPassword';
import ResetPassword from '../pages/reset-password/ResetPassword';
import Profile from '../pages/profile/Profile';
import Events from '../pages/events/Events';
import NoMatch from '../components/utils/NoMatch';
import About from '../pages/about/About';
import Activities from '../pages/activities/Activities';
import ForStudents from '../pages/for-students/ForStudents';
import Partners from '../pages/partners/Partners';
import Career from '../pages/career/Career';
import Contact from '../pages/contact/Contact';
import Branches from '../pages/branches/Branches';
import Teams from '../pages/teams/Teams';

const routes = [
    {
        path: '/',
        exact: true,
        auth: false,
        //component: Dashboard,
        //fallback: Home,
        component: Home,
    },
    {
        path: '/about',
        exact: true,
        auth: false,
        component: About,
    },
    {
        path: '/activities',
        exact: true,
        auth: false,
        component: Activities,
    },
    {
        path: '/for-students',
        exact: true,
        auth: false,
        component: ForStudents,
    },
    {
        path: '/partners',
        exact: true,
        auth: false,
        component: Partners,
    },
    {
        path: '/career',
        exact: true,
        auth: false,
        component: Career,
    },
    {
        path: '/contact',
        exact: true,
        auth: false,
        component: Contact,
    },
    {
        path: '/login',
        exact: true,
        auth: false,
        component: Login,
    },
    {
        path: '/register',
        exact: true,
        auth: false,
        component: Register,
    },
    {
        path: '/forgot-password',
        exact: true,
        auth: false,
        component: ForgotPassword,
    },
    {
        path: '/reset-password',
        exact: true,
        auth: false,
        component: ResetPassword,
    },
    {
        path: '/profile',
        exact: true,
        auth: true,
        component: Profile,
    },
    {
        path: '/events',
        exact: true,
        auth: true,
        component: Events,
    },
    {
        path: '/teams',
        exact: true,
        auth: true,
        component: Teams,
    },
    {
        path: '/branches',
        exact: true,
        auth: true,
        component: Branches,
    },  
    {
        path: '',
        exact: false,
        auth: false,
        component: NoMatch,
    },
];

export default routes;
