import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';
import { connect } from 'react-redux';

const SplitRoute = ({
    component: Component, fallback: Fallback, isAuthenticated, ...rest
}) => (
    <Route
        {...rest}
        render={props => (
            isAuthenticated ? (
                <Component {...props} />
            ) : (
                <Fallback {...props} />
            )
        )}
    />
);

SplitRoute.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
};


const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
});

export default connect(mapStateToProps)(SplitRoute);
