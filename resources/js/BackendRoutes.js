// export const API_BASE_URL = 'http://localhost:8000/api';
export const API_ENDPOINT = () => '/api';

export const API_AUTH_REGISTER = () => API_ENDPOINT() + '/auth/register';
export const API_AUTH_LOGIN = () => API_ENDPOINT() + '/auth/login';
export const API_AUTH_LOGOUT = () => API_ENDPOINT() + '/auth/logout';
export const API_AUTH_PW_FORGOT = () => API_ENDPOINT() + '/auth/forgot-password';
export const API_AUTH_PW_RESET = () => API_ENDPOINT() + '/auth/reset-password';

export const API_THUMBNAIL_RESOURCE = type => API_ENDPOINT() + '/thumbnail/' + type;
export const API_USER_RESOURCE = () => API_ENDPOINT() + '/user';
export const API_EVENT_RESOURCE = () => API_ENDPOINT() + '/events';
export const API_BRANCHES_RESOURCE = () => API_ENDPOINT() + '/branches';
export const API_BRANCHES_RESOURCE_ALL = () => API_ENDPOINT() + '/allbranches';
export const API_TEAMS_RESOURCE = () => API_ENDPOINT() + '/teams';
