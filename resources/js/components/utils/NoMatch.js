import React, {Component} from "react";
import {compose} from "recompose";
import withStyles from "@material-ui/core/styles/withStyles";
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from "@material-ui/core/Box";

const styles = theme => ({
    wrapper: {
        height: '100%',
        display: 'flex',
        flexFlow: 'column',
        marginTop: 200
    },
    infobox: {
        textAlign: 'center',
        justifyContent: 'center',
        display: 'flex',
        flexFlow: 'column',
        flexGrow: 1,
    }
});

class NoMatch extends Component {

    render() {
        const {classes} = this.props;

        return (
            <Container>
                <div className={classes.wrapper}>
                    <div className={classes.infobox}>
                        <Typography variant="h2" color="textSecondary" gutterBottom>
                            <Box fontWeight="fontWeightBold">
                                404
                            </Box>
                            <Box fontWeight="fontWeightBold">
                                Page not found.
                            </Box>
                        </Typography>
                    </div>
                </div>
            </Container>
        );
    }
}

export default compose(
    withStyles(styles)
)(NoMatch);
