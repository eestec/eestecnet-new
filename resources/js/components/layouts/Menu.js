const Menu = [
    {
        label: "About",
        pathname: "/about",
        auth: false
    },
    {
        label: "Activities",
        pathname: "/activities",
        auth: false
    },
    {
        label: "For students",
        pathname: "/for-students",
        auth: false
    },
    {
        label: "Partners",
        pathname: "/partners",
        auth: false
    },
    {
        label: "Career",
        pathname: "/career",
        auth: false
    },
    {
        label: "Contact",
        pathname: "/contact",
        auth: false
    },
    {
        label: "Profile",
        pathname: "/profile",
        auth: true
    },
    {
        label: "Logout",
        pathname: "/logout",
        auth: true
    },
];

export default Menu;
