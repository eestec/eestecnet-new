import Header from './Header';
import Menu from './Menu';
import React from "react";
import Footer from "./Footer";

const withLayout = (Component) => (props) => (
    <React.Fragment>
        <Header/>
        <Component {...props} />
        <Footer/>
    </React.Fragment>
);

export {
    Header,
    Footer,
    Menu,
    withLayout
}
