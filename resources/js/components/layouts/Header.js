import React, {Component} from "react";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Menu from "./Menu";
import {compose} from "recompose";
import {Link, withRouter} from "react-router-dom";
import {connect} from 'react-redux';
import * as actions from '../../store/actions';
import withStyles from "@material-ui/core/styles/withStyles";
import {Toolbar, Typography, Container, IconButton, SwipeableDrawer, List, ListItem, ListItemText} from "@material-ui/core";
import logo from '../../../images/logo_white.png';
import {Search as SearchIcon, Menu as MenuIcon} from '@material-ui/icons';

const styles = theme => ({
    appBar: {
        boxShadow: 'none',
        paddingTop: theme.spacing(1) + 'px'
    },
    tabsWrapper: {
        display: "flex"
    },
    link: {
        textDecoration: "none",
        color: "inherit",
        display: "block",
        '& img': {
            display: "block",
            maxWidth: "100%"
        }
    },
    logoWrapper: {
        marginRight: theme.spacing(6) + 'px'
    },
    tabs: {
        flexGrow: 1,
        [theme.breakpoints.down("sm")]: {
            display: "none"
        }
    },
    tabItem: {
        minWidth: "auto"
    },
    flexContainer: {
        display: "flex",
        width: "100%",
        alignItems: "center",
        boxSizing: "border-box"
    },
    iconContainer: {
        display: "none",
        marginRight: theme.spacing(2) + 'px',
        [theme.breakpoints.down("sm")]: {
            display: "block"
        }
    },
    searchWrapper: {
        [theme.breakpoints.down("sm")]: {
            flexGrow: 1,
            display: 'flex',
            justifyContent: 'flex-end'
        }
    }
});

class Header extends Component {
    state = {
        value: 0,
        menuDrawer: false,
        // isAuthenticated: null,
        // user: null
    };

    handleLogout = (label) => {
        return (e) => {
            if (label === 'Logout') {
                e.preventDefault();
                this.props.dispatch(actions.authLogout());
            }
        }
    }

    handleChange = (event, value) => {
        this.setState({value});
    };

    currentValue = () => {
        for (let i = 0; i < Menu.length; i++) {
            let menuItem = Menu[i];
            if (menuItem.pathname === this.props.location.pathname) {
                return i;
            }
        }
    };

    mobileMenuOpen = event => {
        this.setState({menuDrawer: true});
    };

    mobileMenuClose = event => {
        this.setState({menuDrawer: false});
    };

    render() {
        const {value} = this.state;
        const {classes} = this.props;

        return (
            <React.Fragment>
                <AppBar className={classes.appBar} position="absolute" color="transparent">
                    <Toolbar>
                        <Container>
                            <div className={classes.flexContainer}>
                                <div className={classes.iconContainer}>
                                    <IconButton
                                        onClick={this.mobileMenuOpen}
                                        color="secondary"
                                        aria-label="Menu"
                                    >
                                        <MenuIcon/>
                                    </IconButton>
                                </div>

                                <Typography variant="h6" color="inherit" noWrap className={classes.logoWrapper}>
                                    <Link to="/" className={classes.link}>
                                        <img width={100} src={logo} alt="EESTEC logo"/>
                                    </Link>
                                </Typography>

                                <SwipeableDrawer
                                    anchor="left"
                                    open={this.state.menuDrawer}
                                    onClose={this.mobileMenuClose}
                                    onOpen={this.mobileMenuOpen}
                                >
                                    <AppBar title="Menu"/>
                                    <List>
                                        {Menu.map((item, index) => {
                                            if (item.auth === false || this.props.isAuthenticated) {
                                                return (
                                                    <ListItem
                                                        key={index}
                                                        component={Link}
                                                        to={{pathname: item.pathname}}
                                                        button
                                                    >
                                                        <ListItemText primary={item.label}/>
                                                    </ListItem>
                                                );
                                            }
                                        })}
                                    </List>
                                </SwipeableDrawer>

                                <Tabs
                                    value={this.currentValue() || value}
                                    onChange={this.handleChange}
                                    aria-label="menu tabs"
                                    variant="fullWidth"
                                    indicatorColor="primary"
                                    textColor="primary"
                                    className={classes.tabs}
                                >
                                    {Menu.map((item, index) => {
                                        if (item.auth === false || this.props.isAuthenticated) {
                                            return (
                                                <Tab
                                                    key={index}
                                                    component={Link}
                                                    onClick={this.handleLogout(item.label)}
                                                    to={{pathname: item.pathname}}
                                                    label={item.label}
                                                    className={classes.tabItem}
                                                />
                                            );
                                        }
                                    })}
                                </Tabs>
                                <div className={classes.searchWrapper}>
                                    <IconButton aria-label="search" color="secondary">
                                        <SearchIcon/>
                                    </IconButton>
                                </div>
                            </div>
                        </Container>
                    </Toolbar>
                </AppBar>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
    user: state.Auth.user,
});

export default compose(
    connect(mapStateToProps),
    withRouter,
    withStyles(styles)
)(Header);
