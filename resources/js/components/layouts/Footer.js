import React, {Component} from 'react';
import {compose} from 'recompose';
import withStyles from '@material-ui/core/styles/withStyles';
import logo from '../../../images/logo_red.png';
import {Typography, Container, Link, Grid, List, ListItem, IconButton, Box} from '@material-ui/core';
import {Instagram, Facebook, LinkedIn} from '@material-ui/icons';
import moment from 'moment';

const styles = theme => ({
    footer: {
        height: 'auto',
        paddingTop: theme.spacing(6) + 'px',
        paddingBottom: theme.spacing(4) + 'px'
    },
    logoContainer: {
        display: 'flex',
        '& > img': {
            marginRight: theme.spacing(2) + 'px'
        },
        '& > span': {
            alignItems: 'center',
            display: 'flex'
        },
        marginBottom: theme.spacing(5) + 'px'
    },
    footerMenu: {
        marginBottom: theme.spacing(7) + 'px'
    },
    copyrightsAndSocial: {
        display: 'flex',
        [theme.breakpoints.up('xs')]: {
            flexDirection: 'row'
        },
        [theme.breakpoints.down('xs')]: {
            flexDirection: 'column'
        }
    },
    copyrights: {
        display: 'flex',
        flexGrow: 1,
        alignItems: 'center',

        '& a': {
            marginLeft: theme.spacing(1) + 'px'
        }
    },
    social: {
        display: 'flex',
        alignItems: 'center'
    },
    socialIcons: {
        display: 'flex'
    }
});

class Footer extends Component {
    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <footer className={classes.footer}>
                    <Container>
                        <div className={classes.logoContainer}>
                            <img width={100} src={logo} alt="EESTEC logo"/>
                            <Typography variant="caption" color="textPrimary" component="span"> Electrical Engineering STudents' European assoCiation </Typography>
                        </div>
                        <Grid container className={classes.footerMenu}>
                            <Grid container item lg sm={4} xs={6} direction="column" alignItems="center">
                                <Box direction="column">
                                    <Typography variant="h6" color="textSecondary"> About EESTEC </Typography>
                                    <List dense={true}>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">News</Link></ListItem>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">History</Link></ListItem>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">Map of Commitments</Link></ListItem>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">Official Documents</Link></ListItem>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">Governing Bodies</Link></ListItem>
                                    </List>
                                </Box>
                            </Grid>
                            <Grid container item lg sm={4} xs={6} direction="column" alignItems="center">
                                <Box direction="column">
                                    <Typography variant="h6" color="textSecondary"> Activities </Typography>
                                    <List dense={true}>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">International teams</Link></ListItem>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">Events</Link></ListItem>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">EESTEC Conference</Link></ListItem>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">EESTEC Competition for Android</Link></ListItem>
                                    </List>
                                </Box>
                            </Grid>
                            <Grid container item lg sm={4} xs={6} direction="column" alignItems="center">
                                <Box direction="column">
                                    <Typography variant="h6" color="textSecondary"> For Students </Typography>
                                        <List dense={true}>
                                            <ListItem disableGutters><Link href="#" color="textPrimary">Become a Member</Link></ListItem>
                                            <ListItem disableGutters><Link href="#" color="textPrimary">EESTEC in your city</Link></ListItem>
                                            <ListItem disableGutters><Link href="#" color="textPrimary">Found an Observer</Link></ListItem>
                                        </List>
                                </Box>
                            </Grid>
                            <Grid container item lg sm={4} xs={6} direction="column" alignItems="center">
                                <Box direction="column">
                                    <Typography variant="h6" color="textSecondary"> Partners </Typography>
                                    <List dense={true}>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">Cooperations</Link></ListItem>
                                        <ListItem disableGutters><Link href="#" color="textPrimary">Become a Partner</Link></ListItem>
                                    </List>
                                </Box>
                            </Grid>
                            <Grid container item lg sm={4} xs={6} direction="column" alignItems="center">
                                <Box direction="column">
                                    <Typography variant="h6" color="textSecondary"> Contact Info </Typography>
                                    <List dense={true}>
                                        <ListItem disableGutters>EESTEC International</ListItem>
                                        <ListItem disableGutters>Mekelweg 4, 2628 CD Delft,</ListItem>
                                        <ListItem disableGutters>The Netherlands</ListItem>
                                        <ListItem disableGutters>web: eestec.net</ListItem>
                                        <ListItem disableGutters>email: board@eestec.net</ListItem>
                                    </List>
                                </Box>
                            </Grid>
                        </Grid>
                        <div className={classes.copyrightsAndSocial}>
                            <div className={classes.copyrights}>
                                <Typography variant="caption" color="textPrimary"> Copyright {moment().year()} EESTEC. All rights reserved </Typography>
                                <Typography variant="caption" color="textPrimary">
                                    <Link href="#" color="inherit">Credits</Link>
                                </Typography>
                                <Typography variant="caption" color="textPrimary">
                                    <Link href="#" color="inherit">Privacy</Link>
                                </Typography>
                            </div>
                            <div className={classes.social}>
                                <Typography variant="h5" color="textSecondary"> Social </Typography>
                                <div className={classes.socialIcons}>
                                    <IconButton href="//facebook.com/eestec" target="_blank" color="secondary"><Facebook/></IconButton>
                                    <IconButton href="//instagram.com/eestec" target="_blank" color="secondary"><Instagram/></IconButton>
                                    <IconButton href="//linkedin.com/company/eestec" target="_blank" color="secondary"><LinkedIn/></IconButton>
                                </div>
                            </div>
                        </div>
                    </Container>
                </footer>
            </React.Fragment>
        );
    }
}

export default compose(
    withStyles(styles)
)(Footer);
