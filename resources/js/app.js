import '../sass/app.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {QueryParamProvider} from 'use-query-params';
import { Provider } from 'react-redux';
import Routes from './routes';
import store from './store';
import * as action from './store/actions';
import CssBaseline from '@material-ui/core/CssBaseline';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import {ThemeProvider} from '@material-ui/core/styles';
import {grey} from '@material-ui/core/colors';

const theme = createMuiTheme({
    palette: {
        primary: {
            50: '#fce5e6',
            100: '#f7bfc1',
            200: '#f29598',
            300: '#ed6a6e',
            400: '#e94a4f',
            500: '#e52a30',
            600: '#e2252b',
            700: '#de1f24',
            800: '#da191e',
            900: '#d30f13',
            A100: '#ffffff',
            A200: '#ffcdce',
            A400: '#ff9a9b',
            A700: '#ff8082',
            'contrastDefaultColor': 'light',
        },
        secondary: {
            main: '#fff'
        },
        text: {
            primary: grey[800],
            // TODO: white text is not visible on white background, so move white text color to components with dark bg
            secondary: grey[400],
        },
        background: {
            // TODO: use light bg by default
            default: '#0a0a0a'
        }
    },
    typography: {
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
    }
});

store.dispatch(action.authCheck());

ReactDOM.render(
    <Provider store={store}>
        <React.StrictMode>
            <ThemeProvider theme={theme}>
                <React.Fragment>
                    <CssBaseline/>
                    <Router>
                        <QueryParamProvider ReactRouterRoute={Route}>
                            <Switch>
                                <Routes />
                            </Switch>
                        </QueryParamProvider>
                    </Router>
                </React.Fragment>
            </ThemeProvider>
        </React.StrictMode>
    </Provider>,
    document.getElementById('app'),
);
