import Http from '../Http';
import * as action from '../store/actions';
import { API_USER_RESOURCE } from '../BackendRoutes';

export function fetch(id) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.get(`${API_USER_RESOURCE()}/${id}`)
                .then((res) => {
                    dispatch(action.userFetch(res.data));
                    return resolve({
                        status: res.status,
                        message: res.statusText,
                        user: res.data.user,
                        options: res.data.options,
                    });
                })
                .catch((err) => {
                    const { status, errors } = err.response.data;
                    const data = {
                        status,
                        errors,
                    };
                    return reject(data);
                });
        })
    );
}

export function update(id, data) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.patch(`${API_USER_RESOURCE()}/${id}`, data)
                .then((res) => {
                    dispatch(action.userUpdate(res.data));
                    return resolve(res.data);
                })
                .catch((err) => {
                    console.error(err);
                    const { status, errors } = err.response.data;
                    const data = {
                        status,
                        errors,
                    };
                    return reject(data);
                });
        })
    );
}

export function remove(id) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.delete(`${API_USER_RESOURCE()}/${id}`)
                .then((res) => {
                    dispatch(action.userRemove(res.data));
                    return resolve(res.data);
                })
                .catch((err) => {
                    const { status, errors } = err.response.data;
                    const data = {
                        status,
                        errors,
                    };
                    return reject(data);
                });
        })
    );
}
