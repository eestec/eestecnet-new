import Http from '../Http';
import * as action from '../store/actions';
import { API_AUTH_LOGIN, API_AUTH_REGISTER, API_AUTH_PW_RESET, API_AUTH_PW_FORGOT } from '../BackendRoutes';

export function login(credentials) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.post(API_AUTH_LOGIN(), credentials)
                .then((res) => {
                    dispatch(action.authLogin(res.data));
                    return resolve();
                })
                .catch((err) => {
                    const { status, errors } = err.response.data;
                    const data = {
                        status,
                        errors,
                    };
                    return reject(data);
                });
        })
    );
}

export function register(credentials) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.post(API_AUTH_REGISTER(), credentials)
                .then(res => resolve(res.data))
                .catch((err) => {
                    const { status, errors } = err.response.data;
                    const data = {
                        status,
                        errors,
                    };
                    return reject(data);
                });
        })
    );
}

export function resetPassword(credentials) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.post(API_AUTH_PW_FORGOT(), credentials)
                .then(res => resolve(res.data))
                .catch((err) => {
                    const { status, errors } = err.response.data;
                    const data = {
                        status,
                        errors,
                    };
                    return reject(data);
                });
        })
    );
}

export function updatePassword(credentials) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.post(API_AUTH_PW_RESET(), credentials)
                .then((res) => {
                    const { status } = res.data.status;
                    if (status === 202) {
                        const data = {
                            error: res.data.message,
                            status,
                        };
                        return reject(data);
                    }
                    return resolve(res);
                })
                .catch((err) => {
                    const { status, errors } = err.response.data;
                    const data = {
                        status,
                        errors,
                    };
                    return reject(data);
                });
        })
    );
}
