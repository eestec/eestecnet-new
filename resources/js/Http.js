import axios from 'axios';
import store from './store';
import * as actions from './store/actions';

const token = localStorage.getItem('access_token');
if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
}

axios.interceptors.response.use(
    response => response,
    (error) => {
        console.error(error.response);
        if (error.response.status === 401) {
            store.dispatch(actions.authLogout());
        }
        return Promise.reject(error);
    },
);

export default axios;
