import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {Redirect} from 'react-router-dom';
import ReeValidate from 'ree-validate';
import { UserService } from '../../services';
import moment from 'moment';
import { API_THUMBNAIL_RESOURCE } from '../../BackendRoutes';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

import {Container, Table, Button, Badge, Card} from "@material-ui/core";
import {compose} from "recompose";
import {withLayout} from "../../components/layouts";
import Typography from "@material-ui/core/Typography";
import Alert from "@material-ui/lab/Alert";
import withStyles from "@material-ui/core/styles/withStyles";

// TODO: show email verification page if not verified
class Profile extends Component {

    // Image crop repo: https://github.com/DominicTobias/react-image-crop

    constructor(props) {
        super(props);

        this.THUMBNAIL_FALLBACK_URL = '/img/profile.svg';

        ReeValidate.extend('string', (value) => {
            return value != null && (typeof value) === (typeof 'string');
        });
        ReeValidate.localize('en', {
            messages: {
                'string': function (field) {
                    return ('The content of ' + field + ' should be a string.');
                },
            },
        });

        this.validator = new ReeValidate({
            name: 'required|min:3|max:255',
            full_name: 'required|min:3|max:255',
            dateOfBirth: 'required|date_format:YYYY-MM-DD|before:' + moment().subtract(16, 'years').format('YYYY-MM-DD'),
            email: 'required|email|max:255',
            gender: 'required|string',
            mobile: 'nullable|phone',
            tshirt: 'required|string',
            picture: 'nullable|file|max:2048|dimensions:min_width=250,min_height=250',
            foodPref: 'required|string',
            fos: 'required|string',
            deleteConfirm: 'required|string',
        });

        this.state = {
            loading: false,
            editing: false,
            deleteRequested: false,
            optionValues: {},
            errors: {},
            updated: false,
            response: {
                error: false,
                message: '',
            },
            hashid: '',
            gender: '',
            fos: '',
            tshirt: '',
            foodPref: '',
            name: '',
            full_name: '',
            dateOfBirth: null,
            email: '',
            mobile: '',
            passportNumber: '',
            allergies: '',
            joinedOn: null,
            thumbnailUrl: null,
            pictureMethod: 'keep',
            croppingPicture: false,
            draggingCrop: false,
            picture: null,
            croppedPicture: null,
            crop: {},
            percentCrop: {
                unit: '%',
                width: 100,
                aspect: 1,
            },
        };
    }

    capitalize = (s) => {
        if (typeof s !== 'string') return '';
        return s.charAt(0).toUpperCase() + s.slice(1);
    }

    uppercase = (s) => {
        if (typeof s !== 'string') return '';
        return s.toUpperCase();
    }

    componentDidMount() {
        this.setState({ loading: true });

        this.props.dispatch(UserService.fetch(this.props.user.id))
            .then((res) => {
                const response = {
                    error: false,
                    message: res.message,
                };
                const { hashid, gender, fos, tshirt, food_pref, name, full_name, date_of_birth, email, email_verified_at, mobile, passport_no, allergies, joined_on, thumbnail } = res.user;
                this.setState({
                    loading: false, success: true, response, optionValues: res.options,
                    hashid,
                    gender,
                    fos,
                    tshirt,
                    foodPref: food_pref,
                    name,
                    full_name,
                    dateOfBirth: moment(date_of_birth),
                    email,
                    isEmailVerified: email_verified_at !== null,
                    mobile,
                    passportNumber: passport_no,
                    allergies,
                    joinedOn: moment.utc(joined_on),
                    thumbnailUrl: thumbnail !== null ? API_THUMBNAIL_RESOURCE('user') + '?id=' + hashid : null,
                });
            })
            .catch((err) => {
                console.error(err);
                const errors = Object.values(err.errors);
                errors.join(' ');
                const response = {
                    error: true,
                    message: errors,
                };
                this.setState({ response });
                this.setState({ loading: false });
            });
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });

        // If a field has a validation error, we'll clear it when corrected
        const { errors } = this.state;
        if (name in errors) {
            const validation = this.validator.errors;
            this.validator.validate(name, value).then(() => {
                if (!validation.has(name)) {
                    delete errors[name];
                    this.setState({ errors });
                }
            });
        }
    }

    handleBlur = (e) => {
        const { name, value } = e.target;
        const validation = this.validator.errors;

        // Avoid validation until input has a value
        if (value === '') {
            return;
        }

        this.validator.validate(name, value).then(() => {
            if (validation.has(name)) {
                const { errors } = this.state;
                errors[name] = validation.first(name);
                this.setState({ errors });
            }
        });
    }

    handleNavSelect = (key) => {
        this.setState({ pictureMethod: key });
    }

    handlePictureChange = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () => this.setState({ croppingPicture: true, croppedPicture: null, picture: reader.result }));
            reader.readAsDataURL(e.target.files[0]);
        }
    }

    onPictureLoaded = (image) => {
        this.imageRef = image;

        // Center a square percent crop
        const width = image.width > image.height ? (image.height / image.width) * 100 : 100;
        const height = image.height > image.width ? (image.width / image.height) * 100 : 100;
        const x = width === 100 ? 0 : (100 - width) / 2;
        const y = height === 100 ? 0 : (100 - height) / 2;

        this.setState({
            percentCrop: {
                unit: '%',
                aspect: 1,
                width,
                height,
                x,
                y,
            },
        });

        return false; // Return false, because we set crop state here
    }

    onCropChange = (crop, percentCrop) => {
        this.setState({ crop, percentCrop });
    };

    renderCropAddon = () => {
        return !this.state.draggingCrop && (
            <Container className="w-100 h-100 d-flex justify-content-center align-items-center">
                <Button variant="success" onClick={this.finishCrop}>Done</Button>
            </Container>
        );
    };

    onCropDragStart = () => {
        this.setState({ draggingCrop: true });
    };

    onCropDragEnd = () => {
        this.setState({ draggingCrop: false });
    };

    finishCrop = () => {
        this.makeClientCrop(this.imageRef, this.state.hashid, this.state.crop);
    };

    async makeClientCrop(image, hashid, crop) {
        if (image && hashid && crop.width && crop.height) {
            const canvas = document.createElement('canvas');
            const scaleX = image.naturalWidth / image.width;
            const scaleY = image.naturalHeight / image.height;
            canvas.width = crop.width;
            canvas.height = crop.height;
            const ctx = canvas.getContext('2d');

            ctx.drawImage(
                image,
                crop.x * scaleX,
                crop.y * scaleY,
                crop.width * scaleX,
                crop.height * scaleY,
                0,
                0,
                crop.width,
                crop.height
            );
            const croppedPicture = await new Promise((resolve, reject) => {
                canvas.toBlob(blob => {
                    if (!blob) {
                        console.error('Canvas is empty');
                        return;
                    }
                    blob.name = hashid + '.jpg';
                    // blob to base64
                    const reader = new FileReader();
                    reader.onloadend = function() {
                        resolve(reader.result);
                    };
                    reader.readAsDataURL(blob);
                }, 'image/jpeg');
            });
            this.setState({ croppedPicture, croppingPicture: false });
        }
    }

    handleEditBegin = () => {
        this.setState({ editing: true, updated: false });
    }

    handleEditCancel = () => {
        this.setState({ editing: false });
    }

    handleDeleteRequest = () => {
        this.setState({ deleteRequested: true });
    };

    handleDeleteCancel = () => {
        this.setState({ deleteRequested: false });
    };

    handleDelete = () => {
        const { hashid, deleteConfirm } = this.state;

        if (deleteConfirm !== hashid) {
            return;
        }

        this.setState({ loading: true });

        this.props.dispatch(UserService.remove(hashid))
            .then((res) => {
                console.log(res);
                const response = {
                    error: false,
                    message: res.message,
                    // TODO: clear user data from state
                };
                this.setState({ loading: false, success: true, editing: false, response });
                // TODO: logout if deleted self profile
                // TODO: redirect to home page + show result message there
            })
            .catch((err) => {
                console.error(err);
                const errors = Object.values(err.errors);
                errors.join(' ');
                const response = {
                    error: true,
                    message: errors,
                };
                this.setState({ response });
                this.setState({ loading: false });
            });
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.setState({ loading: true });

        const { hashid, gender, fos, tshirt, foodPref, name, full_name, dateOfBirth, email, mobile, passportNumber, allergies, pictureMethod, croppedPicture } = this.state;
        const userData = {
            gender,
            fos,
            tshirt,
            food_pref: foodPref,
            name,
            full_name,
            date_of_birth: dateOfBirth.format('YYYY-MM-DD'),
            email,
            mobile,
            passport_no: passportNumber,
            allergies,
            picture_method: pictureMethod,
            /*picture: picture,
            crop: percentCrop,*/
            picture: croppedPicture,
        };

        this.props.dispatch(UserService.update(hashid, userData))
            .then((res) => {
                const response = {
                    error: false,
                    message: res.message,
                };
                this.setState({ loading: false, success: true, editing: false, updated: true, response });
            })
            .catch((err) => {
                const response = {
                    error: true,
                    message: err.message,
                };
                this.setState({ loading: false, response });
            });
    }

    // TODO: implement with materialui
    renderView() {
        /*const { loading, optionValues, thumbnailUrl, name, full_name, dateOfBirth, fos, email, isEmailVerified, gender, passportNumber, mobile, allergies, tshirt, foodPref, joinedOn } = this.state;
        const thumbnailStyle = {
            width: '250px',
            height: '250px',
            border: '3px solid #fff',
            borderRadius: '100%',
            backgroundImage: thumbnailUrl ? `url(${thumbnailUrl}), url(${this.THUMBNAIL_FALLBACK_URL})` : `url(${this.THUMBNAIL_FALLBACK_URL})`,
            backgroundSize: '100%',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 50%',
        };

        return (
            <div>
                <Container className="text-center">
                    <div className="mx-auto mb-3" style={thumbnailStyle} />
                    <h2>{full_name} ({name})</h2>
                </Container>

                {this.state.updated &&
                    <Alert variant="success" className="text-center">Profile successfully updated.</Alert>
                }

                <Table className="mt-5">
                    <tbody>
                        <tr>
                            <th>Field of study</th>
                            <td>{(fos ? optionValues.fosList[fos] : '')}</td>
                        </tr>
                        <tr>
                            <th>E-mail address</th>
                            <td>
                                <a href={'mailto:' + email}>{email}</a>
                                {!loading && !isEmailVerified &&
                                    <Badge color="error" className="ml-2">Not verified</Badge>
                                }
                            </td>
                        </tr>
                        <tr>
                            <th>Date of birth</th>
                            <td>{dateOfBirth ? (dateOfBirth.format('YYYY. MM. DD.') + ' (' + moment.duration(moment().diff(dateOfBirth)).get('years') + ' years old)') : ''}</td>
                        </tr>
                        <tr>
                            <th>Gender</th>
                            <td>{this.capitalize(gender)}</td>
                        </tr>
                        <tr>
                            <th>Passport number</th>
                            <td>{passportNumber}</td>
                        </tr>
                        <tr>
                            <th>Mobile</th>
                            <td>
                                {mobile &&
                                    <a href={'tel:+' + mobile}>{mobile}</a>
                                }
                            </td>
                        </tr>
                        <tr>
                            <th>Allergies</th>
                            <td>{allergies}</td>
                        </tr>
                        <tr>
                            <th>T-shirt size</th>
                            <td>{this.uppercase(tshirt)}</td>
                        </tr>
                        <tr>
                            <th>Food preference</th>
                            <td>{(foodPref ? optionValues.foodPrefList[foodPref] : '')}</td>
                        </tr>
                        <tr>
                            <th>Registered</th>
                            <td>{joinedOn ? joinedOn.format('YYYY. MM. DD. h:mm:ss') : ''}</td>
                        </tr>
                    </tbody>
                </Table>

                <ButtonToolbar>
                    <Button
                        variant="primary"
                        className="mx-1"
                        onClick={this.handleEditBegin}
                    >Edit</Button>
                    <Button
                        variant="outline-primary"
                        className="mx-1"
                        href="/change-password"
                    >Change password</Button>
                    <Button
                        className="mx-1 ml-auto"
                        onClick={this.handleDeleteRequest}
                    >Delete</Button>
                </ButtonToolbar>
            </div>
        );*/
    }

    // TODO: implement with materialui
    renderForm() {
        /*const { response, errors, loading, editing, optionValues, gender, fos, tshirt, foodPref, hashid, name, full_name, dateOfBirth, email, mobile, passportNumber, allergies, thumbnailUrl, pictureMethod, picture, percentCrop, croppingPicture, croppedPicture } = this.state;

        let genderOptions = [
            <option key="genderNull" value="" disabled={true}>-- Select one --</option>
        ];
        if (optionValues.genders) {
            for (let i = 0; i < optionValues.genders.length; i++) {
                let g = optionValues.genders[i];
                genderOptions.push(<option key={g} value={g}>{this.capitalize(g)}</option>);
            }
        }
        let fosOptions = [
            <option key="fosNull" value="" disabled={true}>-- Select one --</option>
        ];
        if (optionValues.fosList) {
            for (let f in optionValues.fosList) {
                fosOptions.push(<option key={f} value={f}>{optionValues.fosList[f]}</option>);
            }
        }
        let tshirtOptions = [
            <option key="tshirtNull" value="" disabled={true}>-- Select one --</option>
        ];
        if (optionValues.tshirtSizes) {
            for (let i = 0; i < optionValues.tshirtSizes.length; i++) {
                let size = optionValues.tshirtSizes[i];
                tshirtOptions.push(<option key={size} value={size}>{this.uppercase(size)}</option>);
            }
        }
        let foodPrefOptions = [
            <option key="foodPrefNull" value="" disabled={true}>-- Select one --</option>
        ];
        if (optionValues.foodPrefList) {
            for (let pref in optionValues.foodPrefList) {
                foodPrefOptions.push(<option key={pref} value={pref}>{optionValues.foodPrefList[pref]}</option>);
            }
        }

        let pictureView;
        switch (pictureMethod) {
            case 'keep':
                pictureView = (thumbnailUrl ?
                    <Image thumbnail className="mb-3" src={thumbnailUrl} /> :
                    <p>You have no profile picture set yet.</p>
                );
                break;
            case 'update':
                pictureView = (<Container>
                    <Form.Control
                        id="picture"
                        type="file"
                        accept="image/x-png,image/gif,image/jpeg"
                        className={classNames('form-control-file', { 'is-invalid': ('picture' in errors) })}
                        name="picture"
                        aria-describedby="pictureHelp"
                        onChange={this.handlePictureChange}
                        disabled={loading}
                    />
                    <small id="pictureHelp" className="form-text text-muted">Please upload a valid image file: size should not be more than 2 MB and 250x250 px in dimensions at least (preferably rectangular).</small>

                    {picture &&
                        (croppingPicture ?
                            (<ReactCrop
                                src={picture}
                                crop={percentCrop}
                                minWidth={250}
                                minHeight={250}
                                keepSelection
                                circularCrop
                                className="mt-3"
                                onImageLoaded={this.onPictureLoaded}
                                onChange={this.onCropChange}
                                onDragStart={this.onCropDragStart}
                                onDragEnd={this.onCropDragEnd}
                                renderSelectionAddon={this.renderCropAddon}
                            />) :
                            (<Image thumbnail className="mt-3 w-100" src={croppedPicture} />)
                        )
                    }

                    {('picture' in errors) &&
                        <Form.Control.Feedback type="invalid">{errors.picture}</Form.Control.Feedback>
                    }
                </Container>);
                break;
            case 'remove':
                pictureView = <p>The current profile picture will be removed on save.</p>;
                break;
        }

        return (
            <Container>

                {this.state.response.error &&
                    <Alert severity="error" className="text-center">{this.state.response.message || 'Failed to update profile.'}</Alert>
                }

                <form onSubmit={this.handleSubmit} ref={(el) => { this.editForm = el; }}>

                    <Form.Row>
                        <Form.Group as={Col} controlId="full_name" lg="6">
                            <Form.Label>Full name</Form.Label>
                            <Form.Control
                                type="name"
                                name="full_name"
                                className={classNames({ 'is-invalid': ('full_name' in errors) })}
                                placeholder="Enter full name"
                                required
                                value={full_name}
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                                disabled={loading}
                            />
                            {('full_name' in errors) &&
                                <Form.Control.Feedback type="invalid">{errors.full_name}</Form.Control.Feedback>
                            }
                        </Form.Group>

                        <Form.Group as={Col} controlId="name" lg="6">
                            <Form.Label>Short name</Form.Label>
                            <Form.Control
                                type="name"
                                name="name"
                                className={classNames({ 'is-invalid': ('name' in errors) })}
                                placeholder="How should we call you (eg. first name or nick)?"
                                required
                                value={name}
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                                disabled={loading}
                            />
                            {('name' in errors) &&
                                <Form.Control.Feedback type="invalid">{errors.name}</Form.Control.Feedback>
                            }
                        </Form.Group>
                    </Form.Row>

                    <Form.Group controlId="date-of-birth">
                        <Form.Label>Date of birth</Form.Label>
                        <Form.Control
                            type="date"
                            name="dateOfBirth"
                            className={classNames({ 'is-invalid': ('dateOfBirth' in errors) })}
                            required
                            value={dateOfBirth ? dateOfBirth.format('YYYY-MM-DD') : ''}
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            disabled={loading}
                        />

                        {('dateOfBirth' in errors) &&
                            <Form.Control.Feedback type="invalid">{errors.dateOfBirth}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group controlId="gender">
                        <Form.Label>Gender</Form.Label>
                        <Form.Control
                            name="gender"
                            as="select"
                            className={classNames({ 'is-invalid': ('gender' in errors) })}
                            required
                            value={gender || ''}
                            onChange={this.handleChange}
                            onBlur={this.onBlur}
                            disabled={loading}>
                            {genderOptions}
                        </Form.Control>

                        {('gender' in errors) &&
                            <Form.Control.Feedback type="invalid">{errors.gender}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group controlId="email">
                        <Form.Label>E-mail address</Form.Label>
                        <Form.Control
                            type="email"
                            name="email"
                            className={classNames({ 'is-invalid': ('email' in errors) })}
                            placeholder="Enter e-mail"
                            aria-describedby="emailHelp"
                            required
                            value={email}
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            disabled={loading}
                        />
                        <small id="emailHelp" className="form-text text-muted">Note: changing the email address requires its verification.</small>

                        {('email' in errors) &&
                            <Form.Control.Feedback type="invalid">{errors.email}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group controlId="mobile">
                        <Form.Label>Mobile number</Form.Label>
                        <Form.Control
                            type="phone"
                            name="mobile"
                            className={classNames({ 'is-invalid': ('mobile' in errors) })}
                            placeholder="Enter mobile number"
                            value={mobile ? ('+' + mobile) : ''}
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            disabled={loading}
                        />

                        {('mobile' in errors) &&
                            <Form.Control.Feedback type="invalid">{errors.mobile}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group controlId="passport">
                        <Form.Label>Passport number</Form.Label>

                        <Form.Control
                            type="text"
                            name="passportNumber"
                            className={classNames({ 'is-invalid': ('passportNumber' in errors) })}
                            placeholder="Enter passport number"
                            value={passportNumber || ''}
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            disabled={loading}
                        />

                        {('passportNumber' in errors) &&
                            <Form.Control.Feedback type="invalid">{errors.passportNumber}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group controlId="allergies">
                        <Form.Label>Allergies</Form.Label>
                        <Form.Control
                            type="text"
                            name="allergies"
                            placeholder="Enter allergies"
                            value={allergies || ''}
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            disabled={loading}
                        />
                    </Form.Group>

                    <Form.Group controlId="fos">
                        <Form.Label>Field of study</Form.Label>
                        <Form.Control
                            name="fos"
                            as="select"
                            className={classNames({ 'is-invalid': ('fos' in errors) })}
                            required
                            value={fos || ''}
                            onChange={this.handleChange}
                            onBlur={this.onBlur}
                            disabled={loading}>
                            {fosOptions}
                        </Form.Control>

                        {('fos' in errors) &&
                            <Form.Control.Feedback type="invalid">{errors.fos}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group controlId="tshirt">
                        <Form.Label>T-shirt size</Form.Label>
                        <Form.Control
                            name="tshirt"
                            as="select"
                            className={classNames({ 'is-invalid': ('tshirt' in errors) })}
                            required
                            value={tshirt || ''}
                            onChange={this.handleChange}
                            onBlur={this.onBlur}
                            disabled={loading}>
                            {tshirtOptions}
                        </Form.Control>

                        {('tshirt' in errors) &&
                            <Form.Control.Feedback type="invalid">{errors.tshirt}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group controlId="food-pref">
                        <Form.Label>Food preference</Form.Label>
                        <Form.Control
                            name="foodPref"
                            as="select"
                            className={classNames({ 'is-invalid': ('foodPref' in errors) })}
                            required
                            value={foodPref || ''}
                            onChange={this.handleChange}
                            onBlur={this.onBlur}
                            disabled={loading}>
                            {foodPrefOptions}
                        </Form.Control>

                        {('foodPref' in errors) &&
                            <Form.Control.Feedback type="invalid">{errors.foodPref}</Form.Control.Feedback>
                        }
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Profile picture</Form.Label>
                        <Nav
                            className="mb-3"
                            variant="pills"
                            activeKey={pictureMethod}
                            onSelect={this.handleNavSelect}>
                            <Nav.Item>
                                <Nav.Link eventKey="keep" disabled={loading}>Keep current</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="update" disabled={loading}>Update</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="remove" disabled={thumbnailUrl === null || loading}>Remove</Nav.Link>
                            </Nav.Item>
                        </Nav>
                        {pictureView}
                    </Form.Group>

                    <ButtonToolbar>
                        <Button
                            variant="primary"
                            type="submit"
                            className="mx-1"
                            disabled={loading}
                        >{loading ? 'Please wait...' : 'Save'}</Button>
                        <Button
                            variant="outline-secondary"
                            className="mx-1"
                            onClick={this.handleEditCancel}
                            disabled={loading}
                        >Cancel</Button>
                    </ButtonToolbar>
                </form>
            </Container>
        );*/
    }

    render() {
        // If user was not authenticated we redirect to login screen
        if (!this.props.isAuthenticated) {
            return (
                <Redirect to="/login" />
            );
        }

        return (
            <React.Fragment>
                <Container>
                    <div className="profile">
                        Profile
                    </div>
                </Container>
            </React.Fragment>
        );

        // TODO: implement with materialui

        /*const { editing, deleteRequested, hashid, deleteConfirm } = this.state;

        return (
            <div className="d-flex flex-column flex-row align-content-center py-5">
                <Container>
                    <Modal
                        show={deleteRequested}
                        onHide={this.handleDeleteCancel}
                        aria-labelledby="delete-modal-title"
                        centered>
                        <Modal.Header>
                            <Modal.Title id="delete-modal-title">Confirm user deletion</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Please enter the following text to the field below to delete this user:<br />
                            <big><code>{hashid}</code></big>
                            <FormControl
                                type="text"
                                name="deleteConfirm"
                                placeholder="Enter confirmation"
                                className="mt-3"
                                required
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                            />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="primary" type="danger" onClick={this.handleDelete} disabled={deleteConfirm !== hashid}>Delete</Button>
                            <Button variant="secondary" onClick={this.handleDeleteCancel}>Cancel</Button>
                        </Modal.Footer>
                    </Modal>
                    <Row>
                        <Col className="mx-auto" lg="6">

                            {editing && <h4>Edit profile</h4>}

                            <Card className="mb-3">
                                <Card.Body>

                                    {editing ? this.renderForm() : this.renderView()}

                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );*/
    }
}

Profile.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
    user: state.Auth.user,
});

export default compose(
    connect(mapStateToProps),
    withLayout,
)(Profile);
