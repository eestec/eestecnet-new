import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReeValidate from 'ree-validate';
import * as AuthService from '../../services/authService';
import {Button, TextField, Typography, Container} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import {compose} from "recompose";
import {withLayout} from "../../components/layouts";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
    main: {
      backgroundColor: theme.palette.secondary.main,
      height: 500
    },
    paper: {
      marginTop: theme.spacing(8),
      paddingTop: 60,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  });

// TODO: allow password change without id+token from email (if logged in)
class ResetPassword extends Component {

    constructor(props) {
        super(props);
        this.passRef = React.createRef();

        ReeValidate.extend('confirmation', (value, [otherValue]) => {
            // var other = ReactDOM.findDOMNode(this.refs[otherValue]);
            let other = this.passRef.current;
            return other != null && value === other.children[0].children[0].value; // We should find a better way
        }, { hasTarget: true });
        ReeValidate.localize('en', {
            messages: {
                'confirmation': function (field, ref) {
                    let other = ref[0];
                    return ('The content of ' + other + ' and its confirmation field should match.');
                },
            },
        });


        this.validator = new ReeValidate({
            password: 'required|min:8',
            password_confirmation: 'required|min:8|confirmation:password',
            id: 'required',
            token: 'required',
        });

        this.state = {
            loading: false,
            id: this.getResetId(),
            token: this.getResetToken(),
            password: '',
            password_confirmation: '',
            errors: {},
            response: {
                error: false,
                message: '',
            },
        };
    }

    getResetId() {
        const params = new URLSearchParams(this.props.location.search);
        if (params.has('id')) {
            return params.get('id');
        }
        return '';
    }

    getResetToken() {
        const params = new URLSearchParams(this.props.location.search);
        if (params.has('token')) {
            return params.get('token');
        }
        return '';
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });

        // If a field has a validation error, we'll clear it when corrected
        const { errors } = this.state;
        if (name in errors) {
            const validation = this.validator.errors;
            this.validator.validate(name, value).then(() => {
                if (!validation.has(name)) {
                    delete errors[name];
                    this.setState({ errors });
                }
            });
        }
    }

    handleBlur = (e) => {
        const { name, value } = e.target;
        const validation = this.validator.errors;

        // Avoid validation until input has a value
        if (value === '') {
            return;
        }

        this.validator.validate(name, value).then(() => {
            if (validation.has(name)) {
                const { errors } = this.state;
                errors[name] = validation.first(name);
                this.setState({ errors });
            }
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const credentials = {
            id: this.state.id,
            token: this.state.token,
            password: this.state.password,
            password_confirmation: this.state.password_confirmation,
        };

        this.setState({ loading: true });

        this.props.dispatch(AuthService.updatePassword(credentials))
            .then((res) => {
                this.passwordForm.reset();
                const response = {
                    error: false,
                    message: res.message,
                };
                this.setState({ loading: false, success: true, response });
            })
            .catch((err) => {
                this.passwordForm.reset();
                const errors = Object.values(err.errors);
                errors.join(' ');
                const response = {
                    error: true,
                    message: errors,
                };
                this.setState({ response });
                this.setState({ loading: false });
            });
    }

    render() {
        // If user is not authenticated we redirect to login
        /*if (!this.props.isAuthenticated) {
            return (
                <Redirect to='/login' />
            );
        }*/

        const { response, errors, loading } = this.state;
        const {classes} = this.props;

        return (
            <React.Fragment>
                <main className={classes.main}>
                <Container maxWidth="xs">
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                    Reset password
                    </Typography>
                    {this.state.success &&
                        <Alert severity="success" className="text-center">Your password has been reset!</Alert>
                    }
                    {response.error &&
                        <Alert severity="error" className="text-center">{response.message}</Alert>
                    }

                    {!this.state.success &&
                        <form onSubmit={this.handleSubmit} ref={(el) => { this.passwordForm = el; }}>

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        type="password"
                        ref={this.passRef} // needed for confirm validation
                        label="New password"
                        placeholder="Enter new password"
                        error={'password' in errors}
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('password' in errors) &&
                        <Alert severity="warning">{errors.password}</Alert>
                    }

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password_confirmation"
                        type="password"
                        label="Password confirmation"
                        placeholder="Re-enter new password"
                        error={'password_confirmation' in errors}
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('password_confirmation' in errors) &&
                        <Alert severity="warning">{errors.password_confirmation}</Alert>
                    }

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={loading}
                    >{loading ? 'Please wait...' : 'Reset password'}
                    </Button>
                    </form>
                    }
                </div>

                </Container>
                </main>
            </React.Fragment>
        );
    }
}

ResetPassword.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
});

export default compose(
    connect(mapStateToProps),
    withLayout,
    withStyles(styles)
)(ResetPassword);
