import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
//import Select from 'react-select';
import { Redirect } from 'react-router-dom';
import ReeValidate from 'ree-validate';
import classNames from 'classnames';
import * as AuthService from '../../services/authService';
import moment from 'moment';
import {Button, TextField, FormControlLabel, Link, Checkbox, Grid, Typography, Container} from '@material-ui/core';
import {Autocomplete} from '@material-ui/lab';
import Alert from '@material-ui/lab/Alert';
import {MuiPickersUtilsProvider, KeyboardDatePicker} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import {compose} from 'recompose';
import {withLayout} from "../../components/layouts";
import withStyles from '@material-ui/core/styles/withStyles';
import Http from '../../Http';
import {API_BRANCHES_RESOURCE} from '../../BackendRoutes';


const styles = theme => ({
    main: {
      backgroundColor: theme.palette.secondary.main,
    },
    paper: {
      marginTop: theme.spacing(8),
      paddingTop: 60,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  });
  

class Register extends Component {

    constructor(props) {
        super(props);

        this.passRef = React.createRef();

        ReeValidate.extend('confirmation', (value, [otherValue]) => {
            // var other = ReactDOM.findDOMNode(this.refs[otherValue]);
            let other = this.passRef.current;
            return other != null && value === other.children[0].children[0].value; // We should find a better way
        }, { hasTarget: true });
        ReeValidate.localize('en', {
            messages: {
                'confirmation': function (field, ref) {
                    let other = ref[0];
                    return ('The content of ' + other + ' and its confirmation field should match.');
                },
            },
        });

        const maxDateOfBirth = moment().subtract(16, 'years');

        this.validator = new ReeValidate({
            name: 'required|min:3',
            full_name: 'required|min:3',
            date_of_birth: 'required|date_format:YYYY-MM-DD|before:' + maxDateOfBirth.format('YYYY-MM-DD'),
            email: 'required|email',
            password: 'required|min:8',
            password_confirmation: 'required|min:8|confirmation:password',
            terms: 'required|regex:^terms$',
            branch: 'required'
        });

        this.state = {
            loading: true,
            name: '',
            dateOfBirth: null,
            email: '',
            password: '',
            password_confirmation: '',
            errors: {},
            response: {
                error: false,
                message: '',
            },
            success: false,
            maxDateOfBirth,
            branches: {}
        };
    }


    componentDidMount() {
    Http.get(API_BRANCHES_RESOURCE() + "?all=true")
            .then((response) => {
                const resp = response.data;
                console.log(response);
                this.setState({
                    branches: resp,
                    loading: false,
                    error: false,
                });
            })
            .catch(() => {
                this.setState({
                    error: 'Unable to fetch branches.',
                });
            });
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });

        // If a field has a validation error, we'll clear it when corrected
        const { errors } = this.state;
        if (name in errors) {
            const validation = this.validator.errors;
            this.validator.validate(name, value).then(() => {
                if (!validation.has(name)) {
                    delete errors[name];
                    this.setState({ errors });
                }
            });
        }
    }

    handleBlur = (e) => {
        const { name, value } = e.target;
        const validation = this.validator.errors;

        // Avoid validation until input has a value
        if (value === '') {
            return;
        }

        this.validator.validate(name, value).then(() => {
            if (validation.has(name)) {
                const { errors } = this.state;
                errors[name] = validation.first(name);
                this.setState({ errors });
            }
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { name, full_name, dateOfBirth, email, branch, password, password_confirmation, terms } = this.state;
        const data = { name, full_name, date_of_birth: dateOfBirth.format('yyyy-MM-DD'), email, branch, password, password_confirmation, terms };

        // Set response state back to default
        this.setState({ response: { error: false, message: '' } });

        this.validator.validateAll(data)
            .then((success) => {
                if (success) {
                    this.setState({ loading: true });
                    this.submit(data);
                }
            });
    }

    submit(data) {
        this.props.dispatch(AuthService.register(data))
            .then(() => {
                this.registrationForm.reset();
                this.setState({ loading: false, success: true });
            })
            .catch((err) => {
                const errors = Object.values(err.errors);
                errors.join(' ');
                const response = {
                    error: true,
                    message: errors,
                };
                this.setState({ response });
                this.setState({ loading: false });
            });
    }

    render() {
        const branches = Array.from(this.state.branches);
        // If user is already authenticated we redirect to home page
        if (this.props.isAuthenticated) {
            return <Redirect to="/" replace />;
        }

        const { response, errors, loading, dateOfBirth } = this.state;
        const {classes} = this.props;

        // TODO: not accepting terms doesn't show validation error

        return (
            <React.Fragment>
                <main className={classes.main}>
                <Container maxWidth="xs">
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                    Register
                    </Typography>

                    {response.error &&
                        <Alert severity="error" className="text-center">{response.message}</Alert>
                    }

                    {this.state.success &&
                        <Alert severity="success" className="text-center">
                            Registration successful.<br />
                            <Link href="/login">Please log in with your email and password.</Link>
                        </Alert>
                    }

                    <form className={classes.form} onSubmit={this.handleSubmit} ref={(el) => { this.registrationForm = el; }}>

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        autoFocus
                        type="name"
                        name="name"
                        error={'name' in errors}
                        label="Name"
                        helperText="How can we call you (eg. first name or nick)?"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('name' in errors) &&
                        <Alert severity="warning">{errors.name}</Alert>
                    }

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        type="name"
                        name="full_name"
                        error={'full_name' in errors}
                        label="Full name"
                        helperText="Enter your full name, preferably in [fist name] [last name] order."
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('full_name' in errors) &&
                        <Alert severity="warning">{errors.full_name}</Alert>
                    }

                    <MuiPickersUtilsProvider utils={MomentUtils} locale={window.navigator.userLanguage || window.navigator.language}>
                        <KeyboardDatePicker
                            autoOk
                            variant="dialog"
                            inputVariant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="date_of_birth"
                            errorText={'date_of_birth' in errors}
                            label="Date of birth"
                            format="yyyy. MM. DD."
                            value={dateOfBirth}
                            maxDate={this.state.maxDateOfBirth}
                            minDate={new Date(1970, 1, 1)}
                            initialFocusedDate={new Date(2000, 1, 1)}
                            invalidDateMessage="Invalid/incomplete date format"
                            openTo="year"
                            InputAdornmentProps={{
                                position: 'end',
                            }}
                            onChange={(date) => this.setState({ dateOfBirth: date })}
                        />
                    </MuiPickersUtilsProvider>
                    {('date_of_birth' in errors) &&
                        <Alert severity="warning">{errors.date_of_birth}</Alert>
                    }

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        type="email"
                        name="email"
                        error={'email' in errors}
                        placeholder="Enter your e-mail"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('email' in errors) &&
                        <Alert severity="warning">{errors.email}</Alert>
                    }

<Autocomplete
  id="combo-box-demo"
  options={branches}
 getOptionLabel={(option) => option.name}
  renderInput={(params) => <TextField {...params} label="Your branch" variant="outlined" margin="normal" required fullWidth/>}
/>
               
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        ref={this.passRef} // needed for confirm validation
                        type="password"
                        error={'password' in errors}
                        name="password"
                        placeholder="Enter password"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('password' in errors) &&
                        <Alert severity="warning">{errors.password}</Alert>
                    }

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        type="password"
                        error={'password_confirmation' in errors}
                        name="password_confirmation"
                        placeholder="Re-enter password"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('password_confirmation' in errors) &&
                        <Alert severity="warning">{errors.password_confirmation}</Alert>
                    }

                    <FormControlLabel
                        control={<Checkbox value="terms" color={'terms' in errors ? 'error' : 'primary'} />}
                        required
                        type="checkbox"
                        id="terms"
                        name="terms"
                        onChange={this.handleChange}
                        disabled={loading}
                        label={<label htmlFor="terms">I accept the <Link href="/terms">Terms &amp; Conditions</Link></label>}
                    />
                    {('terms' in errors) &&
                        <Alert severity="warning">{errors.terms}</Alert>
                    }

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={loading}
                    >
                        {loading ? 'Please wait...' : 'Register'}
                    </Button>
                    <Grid>
                    {!this.state.success &&
                        <div className="text-center">
                            <p>Already registered? <Link href="/login">Log in</Link></p>
                        </div>
                    }
                    </Grid>
                    </form>
                </div>

                </Container>
                </main>
            </React.Fragment>
        );
    }
}

Register.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
});

export default compose(
    connect(mapStateToProps),
    withLayout,
    withStyles(styles)
)(Register);
