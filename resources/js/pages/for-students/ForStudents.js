import React, {Component} from 'react';
import Container from "@material-ui/core/Container";
import {compose} from "recompose";
import {withLayout} from "../../components/layouts";

class ForStudents extends Component {

    render() {
        return (
            <React.Fragment>
                <Container>
                    <div className="for-students">
                        For Students
                    </div>
                </Container>
            </React.Fragment>
        );
    }
}

export default compose(
    withLayout
)(ForStudents);
