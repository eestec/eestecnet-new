import React, { Component } from 'react';
import { connect } from 'react-redux';

import Http from '../../Http';
import { API_TEAMS_RESOURCE } from '../../BackendRoutes';
import { withLayout } from "../../components/layouts";

import { Typography, Container, Table, TableHead, TableBody, TableRow, TableCell, Button, Link } from "@material-ui/core";
import { compose } from "recompose";
import withStyles from "@material-ui/core/styles/withStyles";
import Alert from "@material-ui/lab/Alert";

import Jumbotron from "./Jumbotron";

import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActionArea from '@material-ui/core/CardActionArea';
import Avatar from '@material-ui/core/Avatar';

const styles = theme => ({
    main: {
        backgroundColor: theme.palette.secondary.main,
        minHeight: 500
    },
    paper: {
        marginTop: theme.spacing(8),
        paddingTop: 60,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 500,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    media: {
        height: 140,
    },
});

class Teams extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            data: {},
            pageFrom: 1,
            pageTo: 15,
            itemCount: 0,
            lastPage: 1,
            perPage: 0,
            moreLoaded: false,
            error: false,
        };
    }



    componentDidMount() {
        Http.get(API_TEAMS_RESOURCE())
            .then((response) => {
                const resp = response.data;
                this.setState({
                    data: resp.data,
                    pageFrom: resp.from,
                    pageTo: resp.to,
                    itemCount: resp.total,
                    lastPage: resp.last_page,
                    perPage: resp.per_page,
                    nextPageUrl: resp.next_page_url,
                    loading: false,
                    error: false,
                });
            })
            .catch(() => {
                this.setState({
                    error: 'Unable to fetch teams.',
                });
            });
    }

    loadMore = () => {
        this.setState({ loading: true });
        Http.get(this.state.nextPageUrl)
            .then((response) => {
                const resp = response.data;
                const mergedData = this.state.data.concat(resp.data);
                this.setState({
                    data: mergedData,
                    pageFrom: resp.from,
                    pageTo: resp.to,
                    itemCount: resp.total,
                    lastPage: resp.last_page,
                    perPage: resp.per_page,
                    nextPageUrl: resp.next_page_url,
                    loading: false,
                    moreLoaded: true,
                    error: false,
                });
            })
            .catch(() => {
                this.setState({
                    error: 'Unable to fetch teams.',
                });
            });
    }

    deleteTeam = (e) => {
        const { key } = e.currentTarget.dataset;
        const { data: teams } = this.state;

        //console.log({key}.key);
        Http.delete(`${API_TEAMS_RESOURCE()}/${key}`)
            .then((response) => {
                if (response.status === 202) {
                    const index = teams.findIndex(team => team.slug === key);
                    const update = [...teams.slice(0, index), ...teams.slice(index + 1)];
                    // TODO: update item count and indices
                    this.setState({ data: update });
                } else {
                    console.error(response);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    render() {
        const { loading, error, pageTo, itemCount } = this.state;
        const teams = Array.from(this.state.data);
        const { classes } = this.props;
        console.log(teams);

        return (
            <React.Fragment>
                <main className="teams-page">
                    <Jumbotron />
                </main>
                {/* </React.Fragment>

<React.Fragment> */}
                <main className={classes.main}>
                    <Container>
                        <div className={classes.paper}>

                            {error &&
                                <Alert severity="error">{error}</Alert>
                            }

                            <div className={classes.root}>
                                <Grid container spacing={6}>
                                    {teams.map((team) => (
                                        <Grid zeroMinWidth item xs={6} key={team.id} spacing={3}>
                                            <Link href={'/teams/' + team.slug}>
                                                <Grid container spacing={5}>
                                                    <Grid xs={3}>
                                                        {/* <Avatar alt = {team.name} src={team.thumbnail}>
                                                        </Avatar> */}
                                                        <CardMedia
                                                            className={classes.media}
                                                            image={team.thumbnail}
                                                            title={team.name}
                                                        />
                                                    </Grid>
                                                    <Grid xs={9}>
                                                        <CardContent>
                                                            <Typography gutterBottom variant="h5" component="h2">
                                                                {team.name}
                                                            </Typography>
                                                            <Typography noWrap variant="body2" color="textSecondary" component="p">
                                                                {team.description}
                                                            </Typography>
                                                        </CardContent>
                                                    </Grid>
                                                </Grid>



                                                {/* <Card  className={classes.root}>
                                                    <CardActionArea>
                                                        <CardMedia
                                                            className={classes.media}
                                                            image={team.thumbnail}
                                                            title={team.name}
                                                        />
                                                        <CardContent>
                                                            <Typography gutterBottom variant="h5" component="h2">
                                                                {team.name}
                                                            </Typography>
                                                            <Typography noWrap variant="body2" color="textSecondary" component="p">
                                                                {team.description}
                                                            </Typography>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Card> */}
                                            </Link>
                                        </Grid>
                                    ))}
                                </Grid>
                            </div>

                            {pageTo < itemCount &&
                                <div className="text-center">
                                    <Button
                                        variant="text"
                                        onClick={loading ? null : this.loadMore}
                                        disabled={loading}
                                    >{loading ? 'Please wait...' : 'Load more'}</Button>
                                </div>
                            }

                            {(pageTo === itemCount) && (this.state.moreLoaded === true) &&
                                <div className="text-center">
                                    <p>Everything loaded.</p>
                                </div>
                            }
                        </div>
                    </Container>
                </main>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
    user: state.Auth.user,
});

export default compose(
    connect(mapStateToProps),
    withLayout,
    withStyles(styles)
)(Teams);

