import React, {Component} from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import {compose} from "recompose";
import withStyles from "@material-ui/core/styles/withStyles";
import JumbotronImage from "../../../images/bg_homepage.jpg";

const styles = theme => ({
    jumbotron: {
        height: '800px',
        background: `url(${JumbotronImage}) no-repeat center center`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
    },
    container: {
        height: '100%'
    },
    wrapper: {
        height: '100%',
        display: 'flex',
        flexFlow: 'column'
    },
    infobox: {
        textAlign: 'center',
        justifyContent: 'center',
        display: 'flex',
        flexFlow: 'column',
        flexGrow: 1,
    },
    bottomText: {
        display: 'flex',
        '&:after': {
            content: '""',
            display: 'block',
            height: '1px',
            backgroundColor: "#969696",
            flexGrow: 1,
            marginLeft: '10px',
            marginTop: '7px'
        }
    },
    buttonsWrapper: {
        '& > *': {
            margin: theme.spacing(1),
            width: '200px'
        },
    }
});

class Jumbotron extends Component {

    render() {
        const {classes} = this.props;

        return (
            <section className={classes.jumbotron}>
                <Container className={classes.container}>
                    <div className={classes.wrapper}>
                        <div className={classes.infobox}>
                            <Typography variant="h5" color="textSecondary">
                                <Box fontWeight="fontWeightBold">
                                    Don't wait and
                                </Box>
                            </Typography>
                            <Typography variant="h2" color="textSecondary" gutterBottom>
                                <Box fontWeight="fontWeightBold">
                                    Power your future
                                </Box>
                            </Typography>
                            <Typography variant="body1" color="textSecondary" paragraph>
                                Electrical Engineering STudents' European assoCiation<br/>
                                is an apolitical, non-governmental and non-profit organization<br/>
                                for EECS students at universities, institutes and schools of technology.
                            </Typography>
                            <div className={classes.buttonsWrapper}>
                                <Button size="large" variant="contained" color="primary" href="/contact">Contact</Button>
                                <Button size="large" variant="outlined" color="secondary" href="/login">Login</Button>
                            </div>
                        </div>

                        <div className={classes.bottomText}>
                            <Typography variant="caption" color="textSecondary">
                                Electrical Engineering STudents' European assoCiation
                            </Typography>
                        </div>
                    </div>
                </Container>
            </section>
        );
    }
}

export default compose(
    withStyles(styles)
)(Jumbotron);
