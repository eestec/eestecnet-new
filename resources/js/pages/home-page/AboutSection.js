import React, {Component} from "react";
import {compose} from "recompose";
import Container from "@material-ui/core/Container";
import withStyles from "@material-ui/core/styles/withStyles";
import AboutSectionImage from "../../../images/eestec_what_it_is.jpg";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
    aboutSection: {
        [theme.breakpoints.up('xs')]: {
            height: '1000px'
        },
        [theme.breakpoints.down('xs')]: {
            height: 'auto',
            paddingTop: theme.spacing(2) + 'px',
            paddingBottom: theme.spacing(2) + 'px'
        },
        background: `url(${AboutSectionImage}) no-repeat center center`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
    },
    container: {
        height: '100%'
    }
});

class AboutSection extends Component {
    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <section className={classes.aboutSection}>
                    <Container className={classes.container}>
                        <Grid container
                              className={classes.container}
                              justify="flex-end"
                        >
                            <Grid container item xs={12} md={6}
                                  direction="column"
                                  justify="center"
                            >
                                <Typography
                                    color='textSecondary'
                                    variant="h5"
                                >
                                    EESTEC
                                </Typography>
                                <Typography
                                    color='textSecondary'
                                    variant="h2"
                                    // gutterBottom={2}
                                    gutterBottom
                                >
                                    What is it?
                                </Typography>
                                <Typography
                                    color='textSecondary'
                                    variant="body1"
                                    paragraph
                                    component="p"
                                >
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pharetra velit nibh,
                                    in fermentum ex aliquet sed. Class aptent taciti sociosqu ad litora torquent per
                                    conubia nostra, per inceptos himenaeos. Pellentesque imperdiet ex at hendrerit
                                    eleifend. Nullam porta neque id enim pulvinar lacinia. Fusce vel erat et turpis
                                    feugiat tincidunt sit amet a arcu. Nam vehicula tortor sed tortor tincidunt, quis
                                    egestas leo venenatis. Maecenas tristique metus non ipsum scelerisque pellentesque.
                                    In egestas lorem vestibulum commodo aliquet. Morbi ultricies tincidunt nisl.
                                </Typography>
                                <Typography
                                    color='textSecondary'
                                    variant="body1"
                                    paragraph
                                    component="p"
                                >
                                    Proin lacus nulla, faucibus sed dignissim ultrices, mattis et lorem. Ut quis massa
                                    augue. Vestibulum turpis mi, dapibus elementum rhoncus at, fermentum vitae felis.
                                    Aliquam sem enim, accumsan id turpis bibendum, facilisis egestas turpis.
                                    Pellentesque tincidunt molestie nisi at varius. Praesent massa mauris, dignissim vel
                                    odio eu, dignissim convallis justo. Fusce lacus orci, faucibus et varius placerat,
                                    tincidunt ut ante. Nam eu ultrices lacus. Donec risus tortor, maximus vel placerat
                                    a, tincidunt eu nibh. Nulla sed tincidunt lorem. Sed pharetra, eros nec faucibus
                                    scelerisque, lorem mauris suscipit purus, ut semper mi urna sit amet leo. Ut
                                    pulvinar felis et blandit pretium.
                                </Typography>
                                <Typography
                                    color='textSecondary'
                                    variant="body1"
                                    paragraph
                                    component="p"
                                >
                                    Integer eget risus lobortis, venenatis ante egestas, pretium lorem. Duis venenatis
                                    rhoncus augue ut aliquam. Donec vitae diam at nibh faucibus bibendum sed non ipsum.
                                    Vivamus ligula eros, ornare a velit non, pharetra auctor purus. Maecenas malesuada
                                    varius libero, sit amet dictum dolor egestas eu. Aliquam lacus justo, laoreet in
                                    iaculis vel, consectetur sed libero. Ut ultricies, eros sit amet auctor suscipit,
                                    eros sapien ultrices tortor, quis ullamcorper dolor lectus at diam. Class aptent
                                    taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                                    Vestibulum eget purus vitae leo condimentum varius. Ut fermentum fringilla varius.
                                </Typography>
                            </Grid>
                        </Grid>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

export default compose(
    withStyles(styles)
)(AboutSection);
