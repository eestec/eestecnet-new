import React, {Component} from 'react';
import {withLayout} from "../../components/layouts";
import {compose} from "recompose";
import Jumbotron from "./Jumbotron";
import InterestingFacts from "./InterestingFacts";
import AboutSection from "./AboutSection";


class Home extends Component {

    render() {
        return (
            <React.Fragment>
                <main className="home-page">
                    <Jumbotron/>
                    <InterestingFacts/>
                    <AboutSection/>
                </main>
            </React.Fragment>
        );
    }
}

export default compose(
    withLayout
)(Home);
