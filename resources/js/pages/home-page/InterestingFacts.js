import React, {Component} from "react";
import {compose} from "recompose";
import withStyles from "@material-ui/core/styles/withStyles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import PanToolOutlinedIcon from '@material-ui/icons/PanToolOutlined';
import EventNoteOutlinedIcon from '@material-ui/icons/EventNoteOutlined';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import LinearScaleOutlinedIcon from '@material-ui/icons/LinearScaleOutlined';
import {withWidth} from "@material-ui/core";

const styles = theme => ({
    interestingFacts: {
        [theme.breakpoints.up('xs')]: {
            height: '1000px'
        },
        [theme.breakpoints.down('xs')]: {
            height: 'auto',
            paddingTop: theme.spacing(2) + 'px',
            paddingBottom: theme.spacing(2) + 'px'
        },
        backgroundColor: theme.palette.secondary.main
    },
    container: {
        height: '100%'
    },
    flex: {
        display: 'flex'
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center'
    },
    smallCardText: {
        textTransform: 'uppercase'
    },
    leftPanelSmallText: {
        textTransform: 'uppercase'
    },
    bigCardText: {
        fontWeight: 900
    }
});


class InterestingFacts extends Component {
    render() {
        const {classes, width} = this.props;

        return (
            <React.Fragment>
                <section className={classes.interestingFacts}>
                    <Container className={classes.container}>
                        <Grid container className={classes.container}>
                            <Grid container className={classes.flex} item xs={12} md={6}
                                  direction="column"
                                  justify="center"
                            >
                                <Box paddingLeft={width !== 'xs' ? 2 : 0} marginBottom={width === 'xs' ? 4 : 0}>
                                    <Typography
                                        color='primary'
                                        variant="h5"
                                        className={classes.leftPanelSmallText}
                                    >
                                        Interesting facts
                                    </Typography>
                                    <Typography
                                        color='textPrimary'
                                        variant="h3">
                                        Few interesting facts<br/>
                                        about EESTEC
                                    </Typography>
                                    <Typography
                                        color="textPrimary"
                                        variant="body1"
                                        paragraph>
                                        If you are interested for more <br/>
                                        please click the button below...
                                    </Typography>
                                    <Button size="large" fullWidth={width === 'xs'} variant="outlined" color="primary">Read more</Button>
                                </Box>
                            </Grid>
                            <Grid container item xs={12} md={6} direction="column" justify="center">
                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={6}>
                                        <Card>
                                            <CardContent className={classes.cardContent}>
                                                <Box fontSize="60px">
                                                    <PanToolOutlinedIcon fontSize="inherit" color="primary"/>
                                                </Box>
                                                <Typography className={classes.smallCardText} variant="h5"
                                                            color="textPrimary" gutterBottom>
                                                    Something
                                                </Typography>
                                                <Typography className={classes.bigCardText} variant="h1">
                                                    20+
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Card>
                                            <CardContent className={classes.cardContent}>
                                                <Box fontSize="60px">
                                                    <EventNoteOutlinedIcon fontSize="inherit" color="primary"/>
                                                </Box>
                                                <Typography className={classes.smallCardText} variant="h5"
                                                            color="textPrimary" gutterBottom>
                                                    Something
                                                </Typography>
                                                <Typography className={classes.bigCardText} variant="h1">
                                                    3000+
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </Grid>

                                    <Grid item xs={12} sm={6}>
                                        <Card>
                                            <CardContent className={classes.cardContent}>
                                                <Box fontSize="60px">
                                                    <PeopleAltOutlinedIcon fontSize="inherit" color="primary"/>
                                                </Box>
                                                <Typography className={classes.smallCardText} variant="h5"
                                                            color="textPrimary" gutterBottom>
                                                    Something
                                                </Typography>
                                                <Typography className={classes.bigCardText} variant="h1">
                                                    1000+
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Card>
                                            <CardContent className={classes.cardContent}>
                                                <Box fontSize="60px">
                                                    <LinearScaleOutlinedIcon fontSize="inherit" color="primary"/>
                                                </Box>
                                                <Typography className={classes.smallCardText} variant="h5"
                                                            color="textPrimary" gutterBottom>
                                                    Something
                                                </Typography>
                                                <Typography className={classes.bigCardText} variant="h1">
                                                    30+
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

export default compose(
    withWidth(),
    withStyles(styles)
)(InterestingFacts);
