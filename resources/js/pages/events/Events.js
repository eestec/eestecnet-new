import React, { Component } from 'react';
import { connect } from 'react-redux';

import Http from '../../Http';
import { API_EVENT_RESOURCE } from '../../BackendRoutes';
import {withLayout} from "../../components/layouts";

import {Typography, Container, Table, TableHead, TableBody, TableRow, TableCell, TableSortLabel, Button, Link, Select, Input, MenuItem, Checkbox, ListItemText} from "@material-ui/core";
import {compose} from "recompose";
import withStyles from "@material-ui/core/styles/withStyles";
import Alert from "@material-ui/lab/Alert";
import Moment from "moment";
import {withDefault, BooleanParam, StringParam, QueryParams} from 'use-query-params';
import * as QueryString from 'query-string';
import * as PropTypes from 'prop-types';

const styles = theme => ({
    main: {
      backgroundColor: theme.palette.secondary.main,
      minHeight: 500
    },
    paper: {
      marginTop: theme.spacing(8),
      paddingTop: 60,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    }
});

const queryConfig = {
    sort: withDefault(StringParam, 'starts'),
    order: withDefault(StringParam, 'desc'),
    open: BooleanParam,
    pending: BooleanParam,
    ongoing: BooleanParam,
    past: BooleanParam,
};

class Events extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            data: {},
            pageFrom: 1,
            pageTo: 15,
            itemCount: 0,
            lastPage: 1,
            perPage: 0,
            moreLoaded: false,
            error: false,
        };
    }

    buildEventsUrl = (base = API_EVENT_RESOURCE()) => {
        let uri = QueryString.parseUrl(base, {parseBooleans: true});
        const q = QueryString.parse(this.props.location.search);
        uri.query.open = q.open;
        uri.query.pending = q.pending;
        uri.query.ongoing = q.ongoing;
        uri.query.past = q.past;
        uri.query.sort = q.sort;
        uri.query.order = q.order;
        return QueryString.stringifyUrl(uri, {encode: true, skipEmptyString: true, skipNull: true});
    };

    componentDidMount() {
        Http.get(this.buildEventsUrl())
            .then((response) => {
                const resp = response.data;
                this.setState({
                    data: resp.data,
                    pageFrom: resp.from,
                    pageTo: resp.to,
                    itemCount: resp.total,
                    lastPage: resp.last_page,
                    perPage: resp.per_page,
                    nextPageUrl: resp.next_page_url,
                    loading: false,
                    error: false,
                });
            })
            .catch(() => {
                this.setState({
                    error: 'Unable to fetch events.',
                });
            });
    }

    loadMore = () => {
        this.setState({ loading: true });
        Http.get(this.buildEventsUrl(this.state.nextPageUrl))
            .then((response) => {
                const resp = response.data;
                const mergedData = this.state.data.concat(resp.data);
                this.setState({
                    data: mergedData,
                    pageFrom: resp.from,
                    pageTo: resp.to,
                    itemCount: resp.total,
                    lastPage: resp.last_page,
                    perPage: resp.per_page,
                    nextPageUrl: resp.next_page_url,
                    loading: false,
                    moreLoaded: true,
                    error: false,
                });
            })
            .catch(() => {
                this.setState({
                    error: 'Unable to fetch events.',
                });
            });
    }

    deleteEvent = (e) => {
        const { key } = e.currentTarget.dataset;
        const { data: events } = this.state;

        //console.log({key}.key);
        Http.delete(`${API_EVENT_RESOURCE()}/${key}`)
          .then((response) => {
            if (response.status === 202) {
              const index = events.findIndex(event => event.slug === key);
              const update = [...events.slice(0, index), ...events.slice(index + 1)];
                // TODO: update item count and indices
              this.setState({ data: update });
            } else {
                console.error(response);
            }
          })
          .catch((error) => {
              console.log(error);
          });
    }

    render() {
        const { loading, error, pageTo, itemCount } = this.state;
        const events = Array.from(this.state.data);
        const {classes} = this.props;
        //console.log(events);

        return (
            <React.Fragment>
                <main className={classes.main}>
                    <Container>
                        <QueryParams config={queryConfig}>
                            {({ query, setQuery }) => {
                                return (
                                    <div className={classes.paper}>
                                        <Typography component="h1" variant="h5">
                                            Events
                                        </Typography>

                                        <Select
                                            multiple
                                            value={[query.open, query.pending, query.ongoing, query.past]}
                                            onChange={(event, child) => {
                                                // TODO: update query and filter events
                                            }}
                                            input={<Input />}
                                            renderValue={(selected) => {
                                                const names = ['open', 'pending', 'ongoing', 'past'];
                                                let selectedNames = [];
                                                for (let i = 0; i < selected.length; i++) {
                                                    if (selected[i]) {
                                                        selectedNames.push(names[i]);
                                                    }
                                                }
                                                if (selectedNames.length > 0) {
                                                    return selectedNames.join(', ');
                                                }
                                                return 'all';
                                            }}
                                            MenuProps={{
                                                PaperProps: {
                                                    style: {
                                                        maxHeight: 48 * 4.5 + 8,
                                                        width: 250,
                                                    },
                                                },
                                            }}>
                                            {['open', 'pending', 'ongoing', 'past'].map((name) => (
                                                <MenuItem key={name} value={name}>
                                                    <Checkbox checked={query[name]} />
                                                    <ListItemText primary={name} />
                                                </MenuItem>
                                            ))}
                                        </Select>

                                        {error &&
                                            <Alert severity="error">{error}</Alert>
                                        }

                                        <Table>
                                            <EventTableHead
                                                order={query.order}
                                                orderBy={query.sort}
                                                onRequestSort={(event, property) => {
                                                    const isAsc = query.sort === property && query.order === 'asc';
                                                    setQuery({
                                                        order: isAsc ? 'desc' : 'asc',
                                                        sort: property,
                                                    });
                                                    // TODO: sort events
                                                }}/>
                                            <TableBody>
                                                {events.map(event =>
                                                    (
                                                        <TableRow key={event.id}>
                                                            <TableCell>{Moment(event.starts).format('YYYY.\xa0MM.\xa0DD.')}</TableCell>
                                                            <TableCell>{Moment(event.ends).format('YYYY.\xa0MM.\xa0DD.')}</TableCell>
                                                            <TableCell><Link href={'/events/' + event.slug}>{event.name}</Link></TableCell>
                                                            <TableCell>{event.location}</TableCell>
                                                            <TableCell>{event.fee}&nbsp;€</TableCell>
                                                            <TableCell>{event.max_pax}</TableCell>
                                                            <TableCell>{Moment(event.deadline).format('YYYY.\xa0MM.\xa0DD. HH:mm:ss')}</TableCell>
                                                            <TableCell>
                                                                <Button
                                                                    onClick={this.deleteEvent}
                                                                    data-key={event.slug}
                                                                >Delete</Button>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))
                                                }
                                            </TableBody>
                                        </Table>

                                        {pageTo < itemCount &&
                                            <div className="text-center">
                                                <Button
                                                    variant="outlined"
                                                    onClick={loading ? null : this.loadMore}
                                                    disabled={loading}
                                                >{loading ? 'Please wait...' : 'Load more'}</Button>
                                            </div>
                                        }

                                        {(pageTo === itemCount) && (this.state.moreLoaded === true) &&
                                            <div className="text-center">
                                                <p>Everything loaded.</p>
                                            </div>
                                        }
                                    </div>
                                );
                            }}
                        </QueryParams>
                    </Container>
                </main>
            </React.Fragment>
        );
    }
}

const headCells = [
    { id: 'starts', label: 'Start date' },
    { id: 'ends', label: 'End date' },
    { id: 'name', label: 'Name' },
    { id: 'location', label: 'Location' },
    { id: 'fee', label: 'Fee' },
    { id: 'max_pax', label: 'Max pax' },
    { id: 'deadline', label: 'Application dl.' },
    { id: 'actions', label: 'Actions' },
];

function EventTableHead(props) {
    const { order, orderBy, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={'center'}
                        sortDirection={orderBy === headCell.id ? order : false}>
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}>
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <span style={{display: 'none'}}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </span>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EventTableHead.propTypes = {
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
    user: state.Auth.user,
});

export default compose(
    connect(mapStateToProps),
    withLayout,
    withStyles(styles)
)(Events);

