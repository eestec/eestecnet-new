import React, {Component} from 'react';
import Container from "@material-ui/core/Container";
import {compose} from "recompose";
import {withLayout} from "../../components/layouts";

class Partners extends Component {

    render() {
        return (
            <React.Fragment>
                <Container>
                    <div className="partners">
                        Partners
                    </div>
                </Container>
            </React.Fragment>
        );
    }
}

export default compose(
    withLayout
)(Partners);
