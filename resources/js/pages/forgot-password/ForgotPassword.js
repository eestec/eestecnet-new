import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import ReeValidate from 'ree-validate';
import * as AuthService from '../../services/authService';
import {Button, TextField, Typography, Container} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import {compose} from "recompose";
import {withLayout} from "../../components/layouts";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
    main: {
      backgroundColor: theme.palette.secondary.main,
    },
    paper: {
      marginTop: theme.spacing(8),
      paddingTop: 60,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
});

class ForgotPassword extends Component {

    constructor(props) {
        super(props);

        this.validator = new ReeValidate({
            email: 'required|email',
        });

        this.state = {
            loading: false,
            email: '',
            errors: {},
            response: {
                error: false,
                message: '',
            },
        };
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });

        // If a field has a validation error, we'll clear it when corrected
        const { errors } = this.state;
        if (name in errors) {
            const validation = this.validator.errors;
            this.validator.validate(name, value).then(() => {
                if (!validation.has(name)) {
                    delete errors[name];
                    this.setState({ errors });
                }
            });
        }
    }

    handleBlur = (e) => {
        const { name, value } = e.target;
        const validation = this.validator.errors;

        // Avoid validation until input has a value
        if (value === '') {
            return;
        }

        this.validator.validate(name, value).then(() => {
            if (validation.has(name)) {
                const { errors } = this.state;
                errors[name] = validation.first(name);
                this.setState({ errors });
            }
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const credentials = {
            email: this.state.email,
        };

        // Set response state back to default
        this.setState({ response: { error: false, message: '' } });

        this.validator.validateAll(credentials)
            .then((success) => {
                if (success) {
                    this.setState({ loading: true });

                    this.props.dispatch(AuthService.resetPassword(credentials))
                        .then((res) => {
                            this.passwordForm.reset();
                            const response = {
                                error: false,
                                message: res.message,
                            };
                            this.setState({ loading: false, success: true, response });
                        })
                        .catch((err) => {
                            this.passwordForm.reset();
                            const errors = Object.values(err.errors);
                            errors.join(' ');
                            const response = {
                                error: true,
                                message: errors,
                            };
                            this.setState({ response });
                            this.setState({ loading: false });
                        });
                }
            });
    }

    render() {
        // If user is already authenticated we redirect to entry location
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        const { isAuthenticated } = this.props;
        if (isAuthenticated) {
            return (
                <Redirect to={from} />
            );
        }

        const { response, errors, loading } = this.state;
        const {classes} = this.props;

        return (
            <React.Fragment>
                <main className={classes.main}>
                <Container maxWidth="xs">
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                    Request password reset
                    </Typography>
                    {this.state.success &&
                        <Alert severity="success" className="text-center">A password reset link has been sent!</Alert>
                    }
                    {response.error &&
                        <Alert severity="error" className="text-center">{response.message}</Alert>
                    }

                    {!this.state.success &&
                    <form onSubmit={this.handleSubmit} ref={(el) => { this.passwordForm = el; }}>

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        type="email"
                        name="email"
                        label="Email address"
                        error={'email' in errors}
                        placeholder="Enter e-mail"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('email' in errors) &&
                        <Alert severity="warning">{errors.email}</Alert>
                    }

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={loading}
                    >{loading ? 'Please wait...' : 'Send password reset e-mail'}
                    </Button>
                    </form>
                    }
                </div>
                </Container>
                </main>
            </React.Fragment>
        );
    }
}

ForgotPassword.defaultProps = {
    location: {
        state: {
            pathname: '/',
        },
    },
};

ForgotPassword.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    location: PropTypes.shape({
        state: {
            pathname: PropTypes.string,
        },
    }),
};

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
});

export default compose(
    connect(mapStateToProps),
    withLayout,
    withStyles(styles)
)(ForgotPassword);
