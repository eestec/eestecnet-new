import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import ReeValidate from 'ree-validate';
import classNames from 'classnames';
import * as AuthService from '../../services/authService';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import {compose} from "recompose";
import {withLayout} from "../../components/layouts";
import withStyles from "@material-ui/core/styles/withStyles";


const styles = theme => ({
  main: {
    backgroundColor: theme.palette.secondary.main,
    height: 500
  },
  paper: {
    marginTop: theme.spacing(8),
    paddingTop: 60,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class Login extends Component {

    constructor() {
        super();

        this.validator = new ReeValidate({
            email: 'required|email',
            password: 'required|min:8',
        });

        this.state = {
            loading: false,
            email: '',
            password: '',
            remember: false,
            errors: {},
            response: {
                error: false,
                message: '',
            },
        };
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });

        // If a field has a validation error, we'll clear it when corrected
        const { errors } = this.state;
        if (name in errors) {
            const validation = this.validator.errors;
            this.validator.validate(name, value).then(() => {
                if (!validation.has(name)) {
                    delete errors[name];
                    this.setState({ errors });
                }
            });
        }
    }

    handleBlur = (e) => {
        const { name, value } = e.target;

        // Avoid validation until input has a value
        if (value === '') {
            return;
        }

        const validation = this.validator.errors;
        this.validator.validate(name, value).then(() => {
            if (validation.has(name)) {
                const { errors } = this.state;
                errors[name] = validation.first(name);
                this.setState({ errors });
            }
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { email, password, remember } = this.state;
        const credentials = { email, password, remember };

        // Set response state back to default
        this.setState({ response: { error: false, message: '' } });

        this.validator.validateAll(credentials)
            .then((success) => {
                if (success) {
                    this.setState({ loading: true });
                    this.submit(credentials);
                }
            });
    }

    submit(credentials) {
        this.props.dispatch(AuthService.login(credentials))
            .catch((err) => {
                this.loginForm.reset();
                const errors = Object.values(err.errors);
                errors.join(' ');
                const response = {
                    error: true,
                    message: errors,
                };
                this.setState({ response });
                this.setState({ loading: false });
            });
    }

    render() {
        const {classes} = this.props;
        // If user is already authenticated we redirect to entry location
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        if (this.props.isAuthenticated) {
            return (
                <Redirect to={from} />
            );
        }

        const { response, errors, loading } = this.state;

        return (
            <React.Fragment>
                <main className={classes.main}>
                <Container maxWidth="xs">
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                    Sign in
                    </Typography>
                    <form className={classes.form} onSubmit={this.handleSubmit} ref={(el) => { this.loginForm = el; }}>

                        {response.error &&
                            <Alert severity="error">{response.message}</Alert>
                        }

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        type="email"
                        className={classNames({ 'is-invalid': ('email' in errors) })}
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('email' in errors) &&
                        <Alert severity="warning">{errors.email}</Alert>
                    }
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        className={classNames({ 'is-invalid': ('password' in errors) })}
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        disabled={loading}
                    />
                    {('password' in errors) &&
                        <Alert severity="warning">{errors.password}</Alert>
                    }
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                        type="checkbox"
                        id="remember"
                        name="remember"
                        onChange={this.handleChange}
                        disabled={loading}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={loading}
                    >
                        {loading ? 'Please wait...' : 'Login'}
                    </Button>
                    <Grid container>
                        <Grid item xs>
                            <Link href="/forgot-password" variant="body2">
                                Forgot password?
                            </Link>
                        </Grid>
                        <Grid item>
                            <Link href="/register" variant="body2">
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                    </form>
                </div>

                </Container>
                </main>
            </React.Fragment>
        );
    }
}

Login.defaultProps = {
    location: {
        state: {
            pathname: '/',
        },
    },
};

Login.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    location: PropTypes.shape({
        state: {
            pathname: PropTypes.string,
        },
    }),
};

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
});

export default compose(
    connect(mapStateToProps),
    withLayout,
    withStyles(styles)
)(Login);
