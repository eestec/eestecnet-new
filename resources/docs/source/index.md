---
title: API Reference

language_tabs:

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.

<!-- END_INFO -->

#general


<!-- START_379a3beb17bbb91528d80d8507f69655 -->
## Get event details

Returns details about the specified event.

> Example request:


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/events/{event}`


<!-- END_379a3beb17bbb91528d80d8507f69655 -->

<!-- START_6858688d774fcceaa19269573d1f5044 -->
## Apply to event

Creates a new event application with the motivational letter
provided in the request body ('letter').

> Example request:



### HTTP Request
`POST api/events/{event}/apply`


<!-- END_6858688d774fcceaa19269573d1f5044 -->

<!-- START_9e1287cfe7bfb73fd085f712fc567aff -->
## Update application

Only the motivational letter can be modified. Users can only modify
their own applications.

> Example request:



### HTTP Request
`PATCH api/events/{event}/applications/{application}`


<!-- END_9e1287cfe7bfb73fd085f712fc567aff -->

<!-- START_ea38759780b699eee1c296df6989e496 -->
## Update application

Only the motivational letter can be modified. Users can only modify
their own applications.

> Example request:



### HTTP Request
`PUT api/events/{event}/applications/{application}`


<!-- END_ea38759780b699eee1c296df6989e496 -->

<!-- START_4aec8f28f423ee1ef99af6409ea07b50 -->
## Get motivational letter

Used when a user wants to modify their motivational letter.

> Example request:


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/events/{event}/applications/{application}/letter`


<!-- END_4aec8f28f423ee1ef99af6409ea07b50 -->

<!-- START_3389cf223f446d998a46f8543d8e6a71 -->
## Cancel application

An application cannot be cancelled after the deadline. Users can only cancel
their own applications.

> Example request:



### HTTP Request
`DELETE api/events/{event}/applications/{application}`


<!-- END_3389cf223f446d998a46f8543d8e6a71 -->

<!-- START_724bec6ee2613973db0138c25154af16 -->
## Confirm participation

Confirms an user's participation in an event. Users can only confirm their
own applications. Users can only confirm their participation after the deadline
if they are accepted.

> Example request:



### HTTP Request
`POST api/events/{event}/applications/{application}/confirm`


<!-- END_724bec6ee2613973db0138c25154af16 -->

<!-- START_a4040124a509592f4276f7b03723b377 -->
## Get all event applications

Returns a list of all applications for an event. Only event organizers can access.

> Example request:


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/events/{event}/applications`


<!-- END_a4040124a509592f4276f7b03723b377 -->

<!-- START_b6ddcf606dec25a5875cfeb77c7444be -->
## Get applicaiton

Returns the specified application. Only event organizers can access.

> Example request:


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/events/{event}/applications/{application}`


<!-- END_b6ddcf606dec25a5875cfeb77c7444be -->

<!-- START_66057827c0488508ca9b8462a02ea0a6 -->
## Accept or decline a list of applications

The request body can contain two lists: accept_ids and decline_ids. All applications
from accept_ids are accepted and all applications from decline_ids are declined.

> Example request:



### HTTP Request
`POST api/events/{event}/applications/accept`


<!-- END_66057827c0488508ca9b8462a02ea0a6 -->

<!-- START_3744a410bf9935f038d97a3101e45926 -->
## Accept application

> Example request:



### HTTP Request
`POST api/events/{event}/applications/{application}/accept`


<!-- END_3744a410bf9935f038d97a3101e45926 -->

<!-- START_d9a7822dd4898276e154d4d0c1daba28 -->
## Decline application

> Example request:



### HTTP Request
`POST api/events/{event}/applications/{application}/decline`


<!-- END_d9a7822dd4898276e154d4d0c1daba28 -->

<!-- START_b796c035f9fc89ed486fcb35d5809936 -->
## Get participant details

> Example request:


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/events/{event}/applications/{application}/details`


<!-- END_b796c035f9fc89ed486fcb35d5809936 -->

<!-- START_24fa4f26ca3bb42f955f1b5ee2ea6d1a -->
## Submit participant&#039;s details

Stores a participant's details in the database.
Request body: [application_id, arrival, departure, arrival_location, means, allergies, food_prefs, other]

> Example request:



### HTTP Request
`POST api/events/{event}/applications/{application}/details`


<!-- END_24fa4f26ca3bb42f955f1b5ee2ea6d1a -->

<!-- START_379a30feb2949828b5f95efbfd7649c3 -->
## Delete event

> Example request:



### HTTP Request
`DELETE api/events/{event}`


<!-- END_379a30feb2949828b5f95efbfd7649c3 -->

<!-- START_4f27f341ccb7a903258e0a0f908d1247 -->
## Update event

> Example request:



### HTTP Request
`PATCH api/events/{event}`


<!-- END_4f27f341ccb7a903258e0a0f908d1247 -->

<!-- START_388e610b5754b1df34564c1c6c66a126 -->
## Update event

> Example request:



### HTTP Request
`PUT api/events/{event}`


<!-- END_388e610b5754b1df34564c1c6c66a126 -->


