@extends('layouts.app')

@section('content')

<!-- TODO: create profile edit page in rect and delete this file -->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Profile') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('profile-update') }}" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" name="user_id" value="{{ $user->id }}"/>

                        <div class="form-group row">
                            <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ $user->fname }}" required autocomplete="fname" autofocus>

                                @error('fname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ $user->lname }}" required autocomplete="lname" autofocus>

                                @error('lname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date_of_birth" class="col-md-4 col-form-label text-md-right">{{ __('Date of Birth') }}</label>

                            <div class="col-md-6">
                                <input id="date_of_birth" type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" value="{{ $user->date_of_birth }}" required autofocus>

                                @error('date_of_birth')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                            <div class="col-md-6">
                                <select id="gender" class="form-control @error('gender') is-invalid @enderror" name="gender" required>
                                    @php
                                    $curGender = old('gender') ?? $user->gender;
                                    @endphp
                                    <option disabled {{ isset($curGender) ? '' : 'selected' }}>{{ __('-- Select one --') }}</option>
                                    @foreach ($genders as $gender)
                                    <option value='{{ $gender }}' {{ $curGender == $gender ? 'selected' : '' }}>{{ __(ucfirst($gender)) }}</option>
                                    @endforeach
                                </select>

                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email" aria-describedby="emailHelp">
                                <small id="emailHelp" class="form-text text-muted">{{ __('Note: changing the email address requires its verification.') }}</small>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile Number') }}</label>

                            <div class="col-md-6">
                                <input id="mobile" type="phone" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ $user->mobile == null ? null : ('+' . $user->mobile) }}" autocomplete="new-mobile">

                                @error('mobile')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="passport_no" class="col-md-4 col-form-label text-md-right">{{ __('Passport Number') }}</label>

                            <div class="col-md-6">
                                <input id="passport_no" type="text" class="form-control @error('passport_no') is-invalid @enderror" name="passport_no" autocomplete="new-passport_no">

                                @error('passport_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="allergies" class="col-md-4 col-form-label text-md-right">{{ __('Allergies') }}</label>

                            <div class="col-md-6">
                                <input id="allergies" type="text" class="form-control" name="allergies" value="{{ $user->allergies }}" autocomplete="new-allergies">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fos" class="col-md-4 col-form-label text-md-right">{{ __('Field of Study') }}</label>

                            <div class="col-md-6">
                                <select id="fos" class="form-control @error('fos') is-invalid @enderror" name="fos">
                                    @php
                                    $curFos = old('fos') ?? $user->fos;
                                    @endphp
                                    <option disabled {{ isset($curFos) ? '' : 'selected' }}>{{ __('-- Select one --') }}</option>
                                    @foreach ($fosList as $fos => $name)
                                    <option value='{{ $fos }}' {{ $curFos == $fos ? 'selected' : '' }}>{{ $name }}</option>
                                    @endforeach
                                </select>

                                @error('fos')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tshirt" class="col-md-4 col-form-label text-md-right">{{ __('T-Shirt Size') }}</label>

                            <div class="col-md-6">
                                <select id="tshirt" class="form-control @error('tshirt') is-invalid @enderror" name="tshirt">
                                    @php
                                    $curTshirt = old('tshirt') ?? $user->tshirt;
                                    @endphp
                                    <option disabled {{ isset($curTshirt) ? '' : 'selected' }}>{{ __('-- Select one --') }}</option>
                                    @foreach ($tshirtSizes as $tshirt)
                                    <option value='{{ $tshirt }}' {{ $curTshirt == $tshirt ? 'selected' : '' }}>{{ strtoupper($tshirt) }}</option>
                                    @endforeach
                                </select>

                                @error('tshirt')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="food_pref" class="col-md-4 col-form-label text-md-right">{{ __('Food Preference') }}</label>

                            <div class="col-md-6">
                                <select id="food_pref" class="form-control @error('food_pref') is-invalid @enderror" name="food_pref">
                                    @php
                                    $curPref = old('food_pref') ?? $user->food_pref;
                                    @endphp
                                    <option disabled {{ isset($curPref) ? '' : 'selected' }}>{{ __('-- Select one --') }}</option>
                                    @foreach ($foodPrefList as $pref => $name)
                                    <option value='{{ $pref }}' {{ $curPref == $pref ? 'selected' : '' }}>{{ $name }}</option>
                                    @endforeach
                                </select>

                                @error('food_pref')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Profile Picture') }}</label>

                            <div class="col-md-6">
                                @php
                                $picMethod = old('picture_method') ?? 'keep';
                                $picKeep = $picMethod == 'keep';
                                $picUpdate = $picMethod == 'update';
                                $picRemove = $picMethod == 'remove';
                                @endphp
                                <ul class="nav nav-pills mb-3" id="picture-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link {{ $picKeep ? 'active' : ''}}" id="picture-keep-tab" data-toggle="pill" href="#picture-keep" role="tab" aria-controls="picture-keep" aria-selected="{{ $picKeep }}">{{ __ ('Keep current') }}</a>
                                        <input type="radio" name="picture_method" value="keep" class="d-none" {{ $picKeep ? 'checked' : ''}}>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link {{ $picUpdate ? 'active' : ''}}" id="picture-update-tab" data-toggle="pill" href="#picture-update" role="tab" aria-controls="picture-update" aria-selected="{{ $picUpdate }}">{{ __('Update') }}</a>
                                        <input type="radio" name="picture_method" value="update" class="d-none" {{ $picUpdate ? 'checked' : ''}}>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link {{ isset($user->thumbnail) ? 'text-danger' : 'disabled' }}{{ $picRemove ? ' active' : ''}}" id="picture-remove-tab" data-toggle="pill" href="#picture-remove" role="tab" aria-controls="picture-remove" aria-selected="{{ $picRemove }}" {{ isset($user->thumbnail) ? '' : 'aria-disabled="true"' }}>{{ __('Remove') }}</a>
                                        <input type="radio" name="picture_method" value="remove" class="d-none" {{ $picRemove ? 'checked' : ''}}>
                                    </li>
                                </ul>
                                <div class="tab-content" id="picture-tabContent">
                                    <div class="tab-pane fade {{ $picKeep ? 'show active' : ''}}" id="picture-keep" role="tabpanel" aria-labelledby="picture-keep-tab">
                                        @if (isset($user->thumbnail))
                                        <img class="img-thumbnail mb-3" src="/img/profile/{{ $user->id }}" />
                                        @else
                                        <p>{{ __('You have no profile picture set yet.') }}</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade {{ $picUpdate ? 'show active' : ''}}" id="picture-update" role="tabpanel" aria-labelledby="picture-update-tab">
                                        <input id="picture" type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control-file @error('picture') is-invalid @enderror" name="picture" aria-describedby="pictureHelp">
                                        <small id="pictureHelp" class="form-text text-muted">{{ __('Please upload a valid image file: size should not be more than 2 MB and 250x250 px in dimensions at least (preferably rectangular).') }}</small>

                                        @error('picture')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="tab-pane fade {{ $picRemove ? 'show active' : ''}}" id="picture-remove" role="tabpanel" aria-labelledby="picture-remove-tab">
                                        <p>{{ __('The current profile picture will be removed on save.') }}</p>
                                    </div>
                                </div>

                                <script>
                                    // TODO: this sould be only a temporaly solution - create in reactjs
                                    document.addEventListener("DOMContentLoaded", function() {
                                        var container = document.querySelector('#picture-tab');
                                        var tabs = container.querySelectorAll('a[data-toggle="pill"]');
                                        for (var i = 0; i < tabs.length; i++) {
                                            tabs[i].addEventListener('click', function (e) {
                                                // remove button style 'hack'
                                                var remBtn = container.querySelector('a#picture-remove-tab');
                                                if (!remBtn.classList.contains('disabled')) {
                                                    if (e.target.id == remBtn.id) {
                                                        remBtn.classList.add('text-white', 'bg-danger');
                                                        remBtn.classList.remove('text-danger');
                                                    } else {
                                                        remBtn.classList.remove('text-white', 'bg-danger');
                                                        remBtn.classList.add('text-danger');
                                                    }
                                                }
                                                // set sibling radio checked to know in the controller which tab was selected
                                                e.target.parentElement.querySelector('input[type="radio"]').checked = true;
                                            });
                                        }
                                    });
                                </script>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route('profile', \Illuminate\Support\Facades\Auth::user()->id == $user->id ? null : ['slug' => $user->slug]) }}" class="btn btn-outline-primary">{{ __('Cancel') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
