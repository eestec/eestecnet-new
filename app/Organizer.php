<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Carbon;

/**
 * App\Organizer
 *
 * @property int $id
 * @property int $organizer_id
 * @property string $organizer_type
 * @property int $event_id
 * @property string $role
 * @property string|null $role_other
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Event $event
 * @property-read User $user
 * @method static Builder|Organizer newModelQuery()
 * @method static Builder|Organizer newQuery()
 * @method static Builder|Organizer query()
 * @method static Builder|Organizer whereCreatedAt($value)
 * @method static Builder|Organizer whereEventId($value)
 * @method static Builder|Organizer whereId($value)
 * @method static Builder|Organizer whereRole($value)
 * @method static Builder|Organizer whereRoleOther($value)
 * @method static Builder|Organizer whereUpdatedAt($value)
 * @method static Builder|Organizer whereUserId($value)
 * @mixin Eloquent
 */
class Organizer extends Pivot
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organizer_id', 'organizer_type', 'event_id', 'role', 'role_other',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
