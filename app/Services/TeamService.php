<?php


namespace App\Services;

use Illuminate\Http\Request;

class TeamService
{
    public function validateTeam(Request $request): array
    {
        return $request->validate([
            'name' => 'required',
            'category' => 'required|in:team,project,ba',
            'thumbnail' => 'required',
            'description' => 'required',
            'founded' => 'required',
        ]);
    }
}
