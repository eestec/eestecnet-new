<?php


namespace App\Services;


use App\Application;
use App\Event;
use App\Helpers\ValidationResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// TODO: update column names / model fields
class ApplicationService
{
    public function validateApplication(Request $request, Event $event): ValidationResponse
    {
        if (!$request['letter']) {
            return new ValidationResponse(false, 'A motivational letter is required.');
        }
        if ($this->isPastDeadline($event)) {
            return new ValidationResponse(false, 'Cannot apply after the deadline.');
        }
        if (Application::where('event_id', $event->id)
                ->where('user_id', $request['user_id'])
                ->count() > 0) {
            return new ValidationResponse(false, 'The same user cannot apply to the same event twice.');
        }
        if ($event->organizers()->where('organizer_id', $request['user_id'])->count() > 0) {
            return new ValidationResponse(false, 'Organizers cannot apply to their own events.');
        }

        return new ValidationResponse(true);
    }

    public function isPastDeadline($event)
    {
        return $event->deadline->lessThan(Carbon::now());
    }

    public function validateUpdate(Event $event, Application $application, Request $request): ValidationResponse
    {
        if (!$request['letter']) {
            return new ValidationResponse(false, 'A motivational letter is required.');
        }
        if ($this->isPastDeadline($event)) {
            return new ValidationResponse(false, 'Cannot update application after the deadline.');
        }
        if ($application->user_id != Auth::id()) {
            return new ValidationResponse(false, 'Cannot update another user\'s application.');
        }
        return new ValidationResponse(true);
    }

    public function validateDelete(Event $event, Application $application): ValidationResponse
    {
        if ($this->isPastDeadline($event)) {
            return new ValidationResponse(false, 'Cannot delete application after the deadline.');
        }
        if ($application->user_id != Auth::id()) {
            return new ValidationResponse(false, 'Cannot delete another user\'s application.');
        }
        return new ValidationResponse(true);
    }

    public function validateShowLetter(Event $event, Application $application): ValidationResponse
    {
        if ($this->isPastDeadline($event)) {
            return new ValidationResponse(false, 'Cannot view application after the deadline.');
        }
        if ($application->user_id != Auth::id()) {
            return new ValidationResponse(false, 'Cannot view another user\'s application.');
        }
        return new ValidationResponse(true);
    }

    public function validateConfirm(Event $event, Application $application): ValidationResponse
    {
        if (!$this->isPastDeadline($event)) {
            return new ValidationResponse(false, 'Cannot confirm before the deadline');
        }
        if ($application->user_id != Auth::id()) {
            return new ValidationResponse(false, 'Cannot confirm another user\'s participation.');
        }
        if (!$application->accepted) {
            return new ValidationResponse(false, 'Cannot confirm if not accepted as participant.');
        }
        return new ValidationResponse(true);
    }

    public function validateDetails(Application $application): ValidationResponse
    {
        if (is_null($application->confirmed_at)) {
            return new ValidationResponse(false, 'Cannot submit details if participation not confirmed.');
        }
        if ($application->user_id != Auth::id()) {
            return new ValidationResponse(false, 'Cannot submit another user\'s participation details.');
        }
        return new ValidationResponse(true);
    }


}
