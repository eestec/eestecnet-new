<?php


namespace App\Services;

use Illuminate\Http\Request;

class EventService
{
    public function validateEvent(Request $request): array
    {
        return $request->validate([
            'name' => 'required',
            'type' => 'required|in:ws,op,ex,cg,imw',
            'thumbnail' => 'required',
            'location' => 'required',
            'description' => 'required',
            'max_pax' => 'required',
            'fee' => 'required|min:0',
            'starts' => 'required|date|before:ends',
            'ends' => 'required|date|after:starts',
            'deadline' => 'required|date|before:starts',
        ]);
    }
}
