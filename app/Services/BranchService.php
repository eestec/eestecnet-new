<?php


namespace App\Services;

use Illuminate\Http\Request;

class BranchService
{
    public function validateBranch(Request $request): array
    {
        return $request->validate([
            'name' => 'required',
            'country' => 'required',
            'type' => 'required|in:observer,jlc,lc',
            'thumbnail' => 'required',
            'description' => 'required',
            'region' => 'required|in:1,2,3,4,5,6,7,8,9,10',
            'founded' => 'required',
        ]);
    }
}
