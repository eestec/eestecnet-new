<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Carbon;

/**
 * App\Membership
 *
 * @property int $id
 * @property int $user_id
 * @property int $membership_id
 * @property string $membership_type
 * @property bool $is_board
 * @property bool $is_alumni
 * @property string|null $accepted_at
 * @property int|null $accepted_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Branch $branch
 * @property-read User $user
 * @method static Builder|Membership newModelQuery()
 * @method static Builder|Membership newQuery()
 * @method static Builder|Membership query()
 * @method static Builder|Membership whereAcceptedAt($value)
 * @method static Builder|Membership whereAcceptedBy($value)
 * @method static Builder|Membership whereBranchId($value)
 * @method static Builder|Membership whereCreatedAt($value)
 * @method static Builder|Membership whereId($value)
 * @method static Builder|Membership whereIsAlumni($value)
 * @method static Builder|Membership whereIsBoard($value)
 * @method static Builder|Membership whereUpdatedAt($value)
 * @method static Builder|Membership whereUserId($value)
 * @mixin Eloquent
 */
class Membership extends Pivot
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'membership_id', 'membership_type', 'is_board', 'is_alumni', 'accepted_by'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'accepted_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
