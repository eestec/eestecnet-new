<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Carbon;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * App\Branch
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $country
 * @property string $type
 * @property string $thumbnail
 * @property string $description
 * @property int $region
 * @property string|null $website
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $linkedin
 * @property string|null $address
 * @property Carbon $founded
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|User[] $board
 * @property-read int|null $board_count
 * @property-read Collection|User[] $members
 * @property-read int|null $members_count
 * @method static Builder|Branch newModelQuery()
 * @method static Builder|Branch newQuery()
 * @method static Builder|Branch query()
 * @method static Builder|Branch whereAddress($value)
 * @method static Builder|Branch whereCountry($value)
 * @method static Builder|Branch whereCreatedAt($value)
 * @method static Builder|Branch whereDescription($value)
 * @method static Builder|Branch whereFacebook($value)
 * @method static Builder|Branch whereFounded($value)
 * @method static Builder|Branch whereId($value)
 * @method static Builder|Branch whereInstagram($value)
 * @method static Builder|Branch whereLinkedin($value)
 * @method static Builder|Branch whereName($value)
 * @method static Builder|Branch whereRegion($value)
 * @method static Builder|Branch whereSlug($value)
 * @method static Builder|Branch whereThumbnail($value)
 * @method static Builder|Branch whereType($value)
 * @method static Builder|Branch whereUpdatedAt($value)
 * @method static Builder|Branch whereWebsite($value)
 * @mixin Eloquent
 */
class Branch extends Model
{
    use HasSlug;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'country', 'type', 'thumbnail', 'description', 'region', 'website', 'facebook', 'instagram', 'linkedin', 'address', 'founded',
    ];

    protected $casts = [
        'founded' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function members(): MorphToMany
    {
        return $this->morphToMany('App\User', 'membership', 'membership')
            ->withPivot('is_board', 'is_alumni', 'accepted_at')
            ->withTimestamps();
    }

    public function board(): MorphToMany
    {
        return $this->morphToMany('App\User', 'membership', 'membership')
            ->wherePivot('is_board', '=', true)
            ->withTimestamps();
    }

    public function applications_for_events(): BelongsToMany
    {
        return $this->belongsToMany(Application::class, 'membership', 'membership_id', 'user_id', null, 'user_id')
            ->where('membership_type', '=', 'branch');
        // ->where('accepted', '=', null); // Maybe we can implement filtering on the front-end
    }

    public function events(): MorphToMany
    {
        return $this->morphToMany('App\Event', 'organizer', 'organizer')
            ->withTimestamps();
    }
}
