<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Hshn\Base64EncodedFile\HttpFoundation\File\Base64EncodedFile;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\File\File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('phone', function ($value) {
            return preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-.\\\/]?)?((?:\(?\d+\)?[\-.\\\/]?)*)(?:[\-.\\\/]?(?:#|ext\.?|extension|x)[\-\\\/]?(\d+))?$%i', $value) && strlen($value) >= 10;
        });
        Validator::replacer('phone', function ($attribute) {
            return str_replace(':attribute', $attribute, ':attribute is invalid phone number');
        });
        Schema::defaultStringLength(191);

        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Validator::extend('base64_encoded_file', function ($value) {
            $file = new Base64EncodedFile($value);
            return $file instanceof File;
        });
        Validator::replacer('base64_encoded_file', function ($attribute) {
            return str_replace(':attribute', $attribute, ':attribute is not a Base64 encoded file');
        });
        Relation::morphMap([
            'branch' => 'App\Branch',
            'team' => 'App\Team',
            'user' => 'App\User',
        ]);
    }
}
