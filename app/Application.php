<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Application
 *
 * @property int $id
 * @property int $user_id
 * @property int $event_id
 * @property int $priority
 * @property string $letter
 * @property Carbon|null $confirmed_at
 * @property Carbon|null $reviewed_at
 * @property int|null $reviewed_by
 * @property bool|null $accepted
 * @property Carbon|null $arrival
 * @property string|null $arrival_location
 * @property string $arrival_means
 * @property Carbon|null $departure
 * @property string|null $departure_location
 * @property string $departure_means
 * @property string|null $notes
 * @property string|null $organizer_notes
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Event $event
 * @property-read User $user
 * @method static Builder|Application newModelQuery()
 * @method static Builder|Application newQuery()
 * @method static Builder|Application query()
 * @method static Builder|Application whereAcceptedAt($value)
 * @method static Builder|Application whereArrival($value)
 * @method static Builder|Application whereArrivalLocation($value)
 * @method static Builder|Application whereArrivalMeans($value)
 * @method static Builder|Application whereConfirmedAt($value)
 * @method static Builder|Application whereCreatedAt($value)
 * @method static Builder|Application whereDeparture($value)
 * @method static Builder|Application whereDepartureLocation($value)
 * @method static Builder|Application whereDepartureMeans($value)
 * @method static Builder|Application whereEventId($value)
 * @method static Builder|Application whereId($value)
 * @method static Builder|Application whereLetter($value)
 * @method static Builder|Application whereNotes($value)
 * @method static Builder|Application whereOrganizerNotes($value)
 * @method static Builder|Application whereUpdatedAt($value)
 * @method static Builder|Application whereUserId($value)
 * @mixin Eloquent
 */
class Application extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'letter',
        'user_id',
        'event_id',
        'priority'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'confirmed_at' => 'datetime',
        'reviewed_at' => 'datetime',
        'arrival' => 'datetime',
        'departure' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function is_accepted(): ?bool
    {
        return $this->accepted;
    }

    public function is_confirmed(): bool
    {
        return !is_null($this->confirmed_at);
    }
}
