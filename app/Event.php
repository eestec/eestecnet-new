<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Carbon;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * App\Event
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $type
 * @property string $scope
 * @property Carbon $starts
 * @property Carbon $ends
 * @property Carbon $deadline
 * @property string $thumbnail
 * @property string $location
 * @property int $max_pax
 * @property int $fee
 * @property string $description
 * @property int|null $connected_to
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Application[] $applications
 * @property-read int|null $applications_count
 * @property-read Collection|Organizer[] $organizers
 * @property-read int|null $organizers_count
 * @property-read Collection|User[] $participants
 * @property-read int|null $participants_count
 * @method static Builder|Event newModelQuery()
 * @method static Builder|Event newQuery()
 * @method static Builder|Event query()
 * @method static Builder|Event whereConnectedTo($value)
 * @method static Builder|Event whereCreatedAt($value)
 * @method static Builder|Event whereDeadline($value)
 * @method static Builder|Event whereDescription($value)
 * @method static Builder|Event whereEnds($value)
 * @method static Builder|Event whereFee($value)
 * @method static Builder|Event whereId($value)
 * @method static Builder|Event whereLocation($value)
 * @method static Builder|Event whereMaxPax($value)
 * @method static Builder|Event whereName($value)
 * @method static Builder|Event whereScope($value)
 * @method static Builder|Event whereSlug($value)
 * @method static Builder|Event whereStarts($value)
 * @method static Builder|Event whereThumbnail($value)
 * @method static Builder|Event whereType($value)
 * @method static Builder|Event whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Event extends Model
{
    use HasSlug;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'scope', 'starts', 'ends', 'deadline', 'thumbnail', 'location', 'max_pax', 'fee', 'description', 'connected_to',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'starts' => 'datetime',
        'ends' => 'datetime',
        'deadline' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function participants(): BelongsToMany
    {
        return $this->belongsToMany(User::class, Application::class)
            ->wherePivot('accepted', '=', true)
            ->withTimestamps();
    }

    /**
     * Get all of the users that are organizers for the event.
     */
    public function organizers(): MorphToMany
    {
        return $this->morphedByMany('App\User', 'organizer', 'organizer')
            ->withTimestamps();
    }

    /**
     * Get all of the teams that are organizers for the event.
     */
    public function teams(): MorphToMany
    {
        return $this->morphedByMany('App\Team', 'organizer', 'organizer')
            ->withTimestamps();
    }

    public function getApplicationStatus($userId): string
    {
        $application = $this->applications()->where("user_id", "=", $userId)->first();
        if (is_null($application)) {
            $status = "NOT_APPLIED";
        } elseif (!$application->accepted) {
            $status = "APPLIED";
        } elseif (!$application->confirmed_at) {
            $status = "ACCEPTED";
        } else {
            $status = "CONFIRMED";
        }

        return $status;
    }

    public function applications(): HasMany
    {
        return $this->hasMany(Application::class);
    }

}
