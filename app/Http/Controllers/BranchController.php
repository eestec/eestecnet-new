<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Membership;
use App\Services\BranchService;
use App\User;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class BranchController extends ApiController
{
    /** @var BranchService */
    private BranchService $branchService;

    public function __construct(BranchService $branchService)
    {
        $this->branchService = $branchService;
        $this->middleware('branch_admin')->except(['index', 'show']);
    }

    /**
     * Get all branches
     *
     * Returns a list of all branches stored in the database.
     *
     * @return LengthAwarePaginator|Collection
     */
    public function index()
    {
        if (request()->exists("all"))
            return Branch::all();
        return Branch::query()->paginate(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validated = $this->branchService->validateBranch($request);
        $branch = Branch::create($validated);
        if ($branch) {
            return $this->responseResourceCreated('Branch created.');
        }
        return $this->responseServerError('Failed to create branch.');
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return Branch|JsonResponse
     */
    public function show(string $slug)
    {
        $branch = Branch::whereSlug($slug)->first();
        if ($branch) {
            return $branch;
        }
        return $this->responseResourceNotFound();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $slug
     * @return JsonResponse
     */
    public function update(Request $request, string $slug): JsonResponse
    {
        $branch = Branch::whereSlug($slug)->first();
        if ($branch) {
            $validated = $this->branchService->validateBranch($request);
            $updated = $branch->update($validated);
            if ($updated) {
                return $this->responseResourceUpdated('Branch successfully updated.');
            } else {
                return $this->responseServerError('Branch update failed');
            }
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function destroy(string $slug): JsonResponse
    {
        $branch = Branch::whereSlug($slug)->first();
        if ($branch) {
            $deleted = false;
            try {
                $deleted = $branch->delete();
            } catch (Exception $ex) {
                report($ex);
            }
            if ($deleted) {
                return $this->responseResourceDeleted('Branch successfully deleted.');
            } else {
                return $this->responseServerError('Branch deletion failed.');
            }
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Get all pending member requests for the specified branch
     *
     * @param string $slug
     * @return Collection|User[]|JsonResponse
     */
    public function pendingMembers(string $slug)
    {
        $branch = Branch::whereSlug($slug)->first();
        if ($branch) {
            return $branch->members()
                ->wherePivot('accepted_at', '=')
                ->withPivot('id') // for accepting member
                ->get();
        }
        return $this->responseServerError();
    }

    /**
     * Accept member to branch
     *
     * @param int $id
     * @return JsonResponse
     */
    public function accept(int $id): JsonResponse
    {
        if (auth()->check()) {
            $membership = Membership::whereId($id)->first();
            if ($membership) {
                $membership->accepted_at = now();
                $membership->accepted_by = auth()->id();
                if ($membership->update()) {
                    return $this->responseResourceUpdated('Member accepted.');
                }
                return $this->responseServerError('Failed to accept member.');
            }
            return $this->responseResourceNotFound();
        }
        return $this->responseUnauthorized();
    }

    /**
     * Apply to join a Branch
     *
     * Creates a new application to join a specific Branch
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function apply(string $slug): JsonResponse
    {
        $branch = Branch::whereSlug($slug)->first()->id;
        $user = auth()->id();

        $membership = Membership::create(
            [
                'user_id' => $user,
                'membership_id' => $branch,
                'membership_type' => 'branch'
            ]
        );
        if ($membership) {
            return $this->responseResourceCreated('Applied successfully');
        } else {
            return $this->responseServerError('Failed to apply.');
        }
    }

    /**
     * Get all applications for events, made by members of a branch
     *
     * Returns a list of all applications of members of a branch. Only branch's board can access.
     *
     * @param string $slug
     * @return Collection|JsonResponse
     */
    public function listApplications(string $slug)
    {
        $branch = Branch::whereSlug($slug)->first();
        $applications = $branch->applications_for_events()->get();
        if ($applications) {
            return $applications;
        } else {
            return $this->responseServerError('Failed to fetch applications.');
        }
    }

    /**
     * Fetch all events for a specific Branch
     *
     * Returns a list of all events organized by a Branch
     *
     * @param string $slug
     * @return Collection|JsonResponse
     */
    public function listEvents(string $slug)
    {
        $branch = Branch::whereSlug($slug)->first();
        $events = $branch->events()->get();
        if ($events) {
            return $events;
        } else {
            return $this->responseServerError('Failed to fetch events.');
        }
    }
}
