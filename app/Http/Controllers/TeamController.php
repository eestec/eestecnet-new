<?php

namespace App\Http\Controllers;

use App\Membership;
use App\Services\TeamService;
use App\Team;
use App\User;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class TeamController extends ApiController
{
    /** @var TeamService */
    private TeamService $teamService;

    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
        $this->middleware('team_admin')->except(['index', 'show']);
    }

    /**
     * Get all teams
     *
     * Returns a list of all teams stored in the database.
     *
     * @return LengthAwarePaginator
     */
    public function index(): LengthAwarePaginator
    {
        return Team::query()->paginate(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validated = $this->teamService->validateTeam($request);
        $team = Team::create($validated);
        if ($team) {
            return $this->responseResourceCreated('Team created.');
        }
        return $this->responseServerError('Failed to create team.');
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return Team|JsonResponse
     */
    public function show(string $slug)
    {
        $team = Team::whereSlug($slug)->first();
        if ($team) {
            return $team;
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $slug
     * @return JsonResponse
     */
    public function update(Request $request, string $slug): JsonResponse
    {
        $team = Team::whereSlug($slug)->first();
        if ($team) {
            $validated = $this->teamService->validateTeam($request);
            $updated = $team->update($validated);
            if ($updated) {
                return $this->responseResourceUpdated('Team successfully updated.');
            } else {
                return $this->responseServerError('Team update failed');
            }
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function destroy(string $slug): JsonResponse
    {
        $team = Team::whereSlug($slug)->first();
        if ($team) {
            $deleted = false;
            try {
                $deleted = $team->delete();
            } catch (Exception $ex) {
                report($ex);
            }
            if ($deleted) {
                return $this->responseResourceDeleted('Team successfully deleted.');
            } else {
                return $this->responseServerError('Team deletion failed.');
            }
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Get all pending member requests for the specified team
     *
     * @param string $slug
     * @return Collection|User[]|JsonResponse
     */
    public function pendingMembers(string $slug)
    {
        $team = Team::whereSlug($slug)->first();
        if ($team) {
            return $team->members()
                ->wherePivot('accepted_at', '=')
                ->withPivot('id') // for accepting member
                ->get();
        }
        return $this->responseServerError();
    }

    /**
     * Accept member to team
     *
     * @param int $id Membership id
     * @return JsonResponse
     */
    public function accept(int $id): JsonResponse
    {
        if (auth()->check()) {
            $membership = Membership::whereId($id)->first();
            if ($membership) {
                $membership->accepted_at = now();
                $membership->accepted_by = auth()->id();
                if ($membership->update()) {
                    return $this->responseResourceUpdated('Member accepted.');
                }
                return $this->responseServerError('Failed to accept member.');
            }
            return $this->responseResourceNotFound();
        }
        return $this->responseUnauthorized();
    }

    /**
     * Apply to join a Team
     *
     * Creates a new application to join a specific Team
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function apply(string $slug): JsonResponse
    {
        $team = Team::whereSlug($slug)->first()->id;
        $user = auth()->id();

        Membership::create(
            [
                'user_id' => $user,
                'membership_id' => $team,
                'membership_type' => 'team'
            ]
        );
        return $this->responseResourceCreated('Applied to join team successfully');
    }

    /**
     * Fetch all events for a specific Team
     *
     * Returns a list of all events organized by a Team
     *
     * @param string $slug
     * @return Collection|JsonResponse
     */
    public function listEvents(string $slug): Collection
    {
        $team = Team::whereSlug($slug)->first();
        $events = $team->events()->get();
        if ($events) {
            return $events;
        } else {
            return $this->responseServerError('Failed to fetch events.');
        }
    }
}
