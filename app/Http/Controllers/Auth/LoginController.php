<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LoginController extends ApiController
{

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $credentials = $request->only(['email', 'password']);

            // 1 hour
            $ttl = 60 * 60;
            if ($request->get('remember')) {
                // 30 days
                $ttl *= 24 * 30;
            }

            if (!$token = auth()->setTTL($ttl)->attempt($credentials)) {
                return $this->responseUnauthorized('Credentials were incorrect.');
            }

            if (!is_string($token)) {
                return $this->responseServerError(['Failed to login.', 'Cannot create auth token.']);
            }

            // Get the user data
            /* @var User $user */
            $user = auth()->user();
            if ($user) {
                return $this->responseSuccess('Authorized.', [
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => $ttl * 60, // in seconds
                    'user' => $user ? [
                        'name' => $user->name,
                        'fullName' => $user->full_name,
                        'slug' => $user->slug,
                        'thumbnail' => $user->thumbnail,
                    ] : null,
                ]);
            }
        } catch (Exception $e) {
            report($e);
        }
        return $this->responseServerError('Failed to login.');
    }
}
