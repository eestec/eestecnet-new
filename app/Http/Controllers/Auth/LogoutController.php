<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class LogoutController extends ApiController
{

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout()
    {
        if (auth()->user()) {
            auth()->logout();
            return $this->responseSuccess('Successfully logged out.');
        } else {
            return $this->responseUnauthorized('You were not logged in.');
        }
    }
}
