<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\User;
use Exception;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /*
     * TODO: Implement registration workflow
     *
     * 1. user registers with
     *    - name (identification)
     *    - date of birth (checking if old enough)
     *    - e-mail address (identification and confirmation)
     *    - password (authorization)
     *    - branch (notifying board members to accept)
     *    - with accepting T&C
     * 2. the system sends a confirmation e-mail
     * 3. the registered user confirms registration by clicking on a link
     * 4. when confirming e-mail address, a notification is sent to the board members of the branch selected at registration
     * 5. when one of the board members accepts the user, a new email is sent to the user about he/she has been accepted
     * 6. after accepted as a member, the user can access internal pages, like events
     */

    use RegistersUsers;

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'full_name' => 'required|string|max:255',
            'date_of_birth' => 'required|date_format:Y-m-d|before:today',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'terms' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseUnprocessable($validator->errors()->toArray());
        }

        try {
            $user = $this->create($request->all());
            return $this->responseSuccess('Successful registration.');
        } catch (Exception $e) {
            return $this->responseServerError('Registration error.');
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'full_name' => $data['full_name'],
            'date_of_birth' => $data['date_of_birth'],
            'email' => strtolower($data['email']),
            'password' => Hash::make($data['password']),
        ]);
    }
}
