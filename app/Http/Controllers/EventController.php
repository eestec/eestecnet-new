<?php

namespace App\Http\Controllers;

use App\Event;
use App\Services\EventService;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EventController extends ApiController
{
    private static array $sortableAttrs = ['name', 'type', 'location', 'max_pax', 'fee'];

    /** @var EventService */
    private EventService $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * Display a listing of the resource.
     *
     * Returns a list of all events stored in the database.
     * There are some query filters:
     * - open: Filter all events that are currently open for application.
     * - pending: Filter all events that are in pending state. An event is considered as "pending" if the deadline for application is passed but the event is not yet started.
     * - ongoing: Filter all events that are in progress (started, but not ended yet).
     * - past: Filter all events that ended in the past.
     *
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function index(Request $request): LengthAwarePaginator
    {
        // TODO: do filter and sort at the frontend(?)
        $open = $request->query('open') == true;
        $pending = $request->query('pending') == true;
        $ongoing = $request->query('ongoing') == true;
        $past = $request->query('past') == true;
        $sort = $request->query('sort');
        $orderDesc = $request->query('order') == 'desc';

        if ($sort == null || !in_array($sort, self::$sortableAttrs)) {
            $sort = 'starts';
            $orderDesc = true;
        }

        $events = Event::query();
        $now = now();
        if ($open) {
            $events = $events->whereDate('deadline', '>=', $now);
        }
        if ($pending) {
            $events = $events->orWhere(function ($query) use ($now) {
                $query->where('deadline', '<', $now)->where('starts', '>=', $now);
            });
        }
        if ($ongoing) {
            $events = $events->orWhere(function ($query) use ($now) {
                $query->where('starts', '<', $now)->where('ends', '>=', $now);
            });
        }
        if ($past) {
            $events = $events->orWhereDate('ends', '<', $now);
        }
        if ($orderDesc) {
            $events = $events->orderByDesc($sort);
        } else {
            $events = $events->orderBy($sort);
        }
        return $events->paginate();
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return JsonResponse|Event
     */
    public function show(string $slug)
    {
        $event = Event::whereSlug($slug)->first();
        if ($event) {
            return $event;
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validated = $this->eventService->validateEvent($request);
        $event = Event::create($validated);
        if ($event) {
            return $this->responseResourceCreated('Event created.');
        }
        return $this->responseServerError('Failed to create event.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $slug
     * @return JsonResponse
     */
    public function update(Request $request, string $slug): JsonResponse
    {
        $event = Event::whereSlug($slug)->first();
        if ($event) {
            $validated = $this->eventService->validateEvent($request);
            $updated = $event->update($validated);
            if ($updated) {
                return $this->responseResourceUpdated('Event successfully updated.');
            } else {
                return $this->responseServerError('Event update failed');
            }
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function destroy(string $slug): JsonResponse
    {
        $event = Event::whereSlug($slug)->first();
        if ($event) {
            $deleted = false;
            try {
                $deleted = $event->delete();
            } catch (Exception $ex) {
                report($ex);
            }
            if ($deleted) {
                return $this->responseResourceDeleted('Event successfully deleted.');
            } else {
                return $this->responseServerError('Event deletion failed.');
            }
        }
        return $this->responseResourceNotFound();
    }
}
