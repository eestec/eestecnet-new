<?php

namespace App\Http\Controllers;

use App\Application;
use App\Event;
use App\Services\ApplicationService;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

// TODO: extend ApiController and fix name changes
class ApplicationController extends ApiController
{
    /** @var ApplicationService */
    private ApplicationService $applicationService;

    public function __construct(ApplicationService $applicationService)
    {
        $this->applicationService = $applicationService;
        $this->middleware('event_organizer')->only(['index', 'show', 'acceptList', 'accept', 'decline']);
        $this->middleware('branch_admin')->only('priority');
    }

    /**
     * Get all event applications
     *
     * Returns a list of all applications for an event. Only event organizers can access.
     *
     * @param Event $event
     * @return Collection
     */
    public function index(Event $event): Collection
    {
        return $event->applications()->get();
    }

    /**
     * Apply to event
     *
     * Creates a new event application with the motivational letter
     * provided in the request body ('letter').
     *
     * @param Request $request
     * @param Event $event
     * @return JsonResponse $response
     */
    public function apply(Request $request, Event $event): JsonResponse
    {
        $request['event_id'] = $event->id;
        $request['user_id'] = auth()->id();
        $request['applied_on'] = Carbon::now();
        $validationResponse = $this->applicationService->validateApplication($request, $event);

        if (!$validationResponse->isValid) {
            abort(400, $validationResponse->message);
        }

        Application::create(
            $request->only(['letter', 'user_id', 'event_id', 'applied_on'])
        );
        return $this->responseResourceCreated('Applied to the event successfully');
    }

    /**
     * Get application
     *
     * Returns the specified application. Only event organizers can access.
     *
     * @param Event $event
     * @param Application $application
     * @return JsonResponse|Application
     */
    public function show(Event $event, Application $application)
    {
        if ($event->id != $application->event_id)
            return $this->responseUnauthorized();
        return $application;
    }

    /**
     * Get motivational letter
     *
     * Used when a user wants to modify their motivational letter.
     *
     * @param Event $event
     * @param Application $application
     * @return JsonResponse|Response
     */
    public function showLetter(Event $event, Application $application)
    {
        $validationResponse = $this->applicationService->validateShowLetter($event, $application);
        if (!$validationResponse->isValid)
            return $this->responseUnprocessable($validationResponse->message);
        return $this->responseSuccess('Application updated successfully', ['letter' => $application->letter]);

    }


    /**
     * Update application
     *
     * Only the motivational letter can be modified. Users can only modify
     * their own applications.
     *
     * @param Request $request
     * @param Event $event
     * @param Application $application
     * @return Response|JsonResponse
     */
    public function update(Request $request, Event $event, Application $application)
    {
        $validationResponse = $this->applicationService->validateUpdate($event, $application, $request);
        if (!$validationResponse->isValid)
            return $this->responseUnprocessable($validationResponse->message);
        $application->update($request->only(['letter']));
        return $this->responseSuccess('Application updated successfully');
    }

    /**
     * Cancel application
     *
     * An application cannot be cancelled after the deadline. Users can only cancel
     * their own applications.
     *
     * @param Event $event
     * @param Application $application
     * @return Response|JsonResponse
     * @throws Exception
     */
    public function destroy(Event $event, Application $application)
    {
        $validationResponse = $this->applicationService->validateDelete($event, $application);
        if (!$validationResponse->isValid)
            return $this->responseUnprocessable($validationResponse->message);
        $application->delete();
        return $this->responseSuccess('Application deleted successfully');
    }

    /**
     * Confirm participation
     *
     * Confirms an user's participation in an event. Users can only confirm their
     * own applications. Users can only confirm their participation after the deadline
     * if they are accepted.
     *
     * @param Event $event
     * @param Application $application
     * @return JsonResponse
     */
    public function confirm(Event $event, Application $application): JsonResponse
    {
        $validationResponse = $this->applicationService->validateConfirm($event, $application);
        if (!$validationResponse->isValid)
            return $this->responseUnprocessable($validationResponse->message);
        $application->confirmed_at = Carbon::now();
        $application->save();
        return $this->responseSuccess('Application confirmed successfully');
    }

    /**
     * Accept or decline a list of applications
     *
     * The request body can contain two lists: accept_ids and decline_ids. All applications
     * from accept_ids are accepted and all applications from decline_ids are declined.
     *
     * @param Request $request
     * @param Event $event
     * @return JsonResponse
     */
    public function acceptList(Request $request, Event $event): JsonResponse
    {
        if ($request->has('accept_ids')) {
            Application::whereIn('id', $request->get('accept_ids'))
                ->where('event_id', '=', $event->id)
                ->update([
                    'reviewed_at' => Carbon::now(),
                    'reviewed_by' => auth()->id(),
                    'accepted' => true
                ]);
        }
        if ($request->has('decline_ids')) {
            Application::whereIn('id', $request->get('decline_ids'))
                ->where('event_id', '=', $event->id)
                ->update([
                    'reviewed_at' => Carbon::now(),
                    'reviewed_by' => auth()->id(),
                    'accepted' => false
                ]);
        }
        return $this->responseSuccess('Applications updated successfully');
    }

    /**
     * Accept or decline application
     *
     * @param Event $event
     * @param Application $application
     * @param bool $accepted
     * @return Response|JsonResponse
     */
    public function review(Event $event, Application $application, bool $accepted)
    {
        if ($event->id != $application->event_id)
            return $this->responseUnauthorized();
        $application->reviewed_at = Carbon::now();
        $application->reviewed_by = auth()->id();
        $application->accepted = $accepted;
        if ($application->save())
            return $this->responseSuccess('Application updated successfully');
        else
            return $this->responseServerError('Could not update application');
    }

    /**
     * Set a priority for a certain Application
     *
     * @param Request $request
     * @param Application $application
     * @return JsonResponse
     */
    public function priority(Request $request, Application $application): JsonResponse
    {
        $application->priority = $request;
        if ($application->save())
            return $this->responseSuccess('Priority updated successfully');
        else
            return $this->responseServerError();
    }
}
