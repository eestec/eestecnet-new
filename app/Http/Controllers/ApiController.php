<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

abstract class ApiController extends Controller
{
    /**
     * Returns a generic success (200) JSON response.
     *
     * @param string $message
     * @param array|null $payload
     * @return JsonResponse
     */
    public function responseSuccess($message = 'Success.', $payload = null): JsonResponse
    {
        return $this->success($message, Response::HTTP_OK, $payload);
    }

    /**
     * @param string $message
     * @param int $code
     * @param array|null $payload
     * @return JsonResponse
     */
    private function success(string $message, int $code, $payload = null): JsonResponse
    {
        $data = ['status' => $code, 'message' => $message];
        if (is_array($payload)) {
            $data = array_merge($data, $payload);
        }
        return response()->json($data, $code);
    }

    /**
     * Returns a resource updated JSON response with status code 202.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function responseResourceUpdated($message = 'Resource updated.'): JsonResponse
    {
        return $this->success($message, Response::HTTP_ACCEPTED);
    }

    /**
     * Returns a resource created (201) JSON response.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function responseResourceCreated($message = 'Resource created.'): JsonResponse
    {
        return $this->success($message, Response::HTTP_CREATED);
    }

    /**
     * Returns a resource deleted JSON response with status code 202.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function responseResourceDeleted($message = 'Resource deleted.'): JsonResponse
    {
        return $this->success($message, Response::HTTP_ACCEPTED);
    }

    /**
     * Returns a not found (404) JSON response.
     *
     * @param string|array $errors
     * @return JsonResponse
     */
    public function responseResourceNotFound($errors = 'Resource not found.'): JsonResponse
    {
        return $this->error($errors, Response::HTTP_NOT_FOUND);
    }

    /**
     * @param string|array $errors
     * @param int $code
     * @return JsonResponse
     */
    private function error($errors, int $code): JsonResponse
    {
        if (!is_array($errors)) {
            $errorArray = [$errors];
        } else {
            $errorArray = $errors;
        }
        return response()->json(['status' => $code, 'errors' => $errorArray], $code);
    }

    /**
     * Returns an unauthorized (401) JSON response.
     *
     * @param string|array $errors
     * @return JsonResponse
     */
    public function responseUnauthorized($errors = 'Unauthorized.'): JsonResponse
    {
        return $this->error($errors, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Returns a unprocessable entity (422) JSON response.
     *
     * @param string|array $errors
     * @return JsonResponse
     */
    public function responseUnprocessable($errors = 'Unprocessable entity.'): JsonResponse
    {
        return $this->error($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Returns a server error (500) JSON response.
     *
     * @param array|string $errors
     * @return JsonResponse
     */
    public function responseServerError($errors = 'Server error.'): JsonResponse
    {
        return $this->error($errors, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
