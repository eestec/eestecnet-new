<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Hshn\Base64EncodedFile\HttpFoundation\File\Base64EncodedFile;
use Hshn\Base64EncodedFile\HttpFoundation\File\UploadedBase64EncodedFile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserController extends ApiController
{
    const CVS_DIR = 'user_cvs';

    const GENDER_VALUES = ['female', 'male', 'other'];
    const TSHIRT_SIZE_VALUES = ['xs', 's', 'm', 'l', 'xl', 'xxl', 'xxxl'];
    const FOOD_PREFERENCES = [
        'none' => 'None',
        'vegan' => 'Vegan',
        'o-veg' => 'Ovo-vegetarian',
        'l-veg' => 'Lacto-vegetarian',
        'ol-veg' => 'Ovo-lacto vegetarian',
        'pesc' => 'Pescetarian',
        'meat' => 'Meat fan',
    ];
    const FIELD_OF_STUDIES = [
        'none' => 'None',
        'ee' => 'Electrical Engineering',
        'it' => 'Information Technology',
        'cs' => 'Computer Science',
        'bm' => 'Biomedical Engineering',
        'tc' => 'Telecommunications',
        'pe' => 'Power Engineering',
        'se' => 'Software Engineering',
        'au' => 'Automatics',
        'ns' => 'Natural Sciences',
        'ss' => 'Social Sciences',
        'ec' => 'Economy',
        'oe' => 'Other engineering subjects',
        'other' => 'Other',
    ];

    /**
     * Gets profile data of the currently logged in user
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        // TODO: is this function needed? - logged in user data stored in the frontend, so slug is present there for the show method

        /* @var User $currentUser */
        $user = auth()->user();
        if ($user) {
            return $this->responseSuccess('Success.', [
                'user' => $user,
            ]);
        }
        return $this->responseUnauthorized();
    }

    /**
     * Gets single profile data of user with slug
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function show(string $slug): JsonResponse
    {
        /* @var User $user
         * @var User $currentUser
         */
        $currentUser = auth()->user();
        $user = $slug && strlen($slug) > 0 ? User::whereSlug($slug)->first() : false;
        if ($user) {
            if ($user->id != $currentUser->id) {
                // TODO: remove data not accessible with the current user's access level (eg. members of other branches, board members, ...)
            }
            return $this->responseSuccess('Success.', [
                'user' => $user,
            ]);
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Deletes the user currently logged in or the user specified in the request (`slug` query parameter) if the currently logged in user has permission to delete it
     *
     * @param Request $request
     * @param string $slug
     * @return JsonResponse
     */
    public function destroy(Request $request, string $slug): JsonResponse
    {
        /* @var User $user
         * @var User $currentUser
         */
        $currentUser = auth()->setRequest($request)->user();
        $user = $slug && strlen($slug) > 0 ? User::whereSlug($slug)->first() : $currentUser;
        if ($user) {
            // TODO: only board members can delete users
            /*if (...) {
                return $this->responseUnauthorized();
            }*/

            if (isset($user->thumbnail)) {
                Storage::delete(ThumbnailController::getStoragePath(ThumbnailController::TYPE_USERS, $user->thumbnail));
            }
            if (isset($user->cv)) {
                Storage::delete(UserController::CVS_DIR . DIRECTORY_SEPARATOR . $user->cv);
            }
            try {
                if ($user->delete()) {
                    return $this->responseResourceDeleted();
                }
            } catch (Exception $e) {
                report($e);
            }
            return $this->responseServerError("Failed to delete user.");
        }
        return $this->responseResourceNotFound();
    }

    /**
     * Updates the data of the user currently logged in or the user specified in the request (user_id field) if the currently logged in user has permission to edit it
     *
     * @param Request $request The data to update
     * @param string $slug
     * @return JsonResponse
     */
    public
    function update(Request $request, string $slug): JsonResponse
    {
        /* @var User $user
         * @var User $currentUser
         */
        $currentUser = auth()->setRequest($request)->user();
        $user = $slug && strlen($slug) > 0 ? User::whereSlug($slug)->first() : $currentUser;

        if ($user) {
            // TODO: allow board members to update members' data?
            if ($user->id != $currentUser->id) {
                return $this->responseUnauthorized();
            }
            $emailChange = $request->email != $user->email;
            $validations = [
                'name' => ['required', 'string', 'max:255'],
                'full_name' => ['required', 'string', 'max:255'],
                'date_of_birth' => ['required', 'date', 'date_format:Y-m-d', 'before:today'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'gender' => ['required', 'string', 'in:' . join(',', UserController::GENDER_VALUES)],
                'passport_no' => ['nullable', 'string'],
                'mobile' => ['nullable', 'phone'],
                'allergies' => ['nullable', 'string'],
                'tshirt' => ['required', 'string', 'in:' . join(',', UserController::TSHIRT_SIZE_VALUES)],
                'picture' => ['nullable', 'base64_encoded_file'],
                'food_pref' => ['required', 'string', 'in:' . join(',', array_keys(UserController::FOOD_PREFERENCES))],
                'fos' => ['required', 'string', 'in:' . join(',', array_keys(UserController::FIELD_OF_STUDIES))],
                /*'crop.unit' => ['nullable', 'string', 'in:%,px'],
                'crop.x' => ['nullable', 'numeric', 'min:0'],
                'crop.y' => ['nullable', 'numeric', 'min:0'],
                'crop.width' => ['nullable', 'numeric', 'min:0'],
                'crop.height' => ['nullable', 'numeric', 'min:0'],*/
            ];
            if ($emailChange) {
                array_push($validations['email'], 'unique:users');
            }
            $request->validate($validations, [
                'required' => __('This field is required.'),
            ]);

            $oldThumbnail = $user->thumbnail;
            $newThumbnail = null;
            switch ($request->picture_method) {
                case 'keep':
                    $newThumbnail = $oldThumbnail;
                    break;
                case 'update':
                    if (isset($request->picture)) {

                        $pic = new UploadedBase64EncodedFile(new Base64EncodedFile($request->picture));
                        $size = $pic->getSize();
                        $maxSize = 2 * 1024 * 1024;

                        if ($size > $maxSize) {
                            @unlink($pic);
                            return $this->responseUnprocessable(["File size was too big ($size bytes, max allowed: $maxSize bytes)."]);
                        }

                        // store new thumbnail on server
                        $newThumbnail = Str::random(40);
                        if ($extension = $pic->guessExtension()) {
                            $newThumbnail .= '.' . $extension;
                        }
                        try {
                            $pic = $pic->move(ThumbnailController::getStoragePath(ThumbnailController::TYPE_USERS), $newThumbnail);

                            // TODO: rather crop on server side?
                            /*if ($request->crop) {
                                $img = Image::make($pic);
                                $unit = $request->input('crop.unit') ?: 'px';
                                $percentCrop = $unit === '%';
                                $w = $request->input('crop.width') ?: ($percentCrop ? 100 : $img->width());
                                $h = $request->input('crop.height') ?: ($percentCrop ? 100 : $img->height());
                                $x = $request->input('crop.x') ?: 0;
                                $y = $request->input('crop.y') ?: 0;
                                if ($percentCrop) {
                                    $w *= $img->width();
                                    $h *= $img->height();
                                    $x *= $img->width();
                                    $y *= $img->height();
                                }
                                $w = (int)round($w);
                                $h = (int)round($h);
                                $x = (int)round($x);
                                $y = (int)round($y);
                                $img->crop($w, $h, $x, $y)->save();
                            }*/

                        } catch (Exception $e) {
                            // restore old on fail
                            $newThumbnail = $oldThumbnail;
                        }
                    }
                    break;
                case 'remove':
                    $newThumbnail = null;
                    break;
            }

            $user->name = $request->name;
            $user->full_name = $request->full_name;
            $user->email = strtolower($request->email);
            $user->date_of_birth = $request->date_of_birth;
            $user->gender = $request->gender;
            $user->passport_no = strtoupper($request->passport_no);
            $user->mobile = !isset($request->mobile) ? null : preg_replace('/^\+/', '', $request->mobile);
            $user->allergies = $request->allergies;
            $user->tshirt = $request->tshirt;
            $user->thumbnail = $newThumbnail;
            $user->food_pref = $request->food_pref;
            $user->fos = $request->fos;

            if ($emailChange) {
                $user->email_verified_at = null;
            }

            if ($user->save()) {
                // remove old thumbnail from server if succeed to modify
                if ($newThumbnail != $oldThumbnail) {
                    Storage::delete(ThumbnailController::getStoragePath(ThumbnailController::TYPE_USERS, $oldThumbnail));
                }
                return $this->responseResourceUpdated();
            }
            return $this->responseServerError("Failed to update user.");
        }
        return $this->responseResourceNotFound();
    }
}
