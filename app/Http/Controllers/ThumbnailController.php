<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ThumbnailController extends Controller
{
    const DIR_THUMBNAILS = 'thumbnails';
    const TYPE_USERS = 'users';
    const TYPE_EVENTS = 'events';
    const TYPE_BRANCHES = 'branches';
    const TYPE_TEAMS = 'teams';

    /**
     * Return thumbnail for the requested `type` with `id` and `size` for size of the image should be resized to with keeping aspect ratio.
     *
     * @param Request $request Should contain hashed entity id in query parameters and possibly a size parameter. If `size` is larger than any of the original width or height, it remains unchanged as well. After a successful resize, `size` will be the shorter side of the image (or both if it was rectangular).
     * @param string $type Currently supported values: `user`
     * @return BinaryFileResponse|Response
     */
    public function show(Request $request, string $type)
    {
        $slug = $request->query('slug');
        if ($slug && strlen($slug) > 0) {
            $size = $request->query('size');
            switch ($type) {
                case self::TYPE_USERS:
                    $user = User::whereSlug($slug)->first();
                    if ($user && $user->thumbnail) {
                        if (isset($size)) {
                            if (is_numeric($size)) {
                                $size = intval($size);
                            } else {
                                return response(null, Response::HTTP_BAD_REQUEST);
                            }
                        }
                        return $this->tryGetThumbnail($type, $user->thumbnail, $size);
                    } else {
                        return response(null, Response::HTTP_NO_CONTENT);
                    }
                //break;
                //case self::TYPE_EVENTS:
                //    break;
            }
        }
        return response(null, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param string $type
     * @param string $fileName
     * @param int $size
     * @return BinaryFileResponse|Response
     */
    private function tryGetThumbnail(string $type, string $fileName, int $size)
    {
        $path = self::getStoragePath($type, $fileName);
        if (!file_exists($path) || !is_file($path)) {
            return response(null, Response::HTTP_NOT_FOUND);
        }
        if ($size != null && $size > 0) {
            // resize image: the shorter side will have the specified size
            $img = Image::make($path);
            $w = $img->width();
            $h = $img->height();
            if ($w >= $size && $h >= $size) {
                $newWidth = $size;
                $newHeight = $size;
                if ($w != $h) {
                    // if it was not a rectangular image, resize with keeping aspect ratio
                    $ratio = max($size / $h, $size / $w);
                    $newHeight = ceil($h * $ratio);
                    $newWidth = ceil($w * $ratio);
                }
                return $img->resize($newWidth, $newHeight)->response();
            }
            // fallback: if the specified size was bigger than the image width or height, show the original one
        }

        $name = $type;
        $extPos = strrpos($path, '.');
        if ($extPos) {
            $name .= substr($path, $extPos);
        }
        // we want the image to be shown, not downloaded, this is why disposition = inline
        return response()->download($path, $name, [], 'inline');
    }

    /**
     * @param string $type
     * @param string|null $fileName
     * @return string
     */
    public static function getStoragePath(string $type, $fileName = null): string
    {
        $parts = ['app', self::DIR_THUMBNAILS, $type];
        if ($fileName != null && strlen($fileName) > 0) {
            $parts += [$fileName];
        }
        return storage_path(join(DIRECTORY_SEPARATOR, $parts));
    }
}
