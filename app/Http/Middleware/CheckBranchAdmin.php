<?php

namespace App\Http\Middleware;

use App\Branch;
use Closure;
use Illuminate\Http\Request;

class CheckBranchAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            $user_id = auth()->id();
            $branch = Branch::whereSlug($request->route()->parameter('branch'))->first();
            if ($branch) {
                if ($branch->board()->where('id', '=', $user_id)->exists()) {
                    return $next($request);
                }
            }
        }
    }
}
