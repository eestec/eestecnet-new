<?php

namespace App\Http\Middleware;

use App\Team;
use Closure;
use Illuminate\Http\Request;

class CheckTeamAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            $userId = auth()->id();
            $team = Team::whereSlug($request->route()->parameter('team'))->first();
            if ($team) {
                if ($team->board()->where('id', '=', $userId)->count() == 1) {
                    return $next($request);
                }
            }
        }
    }
}
