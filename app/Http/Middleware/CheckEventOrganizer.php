<?php

namespace App\Http\Middleware;

use App\Event;
use Closure;
use Illuminate\Http\Request;

class CheckEventOrganizer
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            $userId = auth()->id();
            $event = Event::whereSlug($request->route()->parameter('event'))->first();
            if ($event) {
                if ($event->organizers()->where('id', '=', $userId)->count() == 1) {
                    return $next($request);
                }
            }
        }
    }
}
