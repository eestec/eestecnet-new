<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;

class CheckUserAccepted
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            /* @var User $user */
            $user = auth()->user();
            if ($user) {
                if ($user->branches()->wherePivot('accepted_at', '!=', null)->count() > 0) {
                    return $next($request);
                }
            }
        }
    }
}
