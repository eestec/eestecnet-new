<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Request;

class UserCollection extends ApiResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        // Transforms the collection to match format in UserResource
        $this->collection->transform(function (User $user) {
            return (new UserResource($user));
        });

        return parent::toArray($request);
    }
}
