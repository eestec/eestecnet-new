<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;

class UserResource extends ApiResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'createdAt' => (string)$this->created_at->toDateTimeString(),
            'updatedAt' => (string)$this->updated_at->toDateTimeString(),
            'userId' => Hasher::encode($this->user_id),
            'name' => $this->name,
            'full_name' => $this->full_name,
            'email' => $this->email,
            'dateOfBirth' => $this->date_of_birth,
            'gender' => $this->gender,
            'passportNumber' => $this->passport_no,
            'mobile' => !isset($this->mobile) ? null : preg_replace('/^\+/', '', $this->mobile),
            'allergies' => $this->allergies,
            'tshirt' => $this->tshirt,
            'foodPref' => $this->food_pref,
            'fos' => $this->fos,
        ];
    }
}
