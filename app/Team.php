<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Carbon;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * App\Team
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $category
 * @property string $thumbnail
 * @property string|null $website
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $linkedin
 * @property string $description
 * @property Carbon $founded
 * @property int|null $parent_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|User[] $board
 * @property-read int|null $board_count
 * @property-read Collection|User[] $members
 * @property-read int|null $members_count
 * @method static Builder|Team newModelQuery()
 * @method static Builder|Team newQuery()
 * @method static Builder|Team query()
 * @method static Builder|Team whereCategory($value)
 * @method static Builder|Team whereCreatedAt($value)
 * @method static Builder|Team whereDescription($value)
 * @method static Builder|Team whereFacebook($value)
 * @method static Builder|Team whereFounded($value)
 * @method static Builder|Team whereId($value)
 * @method static Builder|Team whereInstagram($value)
 * @method static Builder|Team whereLinkedin($value)
 * @method static Builder|Team whereName($value)
 * @method static Builder|Team whereParentId($value)
 * @method static Builder|Team whereSlug($value)
 * @method static Builder|Team whereThumbnail($value)
 * @method static Builder|Team whereUpdatedAt($value)
 * @method static Builder|Team whereWebsite($value)
 * @mixin Eloquent
 */
class Team extends Model
{
    use HasSlug;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category', 'thumbnail', 'website', 'facebook', 'instagram', 'linkedin', 'description', 'founded', 'parent_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'founded' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Get all of the members of the team.
     */
    public function members(): MorphToMany
    {
        return $this->morphToMany('App\User', 'membership', 'membership')
            ->withTimestamps();
    }

    /**
     * Get all of the events the team has organized.
     */
    public function events(): MorphToMany
    {
        return $this->morphToMany('App\Event', 'organizer', 'organizer')
            ->withTimestamps();
    }

    public function board(): MorphToMany
    {
        return $this->morphToMany('App\User', 'membership', 'membership')
            ->wherePivot('is_board', '=', true)
            ->withTimestamps();
    }
}
