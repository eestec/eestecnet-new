<?php

namespace App;

use App\Notifications\ResetPassword as ResetPasswordNotification;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $full_name
 * @property string $slug
 * @property string $email
 * @property string $password
 * @property Carbon|null $email_verified_at
 * @property Carbon|null $date_of_birth
 * @property string|null $gender
 * @property string|null $website
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $linkedin
 * @property string|null $whatsapp
 * @property string|null $telegram
 * @property string|null $thumbnail
 * @property string|null $passport_no
 * @property int|null $mobile
 * @property string|null $cv
 * @property string|null $allergies
 * @property string|null $tshirt
 * @property string|null $food_pref
 * @property string|null $fos
 * @property bool $is_active
 * @property Carbon|null $last_login
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Branch[] $branches
 * @property-read int|null $branches_count
 * @property-read Collection|Event[] $events
 * @property-read int|null $events_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Team[] $teams
 * @property-read int|null $teams_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereAllergies($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereCv($value)
 * @method static Builder|User whereDateOfBirth($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereFacebook($value)
 * @method static Builder|User whereFoodPref($value)
 * @method static Builder|User whereFos($value)
 * @method static Builder|User whereFullName($value)
 * @method static Builder|User whereGender($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereInstagram($value)
 * @method static Builder|User whereIsActive($value)
 * @method static Builder|User whereLastLogin($value)
 * @method static Builder|User whereLinkedin($value)
 * @method static Builder|User whereMobile($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassportNo($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereSlug($value)
 * @method static Builder|User whereTelegram($value)
 * @method static Builder|User whereThumbnail($value)
 * @method static Builder|User whereTshirt($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User whereWebsite($value)
 * @method static Builder|User whereWhatsapp($value)
 * @mixin Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use HasSlug;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'full_name', 'email', 'password', 'gender', 'date_of_birth',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // TODO: Creates a bug when trying to get data from db using belongsToMany(
    // public $appends = [
    //     'hashid',
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'date_of_birth' => 'datetime',
        'last_login' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('full_name')
            ->saveSlugsTo('slug');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * Allows us to customize the password notification email.
     * See: App/Notifications/ResetPassword.php
     *
     * @param string
     */
    public function sendPasswordResetNotification($token)
    {
        $email = $this->getEmailForPasswordReset();
        $user = $this::whereEmail($email)->first();
        $this->notify(new ResetPasswordNotification($token, $user->id));
    }

    public function age(Carbon $when = null): int
    {
        return ($when ?? now())->diff($this->date_of_birth)->y;
    }

    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, Application::class)
            ->wherePivot('accepted', '!=')
            ->withPivot('confirmed_at', 'accepted_at', 'accepted')
            ->withTimestamps();
    }

    /**
     * Get all of the teams a user is member of.
     */
    public function teams(): MorphToMany
    {
        return $this->morphedByMany('App\Team', 'membership', 'membership')
            ->withTimestamps();
    }

    /**
     * Get all of the branches a user is member of.
     */
    public function branches(): MorphToMany
    {
        return $this->morphedByMany('App\Branch', 'membership', 'membership')
            ->withPivot('accepted_at', 'accepted_by')
            ->withTimestamps();
    }

    /**
     * Get all of the events the user has organized.
     */
    public function organizer(): MorphToMany
    {
        return $this->morphToMany('App\Event', 'organizer', 'organizer')
            ->withTimestamps();
    }
}
