<?php


namespace App\Helpers;


class ValidationResponse
{
    public $isValid;
    public $message;

    public function __construct($isValid, $message = '')
    {
        $this->isValid = $isValid;
        $this->message = $message;
    }
}
