<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth endpoints
// matches: api/auth/...
Route::prefix('auth')->middleware('cors')->group(function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LogoutController@logout');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('forgot-password', 'Auth\ForgotPasswordController@email');
    Route::post('reset-password', 'Auth\ResetPasswordController@reset');
});

// Resource endpoints
Route::middleware(['cors', 'api', 'jwt.auth'])->group(function () {
    Route::apiResource('thumbnail', 'ThumbnailController')->only(['show']);
    Route::apiResource('user', 'UserController')->only(['index', 'show', 'update', 'destroy']);
    Route::apiResource('events', 'EventController');
    Route::apiResource('branches', 'BranchController');
    Route::apiResource('teams', 'TeamController');
});

// Not found
Route::fallback(function () {
    return response()->json(['message' => 'Resource not found.'], 404);
});

// Any registered user can access
Route::middleware(['cors', 'api', 'jwt.auth'])->group(function () {

    // Events routes
    // matches: api/events/...
    Route::prefix('events')->group(function () {

        // matches: api/events/{event}/...
        Route::prefix('{event}')->group(function () {

            Route::post('apply', 'ApplicationController@apply');

            // Application routes
            // matches: api/events/{event}/applications/{application}/...
            Route::prefix('applications/{application}')->group(function () {
                Route::patch('', 'ApplicationController@update');
                Route::put('', 'ApplicationController@update');
                Route::get('letter', 'ApplicationController@showLetter');
                Route::delete('', 'ApplicationController@destroy');
                Route::post('confirm', 'ApplicationController@confirm');
                Route::get('applications', 'ApplicationController@index');
                Route::get('', 'ApplicationController@show');
                Route::post('accept', 'ApplicationController@accept');
                Route::post('decline', 'ApplicationController@decline');
                // TODO: create controller
                //Route::get('details', 'ParticipantDetailsController@show');
                //Route::post('details', 'ParticipantDetailsController@store');
            });

            Route::post('applications/accept', 'ApplicationController@acceptList');
        });

        // Internal Event routes
        // TODO: Grant access to site admins as well? - create middleware and add it to array below
        // Only committee administrators can access (request must contain committee_id)
        Route::middleware(['committee_admin'])->group(function () {
            Route::post('', 'EventController@store');

            // matches: api/events/{event}/...
            Route::prefix('{event}')->group(function () {
                Route::delete('', 'EventController@destroy');
                Route::patch('', 'EventController@update');
                Route::put('', 'EventController@update');
                Route::get('applications', 'ApplicationController@index');
            });
        });

    });

    // Branches routes
    // matches: api/branches/...
    Route::prefix('branches')->group(function () {

    });

    // Teams routes
    // matches: api/teams/...
    Route::prefix('branches')->group(function () {

    });
});
