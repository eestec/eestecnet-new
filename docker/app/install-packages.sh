#!/bin/bash

# With errexit turned on, any expression that exits with a non-zero exit code 
# terminates execution of the script, and the exit code of the expression 
# becomes the exit code of the script.
set -o errexit

# Exit if the script tries to use undeclared variables.
set -o nounset

apt-get update

# -- no-install-recommends: Do not install recommended packages.
apt-get install -y --no-install-recommends \
	wget \
	libldb-dev \
	unzip \
	zip \
	libpq-dev \
	apt-utils

# Install PHP extensions
# These extensions can only be installed from source
# See: https://olvlvl.com/2019-06-install-php-ext-source
docker-php-ext-install -j$(nproc) bcmath pgsql pdo_pgsql 

# Clean up after apt install
rm -rf /var/lib/apt/lists/*
