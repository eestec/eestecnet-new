FROM php:7-fpm

ARG USER_ID
ARG GROUP_ID

COPY docker/app/install-packages.sh /tmp/install-packages.sh
RUN chmod +x /tmp/install-packages.sh && \
	/tmp/install-packages.sh && \
	rm /tmp/install-packages.sh

RUN yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

COPY docker/recreate-user.sh /tmp/recreate-user.sh
RUN echo "GROUP_ID=${GROUP_ID:-0} USER_ID=${USER_ID:-0}" && \
	chmod +x /tmp/recreate-user.sh && \
	GROUP_ID=${GROUP_ID:-0} USER_ID=${USER_ID:-0} /tmp/recreate-user.sh /var/www && \
	rm /tmp/recreate-user.sh

RUN rm -rf /var/www/html

USER ${GROUP_ID:-$(id -g ${USER})}:${USER_ID:-$(id -u ${USER})}

EXPOSE 9000
CMD ["php-fpm"]
