FROM php:7-fpm

COPY docker/app/www.conf /usr/local/etc/php-fpm.d/www.conf

COPY docker/app/install-packages.sh /tmp/install-packages.sh
RUN chmod +x /tmp/install-packages.sh && \
	/tmp/install-packages.sh && \
	rm /tmp/install-packages.sh

WORKDIR /var/www

# TODO: finish production dockerfile

COPY . /var/www

RUN chown -R www-data:www-data \
	/var/www/storage \
	/var/www/bootstrap/cache

RUN php artisan optimize
