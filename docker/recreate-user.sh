#!/bin/sh

#
# USAGE: ./recreate-user.sh [directories ...]
#
# [directories ...]: all paths separated by spaces what should be owned by the user (optional)
#
#
# IMPORTANT: this script executes only if USER_ID and GROUP_ID variables are set with value other than 0
#


# With errexit turned on, any expression that exits with a non-zero exit code 
# terminates execution of the script, and the exit code of the expression 
# becomes the exit code of the script.
set -o errexit

# Exit if the script tries to use undeclared variables.
set -o nounset

if [ "${USER_ID:-0}" -ne 0 ] && [ "${GROUP_ID:-0}" -ne 0 ]
then
	# Delete www-data group and user
	userdel -f www-data
	if getent group www-data
	then
		groupdel www-data
	fi
	
	# If group with GROUP_ID exists...
	if getent group "$GROUP_ID"
	then
		group=$(getent group "$GROUP_ID")
		# ... and that group is not www-data...
		if ! [ "$group" = "www-data" ]
		then
			# ... change the group ID (let's hope 4343 is not taken), because we need its current one
			groupmod -g 4343 "$(echo "$group" | cut -d: -f1)"
		fi
	fi

	# Same as before for USER_ID
	if id "$USER_ID"
	then
		user=$(id -un "$USER_ID")
		if ! [ "$user" = "www-data" ]
		then
			usermod -u 4343 "$user"
		fi
	fi


	# Create www-data group and user with provided GROUP_ID and USER_ID
	groupadd --gid="$GROUP_ID" www-data
	useradd -l --uid="$USER_ID" --gid="$GROUP_ID" www-data

	# Create home directory for www-data
	install -d -m 0755 -o www-data -g www-data /home/www-data

	# Set www-data as owner of the specified directory/directories ($@: all arguments)
	chown -R "$USER_ID":"$GROUP_ID" \
		/home/www-data \
		"$@"
fi
