# README.md

## Installation Instructions

### Linux
Follow the instructions in [docs/installation_instructions/linux_installation.md](docs/installation_instructions/linux_installation.md).

### Windows
//TODO

### MacOS
//TODO

## Kick Start

### For the Backend
Check the [Laravel Documentation](laravel.com/docs).

Some important files are: 

* `app`: contains the most important files (eg. controllers in the Http, or models directly in the app directory) 
* `routes`: for the routes handled by the controllers (eg. URLs). In these files only API URLs and URLs not handled by the frontend are defined. 
* `routes/api.php`: for the API resource endpoints

Possibly you will need to create a [database migration](https://laravel.com/docs/7.x/migrations#introduction) by running:

```bash
php artisan make:migration create_users_table
```
as well as to create a database factory/seed in the database/factories or database/seeds directory.


### For the Frontend
Check the [React Documentation](reactjs.org/docs/getting-started.html).

Some important files and directories in the **resources** directory are: 

* `/js`: contains all the important files. Most probably you want to edit the files under resources/js/pages or resources/js/components directories. These components are rendered on different pages of the website.
* `/js/app.js`: the entry point of the frontend React app. It is compiled with Webpack to the public/js/app.js file.
* `/js/routes/routes.js`: the file where frontend URLs are assigned to components to be shown when the corresponding URL is visited from the browser, since the URL handling on the website is done by the React app. It contains definitions for which pages are available for the public and which for the logged in users only.
* `/js/BackendRoutes.js`: the file where backend API routes are defined
* `/images`: for images
* `/sass`: for SCSS resources


### To Contribute

Check the [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

1. When you start working on a new feature, create a feature branch and push your changes following the instructions [here](https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html).
2. Create a merge request.
//TODO: add instructions to make merge requests


## Directory Structure
* For the `app`, `bootstrap`, `config`, `database`, `public`, `resources`, `routes`, `storage`,`tests` directories see the [Laravel Documentation](https://laravel.com/docs/7.x/structure#the-root-directory)
* `docker`: for Docker related files needed currently for local deployment and CI/CD in the future
* `docs`: for documentation 
* `root`: for the files needed for frontend's configuration or deployment / building

After installation the following directories are created:
* `node_modules`: for NodeJS dependencies downloaded by Yarn and Composer
* `vendor`: for PHP dependencies downloaded by Yarn and Composer

## Where to Find...

- the website in local development: [localhost:8080](localhost:8080)

- PgAdmin4 (if you've started it): [localhost:5050](localhost:5050)

- logs of Laravel: `storage/logs`

## Tests

There are some unit tests written for the frontend part, you can run them with

```bash
docker-compose run --rm yarn test
```

## Production

Build frontend for production

```bash
docker-compose run --rm yarn run prod
```

## Other Useful Commands

- Access shell of any stopped container:

	```bash
	docker-compose run --entrypoint="bash --login" --rm [name]
	```

	> [name]: one of `artisan`, `composer` or `yarn`<br>
	Exit with `exit` + <kbd>Enter</kbd>

- Access shell of any running container:

	```bash
	docker-compose exec [name] bash --login
	```

	> [name]: one of `app`, `db` or `nginx`<br>
	Exit with `exit` + <kbd>Enter</kbd>

- Access PostgreSQL shell:

	```bash
	docker-compose exec --user postgres db psql
	```

	> Exit with `\q` + <kbd>Enter</kbd>

- Access logs of any container:

	```bash
	docker-compose logs --follow [name]
	```

	> [name]: one of `app`, `composer`, `db`, `nginx` or `yarn`<br>
	If you don't want to have the logs updating when a new entry is written, omit the `--follow` option. When `--follow` was present, you can exit with <kbd>Ctrl</kbd> + <kbd>C</kbd>

- Install PHP dependency:

	```bash
	docker-compose run --rm composer require [package name]
	```

- Install NodeJS dependency:

	```bash
	docker-compose run --rm yarn add [package name]
	```

... feel free to add your own ones ;)
