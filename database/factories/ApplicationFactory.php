<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Application;
use Faker\Generator as Faker;

$factory->define(Application::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    $events = App\Event::pluck('id')->toArray();
    return [
        'letter' => $faker->sentence(20, true),
        'user_id' => $faker->randomElement($users),
        'event_id' => $faker->randomElement($events),
    ];
});
