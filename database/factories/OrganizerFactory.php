<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Organizer;
use Faker\Generator as Faker;

$factory->define(Organizer::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    $events = App\Event::pluck('id')->toArray();
    $teams = App\Team::pluck('id')->toArray();
    $branches = App\Branch::pluck('id')->toArray();
    return [
        'organizer_id'   => $faker->randomElement($teams),
        'organizer_type' => 'team',
        'event_id'       => $faker->randomElement($events),
        // 'role'      => $faker->randomElement(['ho', 'pr', 'fr', 'lo', 'ac', 'other']),
    ];
});
