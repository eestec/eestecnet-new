<?php

/** @var Factory $factory */

use App\Branch;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Branch::class, function (Faker $faker) {
    $type = $faker->randomElement(['lc', 'jlc', 'observer']);
    return [
        'name'        => (strlen($type) > 3 ? ucfirst($type) : strtoupper($type)) . ' ' . $faker->city,
        'country'     => $faker->country,
        'type'        => $type,
        'thumbnail'   => $faker->imageUrl(),
        'description' => $faker->sentences(5, true),
        'region'      => $faker->numberBetween(1, 10),
        'website'     => $faker->url,
        'facebook'    => $faker->url,
        'instagram'   => $faker->url,
        'linkedin'    => $faker->url,
        'address'     => $faker->streetAddress,
        'founded'     => $faker->dateTimeThisDecade(),
    ];
});
