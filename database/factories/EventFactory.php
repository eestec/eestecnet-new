<?php

/** @var Factory $factory */

use App\Event;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Event::class, function (Faker $faker) {
    $deadline = now()->setTimestamp($faker->dateTimeInInterval('-10 months', '+ 11 months', 'CET')->getTimestamp())->toImmutable();
    $starts = $deadline->addMonth();
    $ends = $starts->addWeek();
    $branches = App\Branch::pluck('id')->toArray();
    return [
        'name'        => $faker->sentence(4, true),
        // 'branch_id'   => $faker->randomElement($branches),
        'type'        => $faker->randomElement(['ws', 'op', 'ex', 'cg', 'mw']),
        'scope'       => $faker->randomElement(['int', 'loc']),
        'starts'      => $starts,
        'ends'        => $ends,
        'deadline'    => $deadline,
        'thumbnail'   => $faker->imageUrl(320, 240, 'city'),
        'location'    => $faker->city,
        'max_pax'     => $faker->numberBetween(10, 50),
        'fee'         => $faker->randomElement([0, 40, 60, 120]),
        'description' => $faker->paragraphs(3, true),
    ];
});
