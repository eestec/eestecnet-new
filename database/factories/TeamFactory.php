<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Team;
use Faker\Generator as Faker;

$factory->define(Team::class, function (Faker $faker) {
    return [
        'name'        => $faker->sentence(4, true),
        'category'    => $faker->randomElement(['team', 'project', 'ba']),
        'thumbnail'   => $faker->imageUrl(),
        'description' => $faker->sentences(5, true),
        'founded'     => $faker->dateTimeThisDecade()
    ];
});
