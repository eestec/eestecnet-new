<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Membership;
use Faker\Generator as Faker;

$factory->define(Membership::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    $branches = App\Branch::pluck('id')->toArray();
    $teams = App\Team::pluck('id')->toArray();
    return [
        'user_id'         => $faker->randomElement($users),
        'membership_id'   => $faker->randomElement($branches),
        'membership_type' => 'branch'
    ];
});
