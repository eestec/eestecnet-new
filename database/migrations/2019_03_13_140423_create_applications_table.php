<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('event_id');

            $table->unsignedInteger('priority')->nullable();
            $table->text('letter');
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('reviewed_at')->nullable();
            $table->unsignedBigInteger('reviewed_by')->nullable();
            $table->boolean('accepted')->nullable();

            $table->timestamp('arrival')->nullable();
            $table->string('arrival_location')->nullable();
            $table->enum('arrival_means', ['bus', 'plane', 'train', 'boat', 'car', 'other'])->nullable();
            $table->timestamp('departure')->nullable();
            $table->string('departure_location')->nullable();
            $table->enum('departure_means', ['bus', 'plane', 'train', 'boat', 'car', 'other'])->nullable();

            $table->text('notes')->nullable();
            $table->text('organizer_notes')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('event_id')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
