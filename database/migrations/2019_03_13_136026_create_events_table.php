<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->enum('type', ['ws', 'op', 'ex', 'cg', 'mw']);
            $table->enum('scope', ['int', 'loc'])->default('int');
            $table->date('starts');
            $table->date('ends');
            $table->timestamp('deadline');
            $table->string('thumbnail');
            $table->string('location');
            $table->unsignedInteger('max_pax');
            $table->unsignedInteger('fee');
            $table->text('description');
            $table->unsignedBigInteger('connected_to')->nullable(); // eg. main event to imw
            $table->timestamps();

            $table->foreign('connected_to')->references('id')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
