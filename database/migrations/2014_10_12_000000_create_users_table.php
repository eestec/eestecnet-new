<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('full_name');
            $table->string('slug')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();

            $table->date('date_of_birth')->nullable();
            $table->enum('gender', ['male', 'female', 'other'])->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('telegram')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('passport_no')->nullable();
            $table->unsignedBigInteger('mobile')->nullable();
            $table->string('cv')->nullable();
            $table->string('allergies')->nullable();
            $table->enum('tshirt', ['xs', 's', 'm', 'l', 'xl', 'xxl', 'xxxl'])->nullable();
            $table->enum('food_pref', ['none', 'vegan', 'o-veg', 'l-veg', 'ol-veg', 'pesc', 'meat'])->nullable();
            $table->enum('fos', ['none', 'ee', 'it', 'cs', 'bm', 'tc', 'pe', 'se', 'au', 'ns', 'ss', 'ec', 'oe', 'other'])->nullable();

            $table->boolean('is_active')->default(true);
            $table->timestamp('last_login')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
